package th.co.growthd.oktrucker

import android.support.multidex.MultiDexApplication
import com.crashlytics.android.Crashlytics
import com.google.firebase.FirebaseApp
import io.fabric.sdk.android.Fabric
import net.khirr.library.foreground.Foreground
import pl.aprilapps.easyphotopicker.EasyImage
import uk.co.chrisjenx.calligraphy.CalligraphyConfig

class MyApplication : MultiDexApplication() {

    override fun onCreate() {
        super.onCreate()

        Foreground.init(this)
        FirebaseApp.initializeApp(this)

        /* image */
        EasyImage.configuration(this).setImagesFolderName("OKTrucker")

        Fabric.with(this, Crashlytics())
    }
}