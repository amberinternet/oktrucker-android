package th.co.growthd.oktrucker.adapter

import android.databinding.DataBindingUtil
import android.support.v7.widget.RecyclerView
import android.view.LayoutInflater
import android.view.ViewGroup
import th.co.growthd.deerandbook.ui.viewholder.NoDataViewHolder
import th.co.growthd.oktrucker.R
import th.co.growthd.deerandbook.util.VIEW_TYPE_DATA
import th.co.growthd.deerandbook.util.VIEW_TYPE_NO_DATA
import th.co.growthd.okTruck.ui.viewholder.CreditViewHolder
import th.co.growthd.oktrucker.databinding.ItemCreditBinding
import th.co.growthd.oktrucker.databinding.ItemNoDataBinding
import th.co.growthd.oktrucker.ui.viewholder.LoadMoreViewHolder

class CreditAdapter(private var creditList: Array<String>) : RecyclerView.Adapter<RecyclerView.ViewHolder>() {

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): RecyclerView.ViewHolder {
        val binding = DataBindingUtil.inflate<ItemCreditBinding>(LayoutInflater.from(parent.context), R.layout.item_credit, parent, false)
        return CreditViewHolder(binding)
    }

    override fun getItemCount(): Int {
        return creditList.size
    }

    override fun onBindViewHolder(holder: RecyclerView.ViewHolder, position: Int) {
        when (holder) {
            is CreditViewHolder -> {
                holder.credit = creditList[position]
                holder.initView()
            }
        }
    }
}