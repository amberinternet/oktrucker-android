package th.co.growthd.oktrucker.adapter

import android.databinding.DataBindingUtil
import android.support.v7.widget.RecyclerView
import android.view.LayoutInflater
import android.view.ViewGroup
import org.greenrobot.eventbus.EventBus
import th.co.growthd.deerandbook.ui.viewholder.NoDataViewHolder
import th.co.growthd.oktrucker.R
import th.co.growthd.deerandbook.util.VIEW_TYPE_DATA
import th.co.growthd.deerandbook.util.VIEW_TYPE_LOAD_MORE
import th.co.growthd.deerandbook.util.VIEW_TYPE_NO_DATA
import th.co.growthd.okTruck.ui.viewholder.DepositViewHolder
import th.co.growthd.oktrucker.databinding.ItemDepositBinding
import th.co.growthd.oktrucker.databinding.ItemLoadMoreBinding
import th.co.growthd.oktrucker.databinding.ItemNoDataBinding
import th.co.growthd.oktrucker.model.Deposit
import th.co.growthd.oktrucker.model.subscriber.LoadMoreSubscriber
import th.co.growthd.oktrucker.models.DepositPagination
import th.co.growthd.oktrucker.ui.viewholder.LoadMoreViewHolder

class DepositAdapter(private var depositList: List<Deposit>, var depositPagination: DepositPagination? = null) : RecyclerView.Adapter<RecyclerView.ViewHolder>() {

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): RecyclerView.ViewHolder {
        return when (viewType) {
            VIEW_TYPE_DATA -> {
                val binding = DataBindingUtil.inflate<ItemDepositBinding>(LayoutInflater.from(parent.context), R.layout.item_deposit, parent, false)
                DepositViewHolder(binding)
            }
            VIEW_TYPE_LOAD_MORE -> {
                val binding = DataBindingUtil.inflate<ItemLoadMoreBinding>(LayoutInflater.from(parent.context), R.layout.item_load_more, parent, false)
                LoadMoreViewHolder(binding)
            }
            else -> {
                val binding = DataBindingUtil.inflate<ItemNoDataBinding>(LayoutInflater.from(parent.context), R.layout.item_no_data, parent, false)
                NoDataViewHolder(binding)
            }
        }
    }

    override fun getItemCount(): Int {
        return if (depositList.isNotEmpty()) depositList.size + 1 else 1
    }

    override fun onBindViewHolder(holder: RecyclerView.ViewHolder, position: Int) {
        when (holder) {
            is DepositViewHolder -> {
                holder.deposit = depositList[position]
                holder.initView()
            }
            is LoadMoreViewHolder -> {
                when {
                    depositPagination == null -> {
                        holder.showProgressBar()
                        loadMore(1)
                    }
                    depositPagination?.total == depositList.size -> holder.hideProgressBar()
                    else -> {
                        holder.showProgressBar()
                        loadMore(depositPagination!!.currentPage + 1)
                    }
                }
            }
            is NoDataViewHolder -> {

            }
        }
    }

    private fun loadMore(nextPage: Int) {
        EventBus.getDefault().post(LoadMoreSubscriber().apply {
            isLoadMoreDeposit = true
            page = nextPage
        })
    }

    override fun getItemViewType(position: Int): Int {
        if (depositList.isNotEmpty() && position != depositList.size) {
            return VIEW_TYPE_DATA
        } else if (depositPagination?.total == 0) {
            return VIEW_TYPE_NO_DATA
        }
        return VIEW_TYPE_LOAD_MORE
    }
}