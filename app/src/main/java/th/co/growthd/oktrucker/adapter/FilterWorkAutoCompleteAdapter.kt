package th.co.growthd.oktrucker.adapter

import android.databinding.DataBindingUtil
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.BaseAdapter
import android.widget.Filter
import android.widget.Filterable
import th.co.growthd.oktrucker.R
import th.co.growthd.oktrucker.databinding.ItemAutoCompleteFilterWorkBinding

class FilterWorkAutoCompleteAdapter : BaseAdapter(), Filterable {

    private lateinit var binding: ItemAutoCompleteFilterWorkBinding
    lateinit var provinceArray: ArrayList<String>
    lateinit var resultArray: ArrayList<String>
    private var isFilter: Boolean = false

    override fun getView(position: Int, convertView: View?, parent: ViewGroup?): View {
        binding = DataBindingUtil.inflate(LayoutInflater.from(parent?.context), R.layout.item_auto_complete_filter_work, parent, false)
        binding.nameTextView.text = resultArray[position].split(",")[0]
        return binding.root
    }

    override fun getItem(position: Int): Any {
        return resultArray[position]
    }

    override fun getItemId(position: Int): Long {
        return position.toLong()
    }

    override fun getCount(): Int {
        return resultArray.size
    }

    override fun getFilter(): Filter {
        return object : Filter() {

            override fun performFiltering(constraint: CharSequence?): Filter.FilterResults {
                isFilter = constraint.toString().isNotEmpty()

                val query = if (constraint != null) autocomplete(constraint.toString()) else arrayListOf()

                val results = FilterResults()
                results.values = query
                results.count = query.size

                return results
            }

            private fun autocomplete(input: String): ArrayList<String> {
                val results = arrayListOf<String>()

                for (province in provinceArray) {
                    if (province.contains(input, ignoreCase = true)) results.add(province)
                }

                return results
            }

            override fun publishResults(constraint: CharSequence?, results: Filter.FilterResults) {
                if (isFilter) {
                    if (results.count > 0) {
                        resultArray = results.values as ArrayList<String>
                        notifyDataSetChanged()
                    } else notifyDataSetInvalidated()
                } else {
                    resultArray = provinceArray
                    notifyDataSetChanged()
                }
            }

            override fun convertResultToString(result: Any) = result.toString()
        }
    }
}