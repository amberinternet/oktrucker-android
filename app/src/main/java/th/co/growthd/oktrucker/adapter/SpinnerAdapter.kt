package th.co.growthd.oktrucker.adapter

import android.databinding.DataBindingUtil
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.BaseAdapter
import th.co.growthd.oktrucker.R
import th.co.growthd.oktrucker.databinding.ItemSpinnerBinding

class SpinnerAdapter : BaseAdapter() {

    private lateinit var binding: ItemSpinnerBinding
    lateinit var stringArray: Array<String>

    override fun getView(position: Int, convertView: View?, parent: ViewGroup?): View {
        binding = DataBindingUtil.inflate(LayoutInflater.from(parent?.context), R.layout.item_spinner, parent, false)
        binding.nameTextView.text = stringArray[position]
        return binding.root
    }

    override fun getItem(position: Int): Any {
        return stringArray[position]
    }

    override fun getItemId(position: Int): Long {
        return position.toLong()
    }

    override fun getCount(): Int {
        return stringArray.size
    }
}