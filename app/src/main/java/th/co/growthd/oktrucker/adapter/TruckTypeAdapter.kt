package th.co.growthd.oktrucker.adapter

import android.databinding.DataBindingUtil
import android.support.v7.widget.RecyclerView
import android.view.LayoutInflater
import android.view.ViewGroup
import th.co.growthd.oktrucker.R
import th.co.growthd.okTruck.ui.viewholder.TruckTypeViewHolder
import th.co.growthd.oktrucker.databinding.ItemTruckTypeBinding
import th.co.growthd.oktrucker.model.TruckType

class TruckTypeAdapter(private var truckTypeList: List<TruckType>, private val truckType: TruckType) : RecyclerView.Adapter<RecyclerView.ViewHolder>() {

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): RecyclerView.ViewHolder {
        val binding = DataBindingUtil.inflate<ItemTruckTypeBinding>(LayoutInflater.from(parent.context), R.layout.item_truck_type, parent, false)
        return TruckTypeViewHolder(binding)
    }

    override fun getItemCount(): Int {
        return truckTypeList.size
    }

    override fun onBindViewHolder(holder: RecyclerView.ViewHolder, position: Int) {
        when (holder) {
            is TruckTypeViewHolder -> {
                holder.truckType = truckTypeList[position]
                holder.initView()
                if (truckTypeList[position].id == truckType.id) holder.highlight()
            }
        }
    }
}