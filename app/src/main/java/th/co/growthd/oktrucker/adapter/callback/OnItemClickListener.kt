package th.co.growthd.oktrucker.adapter.callback

interface OnItemClickListener {
    fun clickAtIndex(position: Int)
}