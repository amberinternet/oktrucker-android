package th.co.growthd.oktrucker.model

import com.google.gson.annotations.SerializedName

data class AccessToken(@SerializedName("token") var token: String = "")