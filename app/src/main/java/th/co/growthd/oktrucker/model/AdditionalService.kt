package th.co.growthd.oktrucker.model

import com.google.gson.annotations.SerializedName
import org.parceler.Parcel
import th.co.growthd.deerandbook.util.INITIAL_INT
import th.co.growthd.deerandbook.util.INITIAL_STRING

@Parcel(Parcel.Serialization.BEAN)
data class AdditionalService(

        @SerializedName("id")
        var id: Int = INITIAL_INT,

        @SerializedName("additional_service_name")
        var name: String = INITIAL_STRING,

        @SerializedName("price")
        var charge: String = INITIAL_STRING
)