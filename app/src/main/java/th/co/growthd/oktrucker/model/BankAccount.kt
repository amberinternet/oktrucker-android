package th.co.growthd.oktrucker.model

import com.google.gson.annotations.SerializedName
import org.parceler.Parcel
import th.co.growthd.deerandbook.util.INITIAL_INT
import th.co.growthd.deerandbook.util.INITIAL_STRING

@Parcel(Parcel.Serialization.BEAN)
class BankAccount(

        @SerializedName("id")
        var id: Int = INITIAL_INT,

        @SerializedName("driver_id")
        var driverId: Int = INITIAL_INT,

        @SerializedName("account_name")
        var accountName: String = INITIAL_STRING,

        @SerializedName("account_bank")
        var bank: String = INITIAL_STRING,

        @SerializedName("account_number")
        var accountNumber: String = INITIAL_STRING,

        @SerializedName("account_image")
        var accountImage: String = INITIAL_STRING

) {

    override fun toString(): String {
        return "BankAccount(id=$id, driverId=$driverId, accountName='$accountName', bank='$bank', accountNumber='$accountNumber', accountImage='$accountImage')"
    }
}