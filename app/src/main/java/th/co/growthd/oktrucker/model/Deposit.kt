package th.co.growthd.oktrucker.model

import com.google.gson.annotations.SerializedName
import org.parceler.Parcel
import th.co.growthd.deerandbook.util.INITIAL_INT
import th.co.growthd.deerandbook.util.INITIAL_STRING
import th.co.growthd.deerandbook.util.UNKNOWN
import th.co.growthd.oktrucker.R
import th.co.growthd.oktrucker.roundDecimalFormat
import th.co.growthd.oktrucker.util.enum.DepositType
import th.co.growthd.oktrucker.util.enum.TransactionRequestStatus

@Parcel(Parcel.Serialization.BEAN)
data class Deposit(

        @SerializedName("id")
        var id: Int = INITIAL_INT,

        @SerializedName("topup_type")
        var type: Int = INITIAL_INT,

        @SerializedName("price")
        var price: String = INITIAL_STRING,

        @SerializedName("status")
        var status: Int = INITIAL_INT,

        @SerializedName("approved_time")
        var approvedTime: String = INITIAL_STRING,

        @SerializedName("approved_note")
        var remark: String = INITIAL_STRING,

        @SerializedName("pay_slip_image")
        var transferSlip: String = INITIAL_STRING,

        @SerializedName("pay_time")
        var payTime: String = INITIAL_STRING,

        @SerializedName("formatted_topup_id")
        var readableId: String = INITIAL_STRING,

        @SerializedName("created_at")
        var createdAt: String = INITIAL_STRING
) {

    fun getCredit() = price.toDouble().roundDecimalFormat()

    fun getReadableCredit(): String {
        val credit = getCredit()
        return "+ ฿$credit"
    }

    fun getStatusBackground(): Int {
        return when (status) {
            TransactionRequestStatus.APPROVED.status -> R.drawable.background_status_success
            TransactionRequestStatus.WAITING.status -> R.drawable.background_status_waiting
            TransactionRequestStatus.REJECT.status -> R.drawable.background_status_reject
            else -> R.drawable.background_status_reject
        }
    }

    fun getStatusStyle(): Int {
        return when (status) {
            TransactionRequestStatus.APPROVED.status -> R.style.TextView_Status_Success
            TransactionRequestStatus.WAITING.status -> R.style.TextView_Status_Waiting
            TransactionRequestStatus.REJECT.status -> R.style.TextView_Status_Reject
            else -> R.drawable.background_status_reject
        }
    }

    fun getReadableStatus(): String {
        return when (status) {
            TransactionRequestStatus.APPROVED.status -> TransactionRequestStatus.APPROVED.depositMessage
            TransactionRequestStatus.REJECT.status -> TransactionRequestStatus.REJECT.depositMessage
            TransactionRequestStatus.WAITING.status -> TransactionRequestStatus.WAITING.depositMessage
            else -> UNKNOWN
        }
    }

    fun getReadablePaymentMethod(): String {
        return when (type) {
            DepositType.OMISE.value -> DepositType.OMISE.method
            DepositType.TRANSFER.value -> DepositType.TRANSFER.method
            else -> UNKNOWN
        }
    }

    fun getSlipImagePath(): String {
        return "image/topup/pay-slip-image/$transferSlip"
    }
}