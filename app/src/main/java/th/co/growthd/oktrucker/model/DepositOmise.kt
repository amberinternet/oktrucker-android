package th.co.growthd.oktrucker.model

import com.google.gson.annotations.SerializedName
import org.parceler.Parcel
import th.co.growthd.deerandbook.util.INITIAL_STRING

@Parcel(Parcel.Serialization.BEAN)
data class DepositOmise(

        @SerializedName("topup")
        var deposit: Deposit = Deposit(),

        @SerializedName("authorize_uri")
        var authorizeUri: String = INITIAL_STRING
)