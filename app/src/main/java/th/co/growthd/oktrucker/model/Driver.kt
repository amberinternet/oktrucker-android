package th.co.growthd.oktrucker.model

import com.google.gson.annotations.SerializedName
import org.parceler.Parcel
import th.co.growthd.deerandbook.util.INITIAL_INT
import th.co.growthd.deerandbook.util.INITIAL_STRING

@Parcel(Parcel.Serialization.BEAN)
data class Driver(

        @SerializedName("id")
        var id: Int = INITIAL_INT,

        @SerializedName("driver_type_id")
        var driverTypeId: Int = INITIAL_INT,

        @SerializedName("company_name")
        var companyName: String = INITIAL_STRING,

        @SerializedName("citizen_id")
        var citizenId: String = INITIAL_STRING,

        @SerializedName("driver_license_id")
        var driverLicenseId: String = INITIAL_STRING,

        var driverLicenseTypePosition: Int = INITIAL_INT,

        @SerializedName("driver_license_type")
        var driverLicenseType: String = INITIAL_STRING,

        @SerializedName("created_at")
        var createdAt: String = INITIAL_STRING,

        @SerializedName("formatted_driver_id")
        var readableDriverId: String = INITIAL_STRING,

        @SerializedName("account")
        var bankAccount: BankAccount = BankAccount(),

        @SerializedName("type")
        var driverType: DriverType = DriverType(),

        @SerializedName("car")
        var truck: Truck = Truck()

) {

        override fun toString(): String {
                return "Driver(id=$id, driverTypeId=$driverTypeId, companyName='$companyName', citizenId='$citizenId', driverLicenseId='$driverLicenseId', driverLicenseTypePosition=$driverLicenseTypePosition, driverLicenseType='$driverLicenseType', driverType=$driverType, truck=$truck)"
        }
}