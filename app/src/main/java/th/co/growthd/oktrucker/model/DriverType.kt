package th.co.growthd.oktrucker.model

import com.google.gson.annotations.SerializedName
import org.parceler.Parcel
import th.co.growthd.deerandbook.util.INITIAL_INT
import th.co.growthd.deerandbook.util.INITIAL_STRING

@Parcel(Parcel.Serialization.BEAN)
data class DriverType(

        @SerializedName("id")
        var id: Int = INITIAL_INT,

        @SerializedName("driver_type_name")
        var name: String = INITIAL_STRING,

        @SerializedName("commission")
        var commission: Int = INITIAL_INT
) {
        fun getCommissionPercent() : String {
                return "$commission%"
        }
}