package th.co.growthd.oktrucker.model

import com.google.gson.annotations.SerializedName
import org.parceler.Parcel
import th.co.growthd.deerandbook.util.INITIAL_INT
import th.co.growthd.deerandbook.util.INITIAL_STRING

@Parcel(Parcel.Serialization.BEAN)
data class Item(

        @SerializedName("id")
        var id: Int = INITIAL_INT,

        @SerializedName("item_name")
        var name: String = INITIAL_STRING,

        @SerializedName("item_quantity")
        var quantity: String = INITIAL_STRING,

        @SerializedName("item_weight")
        var weight: String = INITIAL_STRING,

        @SerializedName("item_image")
        var itemImage: String = INITIAL_STRING
) {

    fun getItemImagePath(): String {
        return "image/item/$itemImage"
    }

    fun getQuantityWeight(): String {
        return "จำนวน $quantity $weight"
    }
}