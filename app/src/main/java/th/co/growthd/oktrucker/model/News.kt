package th.co.growthd.oktrucker.model

import com.google.gson.annotations.SerializedName
import org.parceler.Parcel
import th.co.growthd.deerandbook.util.INITIAL_INT
import th.co.growthd.deerandbook.util.INITIAL_STRING

@Parcel(Parcel.Serialization.BEAN)
data class News(

        @SerializedName("id")
        var id: Int = INITIAL_INT,

        @SerializedName("title")
        var title: String = INITIAL_STRING,

        @SerializedName("short_content")
        var shortContent: String = INITIAL_STRING,

        @SerializedName("full_content")
        var fullContent: String = INITIAL_STRING,

        @SerializedName("is_pinned")
        var isPin: Int = INITIAL_INT,

        @SerializedName("posted_at")
        var postAt: String = INITIAL_STRING
)