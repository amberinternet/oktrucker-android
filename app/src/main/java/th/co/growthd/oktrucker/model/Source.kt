package th.co.growthd.oktrucker.model

import com.google.gson.annotations.SerializedName
import org.parceler.Parcel
import th.co.growthd.deerandbook.util.INITIAL_INT
import th.co.growthd.deerandbook.util.INITIAL_STRING
import th.co.growthd.oktrucker.fromDate
import th.co.growthd.oktrucker.toDateWithFullDayAppFormat

@Parcel(Parcel.Serialization.BEAN)
data class Source(

        @SerializedName("id")
        var id: Int = INITIAL_INT,

        @SerializedName("source_latitude")
        var latitude: String = INITIAL_STRING,

        @SerializedName("source_longitude")
        var longitude: String = INITIAL_STRING,

        @SerializedName("source_full_address")
        var address: String = INITIAL_STRING,

        @SerializedName("source_address_subdistrict")
        var subDistrict: String = INITIAL_STRING,

        @SerializedName("source_address_district")
        var district: String = INITIAL_STRING,

        @SerializedName("source_address_province")
        var province: String = INITIAL_STRING,

        @SerializedName("source_contact_name")
        var contactName: String = INITIAL_STRING,

        @SerializedName("source_contact_telephone_number")
        var contactTelephoneNumber: String = INITIAL_STRING,

        @SerializedName("source_shop_name")
        var companyName: String = INITIAL_STRING,

        @SerializedName("source_landmark")
        var landmark: String = INITIAL_STRING,

        @SerializedName("autograph")
        var autograph: String = INITIAL_STRING,

        @SerializedName("received_date")
        var date: String = INITIAL_STRING,

        @SerializedName("received_time")
        var time: String = INITIAL_STRING

) {
    fun getShortAddress(): String {
        return "$district, $province"
    }

    fun getFullAddress(): String {
        return "$address, $subDistrict, $district, $province"
    }

    fun getReadableTime(): String {
        return if (time == "เวลาใดก็ได้") "$time" else "$time น."
    }

    fun getReceiveDateTime(): String {
        return "${date.fromDate().toDateWithFullDayAppFormat()} | ${getReadableTime()}"
    }

    fun getReadableContactName(): String {
        return "ผู้ติดต่อ: $contactName"
    }
}