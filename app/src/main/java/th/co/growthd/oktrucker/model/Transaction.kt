package th.co.growthd.oktrucker.model

import com.google.gson.annotations.SerializedName
import org.parceler.Parcel

import th.co.growthd.deerandbook.util.INITIAL_INT
import th.co.growthd.deerandbook.util.INITIAL_STRING
import th.co.growthd.oktrucker.R
import th.co.growthd.oktrucker.roundDecimalFormat
import th.co.growthd.oktrucker.util.enum.DepositType
import th.co.growthd.oktrucker.util.enum.TransactionType

@Parcel(Parcel.Serialization.BEAN)
data class Transaction(

        @SerializedName("id")
        var id: Int = INITIAL_INT,

        @SerializedName("credit_log_type")
        var type: Int = INITIAL_INT,

        @SerializedName("description")
        var description: String = INITIAL_STRING,

        @SerializedName("old_credit_balance")
        var oldCreditBalance: String = INITIAL_STRING,

        @SerializedName("new_credit_balance")
        var newCreditBalance: String = INITIAL_STRING,

        @SerializedName("credit")
        var credit: String = INITIAL_STRING,

        @SerializedName("created_at")
        var createdAt: String = INITIAL_STRING,

        @SerializedName("work")
        var work: Work = Work(),

        @SerializedName("topup")
        var deposit: Deposit = Deposit(),

        @SerializedName("withdraw")
        var withdraw: Withdraw = Withdraw()
) {

    fun getReadableType(): String {
        return when (type) {
            TransactionType.DEPOSIT.value -> {
                when (deposit.type) {
                    DepositType.OMISE.value -> "<b>เติมเงิน</b> เลขที่คำขอ ${deposit.readableId}"
                    DepositType.TRANSFER.value -> "<b>เติมเงิน</b> เลขที่คำขอ ${deposit.readableId}"
                    else -> "ไม่ระบุ"
                }
            }
            TransactionType.CANCEL_WORK.value -> "<b>งานยกเลิก</b> เลขที่งาน #${work.readableWorkId}"
            TransactionType.ADMIN_ADD.value -> "<b>เพิ่ม</b>โดยแอดมิน"
            TransactionType.ADMIN_DELETE.value -> "<b>หัก</b>โดยแอดมิน"
            TransactionType.WITHDRAW.value -> "<b>ถอนเงิน</b> เลขที่คำขอ ${withdraw.readableId}"
            TransactionType.FINISH.value -> "<b>เสร็จงาน</b> เลขที่งาน #${work.readableWorkId}"
            TransactionType.ACCEPT_WORK.value -> "<b>รับงาน</b> เลขที่งาน #${work.readableWorkId}"
            else -> "ไม่ระบุ"
        }
    }

    fun getReadableCredit(): String {
        var credit = credit.toDouble().roundDecimalFormat()
        return when (type) {
            TransactionType.DEPOSIT.value, TransactionType.CANCEL_WORK.value, TransactionType.ADMIN_ADD.value, TransactionType.FINISH.value -> "+ ฿$credit"
            TransactionType.ADMIN_DELETE.value, TransactionType.WITHDRAW.value, TransactionType.ACCEPT_WORK.value -> "- ฿$credit"
            else -> "฿$credit"
        }
    }

    fun getCreditTextColor(): Int {
        return when (type) {
            TransactionType.DEPOSIT.value, TransactionType.CANCEL_WORK.value, TransactionType.ADMIN_ADD.value, TransactionType.FINISH.value -> R.color.colorTopUp
            TransactionType.ADMIN_DELETE.value, TransactionType.WITHDRAW.value, TransactionType.ACCEPT_WORK.value -> R.color.colorWithdraw
            else -> R.color.colorPrimaryText
        }
    }
}