package th.co.growthd.oktrucker.model

import com.google.gson.annotations.SerializedName
import org.parceler.Parcel
import th.co.growthd.deerandbook.util.INITIAL_INT
import th.co.growthd.deerandbook.util.INITIAL_STRING

@Parcel(Parcel.Serialization.BEAN)
data class Truck(

        @SerializedName("id")
        var id: Int = INITIAL_INT,

        @SerializedName("car_type_id")
        var truckTypeId: Int = INITIAL_INT,

        @SerializedName("license_plate")
        var licensePlate: String = INITIAL_STRING,

        var truckProvinceId: Int = INITIAL_INT,

        @SerializedName("car_province")
        var truckProvince: String = INITIAL_STRING,

        @SerializedName("car_year")
        var truckYear: String = INITIAL_STRING,

        @SerializedName("car_model")
        var truckModel: String = INITIAL_STRING,

        @SerializedName("car_brand")
        var truckBrand: String = INITIAL_STRING,

        @SerializedName("latitude")
        var latitude: String = INITIAL_STRING,

        @SerializedName("longitude")
        var longitude: String = INITIAL_STRING,

        @SerializedName("car_type")
        var truckType: TruckType = TruckType()
) {

    fun getBrandAndModel(): String {
        return "$truckBrand $truckModel"
    }

    override fun toString(): String {
        return "Truck(id=$id, truckTypeId=$truckTypeId, licensePlate='$licensePlate', truckProvinceId=$truckProvinceId, truckProvince='$truckProvince', truckYear='$truckYear', truckModel='$truckModel', truckBrand='$truckBrand', latitude='$latitude', longitude='$longitude', truckType=$truckType)"
    }
}