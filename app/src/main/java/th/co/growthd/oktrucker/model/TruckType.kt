package th.co.growthd.oktrucker.model

import com.google.gson.annotations.SerializedName
import org.parceler.Parcel
import th.co.growthd.deerandbook.util.INITIAL_INT
import th.co.growthd.deerandbook.util.INITIAL_STRING

@Parcel(Parcel.Serialization.BEAN)
data class TruckType(

        @SerializedName("id")
        var id: Int = INITIAL_INT,

        @SerializedName("car_type_name")
        var name: String = INITIAL_STRING,

        @SerializedName("car_type_image")
        var truckImage: String = INITIAL_STRING,

        var truckImageResId: Int = INITIAL_INT,

        @SerializedName("car_type_description")
        var description: String = INITIAL_STRING,

        @SerializedName("price_for_first_3_km")
        var priceFirst3Km: String = INITIAL_STRING,

        @SerializedName("price_per_km_at_3_15")
        var price3To15Km: String = INITIAL_STRING,

        @SerializedName("price_per_km_at_15_100")
        var price15To100Km: String = INITIAL_STRING,

        @SerializedName("price_per_km_at_100up")
        var price100UpKm: String = INITIAL_STRING

) {
    override fun toString(): String {
        return "TruckType(id=$id, name=$name, truckImage=$truckImage, description=$description, priceFirst3Km=$priceFirst3Km, price3To15Km=$price3To15Km, price15To100Km=$price15To100Km, price100UpKm=$price100UpKm)"
    }
}