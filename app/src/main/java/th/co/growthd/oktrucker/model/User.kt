package th.co.growthd.oktrucker.model

import android.util.Log
import com.google.gson.annotations.SerializedName
import org.parceler.Parcel
import th.co.growthd.deerandbook.util.INITIAL_INT
import th.co.growthd.deerandbook.util.INITIAL_STRING
import th.co.growthd.oktrucker.roundDecimalFormat

@Parcel(Parcel.Serialization.BEAN)
data class User(

        @SerializedName("id")
        var id: Int = INITIAL_INT,

        @SerializedName("first_name")
        var firstname: String = INITIAL_STRING,

        @SerializedName("last_name")
        var lastname: String = INITIAL_STRING,

        @SerializedName("username")
        var username: String = INITIAL_STRING,

        @SerializedName("email")
        var email: String = INITIAL_STRING,

        @SerializedName("profile_image")
        var profileImage: String = INITIAL_STRING,

        @SerializedName("current_address")
        var address: String = INITIAL_STRING,

        @SerializedName("telephone_number")
        var telephoneNumber: String = INITIAL_STRING,

        @SerializedName("credit_balance")
        var creditBalance: String = INITIAL_STRING,

        @SerializedName("is_verified")
        var isVerify: Int = INITIAL_INT,

        @SerializedName("formatted_user_id")
        var readableUserId: String = INITIAL_STRING,

        @SerializedName("driver")
        var driver: Driver = Driver(),

        var password: String = INITIAL_STRING,

        var confirmPassword: String = INITIAL_STRING,

        var isAcceptTerm: Boolean = false
) {

    private object Holder {
        val INSTANCE = User()
    }

    companion object {
        val instance: User by lazy { Holder.INSTANCE }
    }

    fun getFullName() : String {
        return "$firstname $lastname"
    }

    fun init(user: User) {
        instance.id = user.id
        instance.firstname = user.firstname
        instance.lastname = user.lastname
        instance.username = user.username
        instance.email = user.email
        instance.profileImage = user.profileImage
        instance.address = user.address
        instance.telephoneNumber = user.telephoneNumber
        instance.creditBalance = user.creditBalance
        instance.isVerify = user.isVerify
        instance.readableUserId = user.readableUserId
        instance.driver = user.driver
    }

    fun clear() {
        instance.id = INITIAL_INT
        instance.firstname = INITIAL_STRING
        instance.lastname = INITIAL_STRING
        instance.username = INITIAL_STRING
        instance.email = INITIAL_STRING
        instance.profileImage = INITIAL_STRING
        instance.address = INITIAL_STRING
        instance.telephoneNumber = INITIAL_STRING
        instance.creditBalance = INITIAL_STRING
        instance.isVerify = INITIAL_INT
        instance.readableUserId = INITIAL_STRING
        instance.driver = Driver()
        instance.password = INITIAL_STRING
        instance.confirmPassword = INITIAL_STRING
        instance.isAcceptTerm = false
    }

    fun getCredit() = creditBalance.toDouble().roundDecimalFormat()

    fun getReadableCredit(): String {
        var credit = getCredit()
        Log.d("sssssss", creditBalance.toDouble().toString())
        return "฿$credit"
    }

    override fun toString(): String {
        return "User(id=$id, firstname='$firstname', lastname='$lastname', username='$username', email='$email', profileImage='$profileImage', address='$address', telephoneNumber='$telephoneNumber', credit='$creditBalance', isVerify=$isVerify, readableUserId='$readableUserId', driver=$driver, password='$password', confirmPassword='$confirmPassword', isAcceptTerm=$isAcceptTerm)"
    }
}
