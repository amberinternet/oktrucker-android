package th.co.growthd.oktrucker.model

import com.google.gson.annotations.SerializedName
import org.parceler.Parcel

import th.co.growthd.deerandbook.util.INITIAL_INT
import th.co.growthd.deerandbook.util.INITIAL_STRING
import th.co.growthd.deerandbook.util.UNKNOWN
import th.co.growthd.oktrucker.R
import th.co.growthd.oktrucker.fromDateTime
import th.co.growthd.oktrucker.roundDecimalFormat
import th.co.growthd.oktrucker.toDateTimeAppFormat
import th.co.growthd.oktrucker.util.enum.TransactionRequestStatus

@Parcel(Parcel.Serialization.BEAN)
data class Withdraw(

        @SerializedName("id")
        var id: Int = INITIAL_INT,

        @SerializedName("price")
        var price: String = INITIAL_STRING,

        @SerializedName("status")
        var status: Int = INITIAL_INT,

        @SerializedName("approved_time")
        var approvedTime: String = INITIAL_STRING,

        @SerializedName("approved_note")
        var remark: String = INITIAL_STRING,

        @SerializedName("pay_slip_image")
        var transferSlip: String = INITIAL_STRING,

        @SerializedName("pay_time")
        var payTime: String = INITIAL_STRING,

        @SerializedName("formatted_withdraw_id")
        var readableId: String = INITIAL_STRING,

        @SerializedName("created_at")
        var createdAt: String = INITIAL_STRING
) {

    fun getCredit() = price.toDouble().roundDecimalFormat()

    fun getReadableCredit(): String {
        val credit = getCredit()
        return "- ฿$credit"
    }

    fun getStatusBackground(): Int {
        return when (status) {
            TransactionRequestStatus.WAITING.status -> R.drawable.background_status_waiting
            TransactionRequestStatus.APPROVED.status -> R.drawable.background_status_success
            TransactionRequestStatus.REJECT.status -> R.drawable.background_status_reject
            else -> R.drawable.background_status_waiting
        }
    }

    fun getStatusStyle(): Int {
        return when (status) {
            TransactionRequestStatus.WAITING.status -> R.style.TextView_Status_Waiting
            TransactionRequestStatus.APPROVED.status -> R.style.TextView_Status_Success
            TransactionRequestStatus.REJECT.status -> R.style.TextView_Status_Reject
            else -> R.drawable.background_status_reject
        }
    }

    fun getReadableStatus(): String {
        return when (status) {
            TransactionRequestStatus.WAITING.status -> TransactionRequestStatus.WAITING.withdrawMessage
            TransactionRequestStatus.APPROVED.status -> TransactionRequestStatus.APPROVED.withdrawMessage
            TransactionRequestStatus.REJECT.status -> TransactionRequestStatus.REJECT.withdrawMessage
            else -> UNKNOWN
        }
    }

    fun getReadableTransferDate(): String {
        return when (status) {
            TransactionRequestStatus.WAITING.status -> TransactionRequestStatus.WAITING.withdrawMessage
            TransactionRequestStatus.APPROVED.status -> payTime.fromDateTime().toDateTimeAppFormat()
            TransactionRequestStatus.REJECT.status -> TransactionRequestStatus.REJECT.withdrawMessage
            else -> UNKNOWN
        }
    }

    fun getSlipImagePath(): String {
        return "image/withdraw/pay-slip-image/$transferSlip"
    }
}