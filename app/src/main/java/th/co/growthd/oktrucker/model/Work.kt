package th.co.growthd.oktrucker.model

import com.google.gson.annotations.SerializedName
import org.parceler.Parcel
import th.co.growthd.deerandbook.util.INITIAL_DOUBLE
import th.co.growthd.deerandbook.util.INITIAL_INT
import th.co.growthd.deerandbook.util.INITIAL_STRING
import th.co.growthd.deerandbook.util.UNKNOWN
import th.co.growthd.oktrucker.roundDecimalFormat
import th.co.growthd.oktrucker.util.enum.PaymentMethod
import th.co.growthd.oktrucker.util.enum.WorkStatus

@Parcel(Parcel.Serialization.BEAN)
data class Work(

        @SerializedName("id")
        var id: Int = INITIAL_INT,

        @SerializedName("commission")
        var commission: Int = INITIAL_INT,

        @SerializedName("distance")
        var distance: Int = INITIAL_INT,

        @SerializedName("distance_price")
        var distancePrice: String = INITIAL_STRING,

        @SerializedName("full_price")
        var fullPrice: String = INITIAL_STRING,

        @SerializedName("status")
        var status: Int = INITIAL_INT,

        @SerializedName("payment_method")
        var paymentMethod: Int = INITIAL_INT,

        @SerializedName("user_name")
        var userFullName: String = INITIAL_STRING,

        @SerializedName("user_telephone_number")
        var userTelephoneNumber: String = INITIAL_STRING,

        @SerializedName("user_note")
        var remark: String = INITIAL_STRING,

        @SerializedName("is_driver_paid")
        var driverPaid: Int = INITIAL_INT,

        @SerializedName("formatted_work_id")
        var readableWorkId: String = INITIAL_STRING,

        @SerializedName("commission_price")
        var commissionPrice: Double = INITIAL_DOUBLE,

        @SerializedName("item")
        var item: Item = Item(),

        @SerializedName("car_type")
        var truckType: TruckType = TruckType(),

        @SerializedName("additional_services")
        var additionalServiceList: List<AdditionalService> = mutableListOf(),

        @SerializedName("source")
        var source: Source = Source(),

        @SerializedName("destination")
        var destination: Destination = Destination()
) {

    fun getReadableDistance(): String {
        val km = distance.toDouble() / 1000.0
        return "${km.roundDecimalFormat()} กม."
    }

    fun getReadablePaymentMethod(): String {
        return when (paymentMethod) {
            PaymentMethod.CASH_SOURCE.status -> PaymentMethod.CASH_SOURCE.pay
            PaymentMethod.CASH_DESTINATION.status -> PaymentMethod.CASH_DESTINATION.pay
            PaymentMethod.WALLET.status -> PaymentMethod.WALLET.pay
            PaymentMethod.OMISE.status -> PaymentMethod.OMISE.pay
            else -> UNKNOWN
        }
    }

    fun getReadableReceiveMethod(): String {
        return when (paymentMethod) {
            PaymentMethod.CASH_SOURCE.status -> PaymentMethod.CASH_SOURCE.receive
            PaymentMethod.CASH_DESTINATION.status -> PaymentMethod.CASH_DESTINATION.receive
            PaymentMethod.WALLET.status, PaymentMethod.OMISE.status -> PaymentMethod.WALLET.receive
            else -> UNKNOWN
        }
    }

    fun getReadableWorkStatus(): String {
        return when (status) {
            WorkStatus.PAYMENT_FAIL.status -> WorkStatus.PAYMENT_FAIL.readableStatus
            WorkStatus.FIND_TRUCK.status -> WorkStatus.FIND_TRUCK.readableStatus
            WorkStatus.WAITING_CONFIRM_WORK.status -> WorkStatus.WAITING_CONFIRM_WORK.readableStatus
            WorkStatus.WAITING_RECEIVE_ITEM.status -> WorkStatus.WAITING_RECEIVE_ITEM.readableStatus
            WorkStatus.ARRIVED_SOURCE.status -> WorkStatus.ARRIVED_SOURCE.readableStatus
            WorkStatus.WAITING_SENT_ITEM.status -> WorkStatus.WAITING_SENT_ITEM.readableStatus
            WorkStatus.ARRIVED_DESTINATION.status -> WorkStatus.ARRIVED_DESTINATION.readableStatus
            WorkStatus.COMPLETE.status -> WorkStatus.COMPLETE.readableStatus
            WorkStatus.CANCEL.status -> WorkStatus.CANCEL.readableStatus
            else -> UNKNOWN
        }
    }

    fun getReadableWorkAction(): String {
        return when (status) {
            WorkStatus.FIND_TRUCK.status -> WorkStatus.FIND_TRUCK.readableAction
            WorkStatus.WAITING_CONFIRM_WORK.status -> WorkStatus.WAITING_CONFIRM_WORK.readableAction
            WorkStatus.WAITING_RECEIVE_ITEM.status -> WorkStatus.WAITING_RECEIVE_ITEM.readableAction
            WorkStatus.ARRIVED_SOURCE.status -> WorkStatus.ARRIVED_SOURCE.readableAction
            WorkStatus.WAITING_SENT_ITEM.status -> WorkStatus.WAITING_SENT_ITEM.readableAction
            WorkStatus.ARRIVED_DESTINATION.status -> WorkStatus.ARRIVED_DESTINATION.readableAction
            WorkStatus.COMPLETE.status -> WorkStatus.COMPLETE.readableAction
            WorkStatus.CANCEL.status -> WorkStatus.CANCEL.readableAction
            else -> UNKNOWN
        }
    }

    fun getPrice() = fullPrice.toDouble().roundDecimalFormat()

    fun getReadablePrice(): String {
        val price = getPrice()
        return "฿$price"
    }

    fun getFee(): String {
        val commission = (User.instance.driver.driverType.commission * fullPrice.toDouble() / 100).roundDecimalFormat()
        return "$commission"
    }

    fun getFinalWage(): String {
        val commission = (fullPrice.toDouble() - User.instance.driver.driverType.commission * fullPrice.toDouble() / 100).roundDecimalFormat()
        return "$commission"
    }

    override fun toString(): String {
        return "Work(id=$id, commission=$commission, distance=$distance, distancePrice='$distancePrice', fullPrice='$fullPrice', status=$status, paymentMethod=$paymentMethod, userFullName='$userFullName', userTelephoneNumber='$userTelephoneNumber', remark='$remark', driverPaid=$driverPaid, readableWorkId='$readableWorkId', commissionPrice=$commissionPrice, item=$item, truckType=$truckType, additionalServiceList=$additionalServiceList, source=$source, destination=$destination)"
    }
}