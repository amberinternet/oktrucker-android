package th.co.growthd.oktrucker.models

import com.google.gson.annotations.SerializedName
import th.co.growthd.oktrucker.model.Deposit

data class DepositPagination(@SerializedName("data") var depositList: MutableList<Deposit>): Pagination() {

    override fun toString(): String {
        return "DepositPagination(depositList=$depositList)"
    }
}