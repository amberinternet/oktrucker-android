package th.co.growthd.oktrucker.models

import com.google.gson.annotations.SerializedName
import th.co.growthd.oktrucker.model.News

data class NewsPagination(@SerializedName("data") var newsList: MutableList<News>): Pagination() {

    override fun toString(): String {
        return "NewsPagination(newsList=$newsList)"
    }
}