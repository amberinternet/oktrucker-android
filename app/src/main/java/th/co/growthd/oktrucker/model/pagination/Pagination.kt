package th.co.growthd.oktrucker.models

import com.google.gson.annotations.SerializedName
import th.co.growthd.deerandbook.util.INITIAL_INT

abstract class Pagination(

        @SerializedName("total")
        var total: Int = INITIAL_INT,

        @SerializedName("current_page")
        var currentPage: Int = INITIAL_INT
) {
    override fun toString(): String {
        return "Pagination(total=$total, currentPage=$currentPage)"
    }
}