package th.co.growthd.oktrucker.models

import com.google.gson.annotations.SerializedName
import th.co.growthd.oktrucker.model.Transaction

data class TransactionPagination(@SerializedName("data") var transactionList: MutableList<Transaction>): Pagination() {

    override fun toString(): String {
        return "NewsPagination(newsList=$transactionList)"
    }
}