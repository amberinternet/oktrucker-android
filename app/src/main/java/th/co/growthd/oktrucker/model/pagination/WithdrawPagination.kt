package th.co.growthd.oktrucker.models

import com.google.gson.annotations.SerializedName
import th.co.growthd.oktrucker.model.Withdraw

data class WithdrawPagination(@SerializedName("data") var withdrawList: MutableList<Withdraw>): Pagination() {

    override fun toString(): String {
        return "WithdrawPagination(withdrawList=$withdrawList)"
    }
}