package th.co.growthd.oktrucker.models

import com.google.gson.annotations.SerializedName
import th.co.growthd.oktrucker.model.Work

data class WorkPagination(@SerializedName("data") var workList: MutableList<Work>): Pagination() {

    override fun toString(): String {
        return "WorkPagination(bookList=$workList)"
    }
}