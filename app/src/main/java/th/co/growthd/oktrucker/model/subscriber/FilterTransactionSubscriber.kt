package th.co.growthd.oktrucker.model.subscriber

import th.co.growthd.deerandbook.util.INITIAL_INT

data class FilterTransactionSubscriber(
        var month: Int = INITIAL_INT,
        var year: Int = INITIAL_INT
)