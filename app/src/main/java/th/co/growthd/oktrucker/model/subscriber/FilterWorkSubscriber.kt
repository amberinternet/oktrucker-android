package th.co.growthd.oktrucker.model.subscriber

import th.co.growthd.deerandbook.util.INITIAL_STRING

data class FilterWorkSubscriber(
        var province: String = INITIAL_STRING
)