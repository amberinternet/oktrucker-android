package th.co.growthd.oktrucker.model.subscriber

import th.co.growthd.deerandbook.util.INITIAL_DOUBLE

data class LocationSubscriber(
        var latitude: Double = INITIAL_DOUBLE,
        var longitude: Double = INITIAL_DOUBLE
)