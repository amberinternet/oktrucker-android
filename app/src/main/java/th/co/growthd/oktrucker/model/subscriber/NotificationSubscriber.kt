package th.co.growthd.oktrucker.model.subscriber

import th.co.growthd.deerandbook.util.INITIAL_STRING

data class NotificationSubscriber(
        var message: String = INITIAL_STRING
)