package th.co.growthd.oktrucker.model.subscriber

data class PermissionSubscriber(
        var isEnable: Boolean
)