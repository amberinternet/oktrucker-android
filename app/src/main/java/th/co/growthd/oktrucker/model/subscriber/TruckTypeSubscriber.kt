package th.co.growthd.oktrucker.model.subscriber

import th.co.growthd.oktrucker.model.TruckType

data class TruckTypeSubscriber(
        var truckType: TruckType = TruckType()
)