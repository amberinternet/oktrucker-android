package th.co.growthd.oktrucker.ui.activity

import android.databinding.DataBindingUtil
import android.os.Bundle
import th.co.growthd.oktrucker.R
import th.co.growthd.oktrucker.databinding.AboutApplicationActivityBinding
import th.co.growthd.oktrucker.ui.base.BaseActivity
import th.co.growthd.oktrucker.ui.fragment.AboutApplicationFragment

class AboutApplicationActivity: BaseActivity() {

    lateinit var binding: AboutApplicationActivityBinding

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = DataBindingUtil.setContentView(this, R.layout.about_application_activity)
        supportFragmentManager.beginTransaction()
                .replace(R.id.about_application_container, AboutApplicationFragment.newInstance())
                .commitNowAllowingStateLoss()
    }
}