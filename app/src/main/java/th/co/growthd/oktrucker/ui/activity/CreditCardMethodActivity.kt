package th.co.growthd.oktrucker.ui.activity

import android.databinding.DataBindingUtil
import android.os.Bundle
import th.co.growthd.deerandbook.util.PRICE_KEY
import th.co.growthd.oktrucker.R
import th.co.growthd.oktrucker.databinding.CreditCardMethodActivityBinding
import th.co.growthd.oktrucker.ui.base.BaseActivity
import th.co.growthd.oktrucker.ui.fragment.CreditCardMethodFragment

class CreditCardMethodActivity: BaseActivity() {

    lateinit var binding: CreditCardMethodActivityBinding
    lateinit var price: String

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        getVariable()
        binding = DataBindingUtil.setContentView(this, R.layout.credit_card_method_activity)
        supportFragmentManager
                .beginTransaction()
                .replace(R.id.credit_card_method_container, CreditCardMethodFragment.newInstance(price))
                .commitNowAllowingStateLoss()
    }

    private fun getVariable() {
        intent.extras?.let {
            price = it.getString(PRICE_KEY)
        }
    }
}