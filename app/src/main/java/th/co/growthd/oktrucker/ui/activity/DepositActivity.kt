package th.co.growthd.oktrucker.ui.activity

import android.databinding.DataBindingUtil
import android.os.Bundle
import th.co.growthd.oktrucker.R
import th.co.growthd.oktrucker.databinding.DepositActivityBinding
import th.co.growthd.oktrucker.ui.base.BaseActivity
import th.co.growthd.oktrucker.ui.fragment.DepositFragment

class DepositActivity: BaseActivity() {

    lateinit var binding: DepositActivityBinding

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = DataBindingUtil.setContentView(this, R.layout.deposit_activity)
        supportFragmentManager
                .beginTransaction()
                .replace(R.id.deposit_container, DepositFragment.newInstance())
                .commitNowAllowingStateLoss()
    }
}