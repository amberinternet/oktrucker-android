package th.co.growthd.oktrucker.ui.activity

import android.content.Intent
import android.databinding.DataBindingUtil
import android.os.Bundle
import th.co.growthd.oktrucker.R
import th.co.growthd.oktrucker.databinding.ExplanationActivityBinding
import th.co.growthd.oktrucker.ui.base.BaseActivity

class ExplanationActivity : BaseActivity() {

    lateinit var binding: ExplanationActivityBinding

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = DataBindingUtil.setContentView(this, R.layout.explanation_activity)
        initOnClick()
    }

    private fun initOnClick() {
        binding.toolBar!!.titleTextView.setText(R.string.explanation)
        binding.toolBar!!.backImageView.setOnClickListener {
            onBackPressed()
        }
        binding.cancelTextView.setOnClickListener {
            onBackPressed()
        }
        binding.agreeTextView.setOnClickListener {
            val intent = Intent(this, SignUpActivity::class.java)
            startActivity(intent)
        }
    }
}