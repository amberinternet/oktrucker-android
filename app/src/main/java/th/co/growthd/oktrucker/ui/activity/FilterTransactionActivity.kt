package th.co.growthd.oktrucker.ui.activity

import android.databinding.DataBindingUtil
import android.os.Bundle
import th.co.growthd.deerandbook.util.INITIAL_INT
import th.co.growthd.deerandbook.util.MONTH_KEY
import th.co.growthd.deerandbook.util.YEAR_KEY
import th.co.growthd.oktrucker.R
import th.co.growthd.oktrucker.databinding.FilterTransactionActivityBinding
import th.co.growthd.oktrucker.ui.base.BaseActivity
import th.co.growthd.oktrucker.ui.fragment.FilterTransactionFragment

class FilterTransactionActivity : BaseActivity() {

    lateinit var binding: FilterTransactionActivityBinding
    var month: Int = INITIAL_INT
    var year: Int = INITIAL_INT

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = DataBindingUtil.setContentView(this, R.layout.filter_transaction_activity)
        getVariable()
        supportFragmentManager
                .beginTransaction()
                .replace(R.id.filter_transaction_container, FilterTransactionFragment.newInstance(month, year))
                .commitNowAllowingStateLoss()
    }

    private fun getVariable() {
        month = intent.extras.getInt(MONTH_KEY)
        year = intent.extras.getInt(YEAR_KEY)
    }
}