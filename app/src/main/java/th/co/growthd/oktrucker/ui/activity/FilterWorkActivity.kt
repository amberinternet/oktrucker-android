package th.co.growthd.oktrucker.ui.activity

import android.databinding.DataBindingUtil
import android.os.Bundle
import th.co.growthd.deerandbook.util.PROVINCE_KEY
import th.co.growthd.oktrucker.R
import th.co.growthd.oktrucker.databinding.FilterWorkActivityBinding
import th.co.growthd.oktrucker.ui.base.BaseActivity
import th.co.growthd.oktrucker.ui.fragment.FilterWorkFragment

class FilterWorkActivity : BaseActivity() {

    lateinit var binding: FilterWorkActivityBinding
    lateinit var province: String

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = DataBindingUtil.setContentView(this, R.layout.filter_work_activity)
        getVariable()
        supportFragmentManager
                .beginTransaction()
                .replace(R.id.filter_work_container, FilterWorkFragment.newInstance(province))
                .commitNowAllowingStateLoss()
    }

    private fun getVariable() {
        province = intent.extras.getString(PROVINCE_KEY)
    }
}