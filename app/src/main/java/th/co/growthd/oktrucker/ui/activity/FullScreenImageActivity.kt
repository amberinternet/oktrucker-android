package th.co.growthd.oktrucker.ui.activity

import android.databinding.DataBindingUtil
import android.os.Bundle
import th.co.growthd.deerandbook.util.IMAGE_NAME_KEY
import th.co.growthd.deerandbook.util.IMAGE_PATH_KEY
import th.co.growthd.deerandbook.util.INITIAL_STRING
import th.co.growthd.oktrucker.R
import th.co.growthd.oktrucker.databinding.FullScreenImageActivityBinding
import th.co.growthd.oktrucker.setImageUrl
import th.co.growthd.oktrucker.ui.base.BaseActivity
import th.co.growthd.oktrucker.util.manager.UserManager

class FullScreenImageActivity : BaseActivity() {

    lateinit var binding: FullScreenImageActivityBinding
    lateinit var imagePath: String
    lateinit var imageName: String

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = DataBindingUtil.setContentView(this, R.layout.full_screen_image_activity)
        getVariable()
        initView()
    }

    private fun getVariable() {
        imagePath = intent.extras.getString(IMAGE_PATH_KEY)
        imageName = intent.extras.getString(IMAGE_NAME_KEY, INITIAL_STRING)
    }

    private fun initView() {
        binding.photoView.setImageUrl(imagePath, UserManager.getInstance(this).token)
        binding.backImageView.setOnClickListener {
            finish()
        }
    }
}