package th.co.growthd.oktrucker.ui.activity

import android.arch.lifecycle.ViewModelProviders
import android.content.Intent
import android.databinding.DataBindingUtil
import android.os.Bundle
import android.os.Handler
import android.support.design.widget.BottomSheetBehavior
import android.support.design.widget.BottomSheetDialog
import android.support.v4.content.ContextCompat
import android.view.View
import android.widget.Button
import th.co.growthd.oktrucker.databinding.LogInActivityBinding
import th.co.growthd.oktrucker.ui.base.BaseActivity
import th.co.growthd.oktrucker.util.manager.DialogManager
import android.widget.Toast
import com.google.android.gms.common.ConnectionResult
import com.google.android.gms.common.GoogleApiAvailability
import com.klinker.android.link_builder.Link
import com.klinker.android.link_builder.applyLinks
import th.co.growthd.deerandbook.ui.factory.LogInViewModelFactory
import th.co.growthd.oktrucker.*
import th.co.growthd.oktrucker.ui.viewmodel.LogInViewModel

class LogInActivity : BaseActivity() {

    private var doubleTapToExit = false
    private lateinit var binding: LogInActivityBinding
    private lateinit var dialogManager: DialogManager
    private lateinit var viewModel: LogInViewModel

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = DataBindingUtil.setContentView(this, R.layout.log_in_activity)
        viewModel = ViewModelProviders.of(this, LogInViewModelFactory(this)).get(LogInViewModel::class.java)
        viewModel.logInSuccess.addOnPropertyChanged {
            if (it.get()) {
                startTabBarActivity()
            }
        }
        checkGooglePlayService()
        initView()
        initVariable()
        initOnClick()
    }

    private fun checkGooglePlayService() {
        if (!isGooglePlayServicesAvailable()) {
            dialogManager.showError(getString(R.string.update_google_play_services))
        }
    }

    private fun isGooglePlayServicesAvailable(): Boolean {
        val googleApiAvailability = GoogleApiAvailability.getInstance()
        val status = googleApiAvailability.isGooglePlayServicesAvailable(this)
        if (status != ConnectionResult.SUCCESS) {
            if (googleApiAvailability.isUserResolvableError(status)) {
                googleApiAvailability.getErrorDialog(this, status, 2404).show()
            }
            return false
        }
        return true
    }

    private fun initView() {
        val termLink = Link("ข้อตกลงและเงื่อนไขการใช้บริการ").apply {
            textColor = ContextCompat.getColor(this@LogInActivity, R.color.colorWhitePress)
            underlined = true
            setOnClickListener {
                val intent = Intent(this@LogInActivity, TermAndConditionActivity::class.java)
                startActivity(intent)
            }
        }

        val privacyLink = Link("นโยบายความเป็นส่วนตัว").apply {
            textColor = ContextCompat.getColor(this@LogInActivity, R.color.colorWhitePress)
            underlined = true
            setOnClickListener {
                val intent = Intent(this@LogInActivity, PrivacyPolicyActivity::class.java)
                startActivity(intent)
            }
        }

        binding.acceptTextView.applyLinks(termLink, privacyLink)
    }

    private fun initVariable() {
        dialogManager = DialogManager.getInstance(supportFragmentManager)
    }

    private fun initOnClick() {
        binding.logInButton.setOnClickListener {
            if (validate()) {
                logInWithTelephone()
            }
        }
        binding.signUpButton.setOnClickListener {
            val intent = Intent(this, ExplanationActivity::class.java)
            startActivity(intent)
        }
        binding.forgotPasswordTextView.setOnClickListener {
            DialogManager.getInstance(supportFragmentManager).showContactAdmin(R.string.please_contact_admin, R.string.request_new_password)
        }
    }

    private fun validate(): Boolean {
        return when {
            binding.telephoneEditText.isEmpty() -> {
                dialogManager.showAlert(R.string.please_fill_out_telephone)
                false
            }
            !binding.telephoneEditText.isValidTelephone() -> {
                dialogManager.showAlert(R.string.invalid_telephone)
                false
            }

            binding.passwordEditText.isEmpty() -> {
                dialogManager.showAlert(R.string.please_fill_out_password)
                false
            }

            else -> true
        }
    }

    private fun logInWithTelephone() {
        viewModel.getFirebaseToken { token ->
            subscription = viewModel.logInWithTelephone(binding.telephoneEditText.string(), binding.passwordEditText.string(), token)
        }
    }

    private fun startTabBarActivity() {
        val intent = Intent(this, TabBarActivity::class.java)
        startActivity(intent)
        finishAffinity()
    }

    override fun onBackPressed() {
        if (doubleTapToExit) {
            finishAffinity()
        } else {
            doubleTapToExit = true
            Toast.makeText(this, R.string.press_again_to_exit_application, Toast.LENGTH_SHORT).show()
            Handler().postDelayed({ doubleTapToExit = false }, 2000)
        }
    }
}