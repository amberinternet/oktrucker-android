package th.co.growthd.oktrucker.ui.activity

import android.databinding.DataBindingUtil
import android.os.Bundle
import org.parceler.Parcels
import th.co.growthd.deerandbook.util.NEWS_KEY
import th.co.growthd.oktrucker.R
import th.co.growthd.oktrucker.databinding.NewsDetailActivityBinding
import th.co.growthd.oktrucker.model.News
import th.co.growthd.oktrucker.ui.base.BaseActivity
import th.co.growthd.oktrucker.ui.fragment.NewsDetailFragment

class NewsDetailActivity: BaseActivity() {

    lateinit var binding: NewsDetailActivityBinding
    lateinit var news: News

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = DataBindingUtil.setContentView(this, R.layout.news_detail_activity)
        getVariable()
        supportFragmentManager.beginTransaction()
                .replace(R.id.news_detail_container, NewsDetailFragment.newInstance(news))
                .commitNowAllowingStateLoss()
    }

    fun getVariable() {
        intent.extras?.let {
            news = Parcels.unwrap(it.getParcelable(NEWS_KEY))
        }
    }
}