package th.co.growthd.oktrucker.ui.activity

import android.databinding.DataBindingUtil
import android.os.Bundle
import th.co.growthd.deerandbook.util.PRICE_KEY
import th.co.growthd.oktrucker.R
import th.co.growthd.oktrucker.databinding.PaymentMethodActivityBinding
import th.co.growthd.oktrucker.ui.base.BaseActivity
import th.co.growthd.oktrucker.ui.fragment.PaymentMethodFragment

class PaymentMethodActivity: BaseActivity() {

    lateinit var binding: PaymentMethodActivityBinding
    lateinit var price: String

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        getVariable()
        binding = DataBindingUtil.setContentView(this, R.layout.payment_method_activity)
        supportFragmentManager
                .beginTransaction()
                .replace(R.id.payment_method_container, PaymentMethodFragment.newInstance(price))
                .commitNowAllowingStateLoss()
    }

    private fun getVariable() {
        intent.extras?.let {
            price = it.getString(PRICE_KEY)
        }
    }
}