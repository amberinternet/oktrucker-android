package th.co.growthd.oktrucker.ui.activity

import android.databinding.DataBindingUtil
import android.os.Bundle
import th.co.growthd.oktrucker.R
import th.co.growthd.oktrucker.databinding.PrivacyPolicyActivityBinding
import th.co.growthd.oktrucker.ui.base.BaseActivity
import th.co.growthd.oktrucker.ui.fragment.PrivacyPolicyFragment

class PrivacyPolicyActivity: BaseActivity() {

    lateinit var binding: PrivacyPolicyActivityBinding

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = DataBindingUtil.setContentView(this, R.layout.privacy_policy_activity)
        supportFragmentManager.beginTransaction()
                .replace(R.id.privacy_policy_container, PrivacyPolicyFragment.newInstance())
                .commitNowAllowingStateLoss()
    }
}