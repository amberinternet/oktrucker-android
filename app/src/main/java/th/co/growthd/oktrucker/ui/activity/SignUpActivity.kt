package th.co.growthd.oktrucker.ui.activity

import android.databinding.DataBindingUtil
import android.os.Bundle
import android.support.v4.app.Fragment
import org.greenrobot.eventbus.EventBus
import th.co.growthd.oktrucker.R
import th.co.growthd.oktrucker.databinding.SignUpActivityBinding
import th.co.growthd.oktrucker.model.subscriber.SignUpSubscriber
import th.co.growthd.oktrucker.ui.base.BaseActivity
import th.co.growthd.oktrucker.ui.fragment.SignUp1Fragment
import th.co.growthd.oktrucker.ui.fragment.SignUp2Fragment
import th.co.growthd.oktrucker.ui.fragment.SignUp3Fragment
import th.co.growthd.oktrucker.ui.fragment.SignUpFragment

class SignUpActivity : BaseActivity() {

    lateinit var binding: SignUpActivityBinding

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = DataBindingUtil.setContentView(this, R.layout.sign_up_activity)
        supportFragmentManager.beginTransaction()
                .replace(R.id.sign_up_container, SignUpFragment.newInstance(), SignUpFragment::class.java.simpleName)
                .commitAllowingStateLoss()
    }

    override fun onBackPressed() {
        var fragment: Fragment = supportFragmentManager.findFragmentByTag(SignUpFragment::class.java.simpleName)!!
        when {
            fragment.childFragmentManager.findFragmentByTag(SignUp1Fragment::class.java.simpleName) != null -> {
                super.onBackPressed()
            }
            fragment.childFragmentManager.findFragmentByTag(SignUp2Fragment::class.java.simpleName) != null -> {
                EventBus.getDefault().post(SignUpSubscriber(1))
            }
            fragment.childFragmentManager.findFragmentByTag(SignUp3Fragment::class.java.simpleName) != null -> {
                EventBus.getDefault().post(SignUpSubscriber(2))
            }
        }
    }
}