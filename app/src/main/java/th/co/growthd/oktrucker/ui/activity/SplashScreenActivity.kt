package th.co.growthd.oktrucker.ui.activity

import android.Manifest
import android.content.Intent
import android.databinding.DataBindingUtil
import android.graphics.drawable.AnimationDrawable
import android.os.Bundle
import android.os.Handler
import android.support.v4.content.ContextCompat
import android.support.v7.app.AppCompatActivity
import android.view.View
import rx.Observable
import th.co.growthd.deerandbook.util.REQUEST_CODE_LOCATION_PERMISSION
import th.co.growthd.oktrucker.R
import th.co.growthd.oktrucker.databinding.SplashScreenActivityBinding
import th.co.growthd.oktrucker.model.User
import th.co.growthd.oktrucker.ui.base.BaseActivity
import th.co.growthd.oktrucker.util.manager.UserManager
import th.co.growthd.oktrucker.util.manager.DialogManager
import th.co.growthd.oktrucker.util.network.RxNetwork
import th.co.growthd.oktrucker.util.network.response.UserResponse
import th.co.growthd.oktrucker.util.network.service.UserService

class SplashScreenActivity : BaseActivity() {

    lateinit var binding: SplashScreenActivityBinding
    lateinit var dialogManager: DialogManager
    lateinit var userManager: UserManager

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = DataBindingUtil.setContentView(this, R.layout.splash_screen_activity)
        initVariable()
        initAccount()
    }

    private fun initVariable() {
        userManager = UserManager(this)
        dialogManager = DialogManager.getInstance(supportFragmentManager)
    }

    private fun initAccount() {
        if (userManager.hasToken()) {
            binding.loadingImageView.visibility = View.VISIBLE
            val frameAnimation = binding.loadingImageView.drawable as AnimationDrawable
            frameAnimation.start()
            refreshTokenWithStartMainActivity()
        } else {
            binding.loadingImageView.visibility = View.GONE
            startLogInActivity()
        }
    }

    private fun refreshTokenWithStartMainActivity() {
        var userService = UserService.getInstance(this)
        val observable: Observable<UserResponse> = userService.refreshToken().flatMap {
            UserManager.getInstance(applicationContext).storeAccessToken(it.accessToken)
            return@flatMap userService.getProfile()
        }

        subscription = RxNetwork<UserResponse>(this).request(observable, onSuccess = { response ->
            User.instance.init(response.user)
            if (response.user.isVerify == 1) {
                startTabBarActivity()
            } else {
                dialogManager.showAlert(R.string.alert_user_not_verify)
            }
        }, onFailure = {
            startLogInActivity()
        })
    }

    private fun startTabBarActivity() {
        Handler().postDelayed({
            val intent = Intent(this, TabBarActivity::class.java)
            startActivity(intent)
            finishAffinity()
        }, 2000)
    }

    private fun startLogInActivity() {
        Handler().postDelayed({
            val intent = Intent(this, LogInActivity::class.java)
            startActivity(intent)
            finishAffinity()
        }, 2000)
    }
}