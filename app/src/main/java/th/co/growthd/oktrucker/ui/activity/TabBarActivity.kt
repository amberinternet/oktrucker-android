package th.co.growthd.oktrucker.ui.activity

import android.Manifest
import android.annotation.TargetApi
import android.app.PendingIntent
import android.arch.lifecycle.ViewModelProviders
import android.content.pm.PackageManager
import android.databinding.DataBindingUtil
import android.os.Build
import android.os.Bundle
import android.os.Handler
import android.support.v4.app.ActivityCompat
import android.support.v4.app.Fragment
import android.util.Log
import android.widget.Toast
import th.co.growthd.deerandbook.util.REQUEST_CODE_LOCATION_PERMISSION
import th.co.growthd.oktrucker.R
import th.co.growthd.oktrucker.databinding.TabBarActivityBinding
import th.co.growthd.oktrucker.ui.base.BaseActivity
import th.co.growthd.oktrucker.ui.fragment.*
import th.co.growthd.oktrucker.util.BottomNavigationViewHelper
import th.co.growthd.oktrucker.util.manager.DialogManager
import android.content.Intent
import android.net.Uri
import android.provider.Settings
import android.location.LocationManager
import com.google.android.gms.location.LocationRequest
import com.google.android.gms.location.LocationServices
import com.google.android.gms.location.LocationSettingsRequest
import org.greenrobot.eventbus.EventBus
import org.greenrobot.eventbus.Subscribe
import th.co.growthd.deerandbook.ui.factory.TabBarViewModelFactory
import th.co.growthd.deerandbook.util.NAVIGATION_TAB_KEY
import th.co.growthd.deerandbook.util.REQUEST_CODE_GPS_SERVICE
import th.co.growthd.oktrucker.model.subscriber.LocationSubscriber
import th.co.growthd.oktrucker.model.subscriber.PermissionSubscriber
import th.co.growthd.oktrucker.ui.viewmodel.TabBarViewModel
import th.co.growthd.oktrucker.util.location.LocationBroadcastReceiver

class TabBarActivity : BaseActivity() {

    private lateinit var viewModel: TabBarViewModel
    private lateinit var dialogManager: DialogManager
    private var doubleTapToExit = false
    lateinit var binding: TabBarActivityBinding

    /* location */
    private lateinit var locationSettingsRequest: LocationSettingsRequest
    private lateinit var locationRequest: LocationRequest
    private val UPDATE_INTERVAL_TIME = (5 * 60 * 1000).toLong()
    private val FASTEST_UPDATE_INTERVAL_TIME = (1 * 1 * 1000).toLong()

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = DataBindingUtil.setContentView(this, R.layout.tab_bar_activity)
        viewModel = ViewModelProviders.of(this, TabBarViewModelFactory(this)).get(TabBarViewModel::class.java)
        EventBus.getDefault().register(this)
        initVariable()
        initNavigationView()
        initView()
        subscribeToLocationUpdate()
    }

    override fun onDestroy() {
        EventBus.getDefault().unregister(this)
        super.onDestroy()
    }

    private fun initVariable() {
        dialogManager = DialogManager.getInstance(supportFragmentManager)
    }

    private fun initView() {
        val selectedTab = intent.getIntExtra(NAVIGATION_TAB_KEY, R.id.tab_bar_my_work)
        when (selectedTab) {
            R.id.tab_bar_find_work -> binding.navigation.selectedItemId = selectedTab
            R.id.tab_bar_news -> binding.navigation.selectedItemId = selectedTab
            R.id.tab_bar_my_work -> binding.navigation.selectedItemId = selectedTab
            R.id.tab_bar_wallet -> binding.navigation.selectedItemId = selectedTab
            R.id.tab_bar_setting -> binding.navigation.selectedItemId = selectedTab
        }
    }

    private fun checkPermission(): Boolean {
        return if (ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_COARSE_LOCATION) == PackageManager.PERMISSION_GRANTED) {
            true
        } else {
            requestPermission()
            false
        }
    }

    private fun requestPermission() {
        ActivityCompat.requestPermissions(this, arrayOf(Manifest.permission.ACCESS_COARSE_LOCATION, Manifest.permission.ACCESS_FINE_LOCATION), REQUEST_CODE_LOCATION_PERMISSION)
    }

    @TargetApi(Build.VERSION_CODES.M)
    override fun onRequestPermissionsResult(requestCode: Int, permissions: Array<String>, grantResults: IntArray) {
        when (requestCode) {
            REQUEST_CODE_LOCATION_PERMISSION -> {
                if (shouldShowRequestPermissionRationale(Manifest.permission.ACCESS_COARSE_LOCATION)) {
                    if (grantResults[0] == PackageManager.PERMISSION_DENIED) {
                        dialogManager?.showConfirm(R.string.alert, R.string.enable_location_permission, onConfirm = {
                            subscribeToLocationUpdate()
                        }, onCancel = {
                            finish()
                        })
                    } else {
                        finish()
                    }
                } else {
                    if (grantResults[0] == PackageManager.PERMISSION_DENIED) {
                        dialogManager?.showAlert(R.string.alert, R.string.enable_location_permission, R.string.ok) {
                            val intent = Intent(Settings.ACTION_APPLICATION_DETAILS_SETTINGS)
                            val uri = Uri.fromParts("package", packageName, null)
                            intent.data = uri
                            startActivityForResult(intent, REQUEST_CODE_LOCATION_PERMISSION)
                        }
                    } else {
                        EventBus.getDefault().post(PermissionSubscriber(true))
                    }
                }
            }
            else -> super.onRequestPermissionsResult(requestCode, permissions, grantResults)
        }
    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)
        if (requestCode == REQUEST_CODE_LOCATION_PERMISSION) {
            subscribeToLocationUpdate()
        }
        if (requestCode == REQUEST_CODE_GPS_SERVICE) {
            subscribeToLocationUpdate()
        }
    }

    private fun checkLocationService(): Boolean {
        val lm = getSystemService(LOCATION_SERVICE) as? LocationManager
        lm?.let {
            var isGpsEnabled = it.isProviderEnabled(LocationManager.GPS_PROVIDER)
            var isNetworkEnabled = it.isProviderEnabled(LocationManager.NETWORK_PROVIDER)
            if (!isGpsEnabled && !isNetworkEnabled) {
                dialogManager?.showConfirm(R.string.gps_not_enable, R.string.open_location_settings, onConfirm = {
                    val intent = Intent(Settings.ACTION_LOCATION_SOURCE_SETTINGS)
                    startActivityForResult(intent, REQUEST_CODE_GPS_SERVICE)
                }, onCancel = {
                    subscribeToLocationUpdate()
                })

                return false
            }
        }
        return true
    }

    private fun subscribeToLocationUpdate() {
        if (checkPermission() && checkLocationService()) {
            Log.d("ssssss" ,"start update")
            locationRequest = LocationRequest.create().apply {
                priority = LocationRequest.PRIORITY_HIGH_ACCURACY
                fastestInterval = FASTEST_UPDATE_INTERVAL_TIME
                interval = UPDATE_INTERVAL_TIME
            }

            locationSettingsRequest = LocationSettingsRequest.Builder().addLocationRequest(locationRequest).build()
            LocationServices.getSettingsClient(this).checkLocationSettings(locationSettingsRequest)

            val broadcastIntent = Intent(this, LocationBroadcastReceiver::class.java)
            val pendingIntent = PendingIntent.getBroadcast(this, 0, broadcastIntent, PendingIntent.FLAG_UPDATE_CURRENT)
            LocationServices.getFusedLocationProviderClient(this).requestLocationUpdates(locationRequest, pendingIntent)
        }
    }

    private fun initNavigationView() {
        binding.navigation.itemIconTintList = null
        BottomNavigationViewHelper.disableShiftMode(binding.navigation)
        BottomNavigationViewHelper.setIconSize(binding.navigation, 33, 23, 3.5f, this)
//        BottomNavigationViewHelper.setLabelPadding(binding.navigation, 6f, this)
        binding.navigation.setOnNavigationItemSelectedListener {
            when (it.itemId) {
                R.id.tab_bar_find_work -> replaceFragment(FindWorkFragment.newInstance())
                R.id.tab_bar_news -> replaceFragment(NewsFragment.newInstance())
                R.id.tab_bar_my_work -> replaceFragment(MyWorkFragment.newInstance())
                R.id.tab_bar_wallet -> replaceFragment(WalletFragment.newInstance())
                R.id.tab_bar_setting -> replaceFragment(SettingFragment.newInstance())
            }

            true
        }
    }

    private fun replaceFragment(fragment: Fragment) {
        supportFragmentManager.beginTransaction()
                .replace(R.id.tab_bar_container, fragment)
                .commitNowAllowingStateLoss()
    }

    override fun onBackPressed() {
        if (doubleTapToExit) {
            super.onBackPressed()
        } else {
            doubleTapToExit = true
            Toast.makeText(this, R.string.press_again_to_exit_application, Toast.LENGTH_SHORT).show()
            Handler().postDelayed({ doubleTapToExit = false }, 2000)
        }
    }

    @Subscribe
    fun onUpdateLocation(subscriber: LocationSubscriber) {
        Log.d("fetch & update location", "${subscriber?.latitude} , ${subscriber?.longitude}")
        viewModel.updateLocation(subscriber.latitude, subscriber.longitude)
    }
}