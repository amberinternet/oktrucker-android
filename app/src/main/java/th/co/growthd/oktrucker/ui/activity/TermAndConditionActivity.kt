package th.co.growthd.oktrucker.ui.activity

import android.databinding.DataBindingUtil
import android.os.Bundle
import th.co.growthd.oktrucker.R
import th.co.growthd.oktrucker.databinding.TermAndConditionActivityBinding
import th.co.growthd.oktrucker.ui.base.BaseActivity
import th.co.growthd.oktrucker.ui.fragment.TermAndConditionFragment

class TermAndConditionActivity: BaseActivity() {

    lateinit var binding: TermAndConditionActivityBinding

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = DataBindingUtil.setContentView(this, R.layout.term_and_condition_activity)
        supportFragmentManager.beginTransaction()
                .replace(R.id.term_and_condition_container, TermAndConditionFragment.newInstance())
                .commitNowAllowingStateLoss()
    }
}