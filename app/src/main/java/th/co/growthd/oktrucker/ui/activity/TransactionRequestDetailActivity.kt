package th.co.growthd.oktrucker.ui.activity

import android.databinding.DataBindingUtil
import android.os.Bundle
import org.parceler.Parcels
import th.co.growthd.deerandbook.util.DEPOSIT_KEY
import th.co.growthd.deerandbook.util.TRANSACTION_TYPE_KEY
import th.co.growthd.deerandbook.util.WITHDRAW_KEY
import th.co.growthd.oktrucker.R
import th.co.growthd.oktrucker.databinding.TransactionRequestDetailActivityBinding
import th.co.growthd.oktrucker.model.Deposit
import th.co.growthd.oktrucker.model.Withdraw
import th.co.growthd.oktrucker.ui.base.BaseActivity
import th.co.growthd.oktrucker.ui.fragment.TransactionRequestDetailFragment
import th.co.growthd.oktrucker.util.enum.TransactionType

class TransactionRequestDetailActivity: BaseActivity() {

    lateinit var binding: TransactionRequestDetailActivityBinding
    lateinit var fragment: TransactionRequestDetailFragment
    private var deposit: Deposit? = null
    private var withdraw: Withdraw? = null
    private var transactionType: Int = 0

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        getVariable()
        binding = DataBindingUtil.setContentView(this, R.layout.transaction_request_detail_activity)
        supportFragmentManager.beginTransaction()
                .replace(R.id.transaction_request_detail_container, fragment)
                .commitNowAllowingStateLoss()
    }

    private fun getVariable() {
        intent.extras?.let {
            transactionType = it.getInt(TRANSACTION_TYPE_KEY)
            if (transactionType == TransactionType.DEPOSIT.spinnerValue) {
                deposit = Parcels.unwrap(it.getParcelable(DEPOSIT_KEY))
                fragment = TransactionRequestDetailFragment.newInstance(TransactionType.DEPOSIT.spinnerValue, deposit, null)
            } else {
                withdraw = Parcels.unwrap(it.getParcelable(WITHDRAW_KEY))
                fragment = TransactionRequestDetailFragment.newInstance(TransactionType.WITHDRAW.spinnerValue, null, withdraw)
            }
        }
    }
}