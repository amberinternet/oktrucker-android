package th.co.growthd.oktrucker.ui.activity

import android.databinding.DataBindingUtil
import android.os.Bundle
import th.co.growthd.oktrucker.R
import th.co.growthd.oktrucker.databinding.TransactionRequestHistoryActivityBinding
import th.co.growthd.oktrucker.ui.base.BaseActivity
import th.co.growthd.oktrucker.ui.fragment.TransactionRequestHistoryFragment

class TransactionRequestHistoryActivity: BaseActivity() {

    lateinit var binding: TransactionRequestHistoryActivityBinding

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = DataBindingUtil.setContentView(this, R.layout.transaction_request_history_activity)
        supportFragmentManager
                .beginTransaction()
                .replace(R.id.transaction_request_history_container, TransactionRequestHistoryFragment.newInstance())
                .commitNowAllowingStateLoss()
    }
}