package th.co.growthd.oktrucker.ui.activity

import android.databinding.DataBindingUtil
import android.os.Bundle
import th.co.growthd.deerandbook.util.PRICE_KEY
import th.co.growthd.oktrucker.R
import th.co.growthd.oktrucker.databinding.TransferMethodActivityBinding
import th.co.growthd.oktrucker.ui.base.BaseActivity
import th.co.growthd.oktrucker.ui.fragment.TransferMethodFragment

class TransferMethodActivity: BaseActivity() {

    lateinit var binding: TransferMethodActivityBinding
    lateinit var price: String

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        getVariable()
        binding = DataBindingUtil.setContentView(this, R.layout.transfer_method_activity)
        supportFragmentManager
                .beginTransaction()
                .replace(R.id.transfer_method_container, TransferMethodFragment.newInstance(price))
                .commitNowAllowingStateLoss()
    }

    private fun getVariable() {
        intent.extras?.let {
            price = it.getString(PRICE_KEY)
        }
    }
}