package th.co.growthd.oktrucker.ui.activity

import android.databinding.DataBindingUtil
import android.os.Bundle
import th.co.growthd.oktrucker.R
import th.co.growthd.oktrucker.databinding.WithdrawActivityBinding
import th.co.growthd.oktrucker.ui.base.BaseActivity
import th.co.growthd.oktrucker.ui.fragment.WithdrawFragment

class WithdrawActivity: BaseActivity() {

    lateinit var binding: WithdrawActivityBinding

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = DataBindingUtil.setContentView(this, R.layout.withdraw_activity)
        supportFragmentManager
                .beginTransaction()
                .replace(R.id.withdraw_container, WithdrawFragment.newInstance())
                .commitNowAllowingStateLoss()
    }
}