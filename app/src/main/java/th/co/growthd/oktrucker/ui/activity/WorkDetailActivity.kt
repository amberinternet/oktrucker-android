package th.co.growthd.oktrucker.ui.activity

import android.databinding.DataBindingUtil
import android.os.Bundle
import android.view.WindowManager
import org.parceler.Parcels
import th.co.growthd.deerandbook.util.WORK_KEY
import th.co.growthd.oktrucker.R
import th.co.growthd.oktrucker.databinding.WorkDetailActivityBinding
import th.co.growthd.oktrucker.model.Work
import th.co.growthd.oktrucker.ui.base.BaseActivity
import th.co.growthd.oktrucker.ui.fragment.WorkDetailFragment

class WorkDetailActivity: BaseActivity() {

    lateinit var binding: WorkDetailActivityBinding
    lateinit var work: Work

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        // prevent Screen Capture
        window.setFlags(WindowManager.LayoutParams.FLAG_SECURE, WindowManager.LayoutParams.FLAG_SECURE)

        getVariable()
        binding = DataBindingUtil.setContentView(this, R.layout.work_detail_activity)
        supportFragmentManager.beginTransaction()
                .replace(R.id.work_detail_container, WorkDetailFragment.newInstance(work))
                .commitNowAllowingStateLoss()
    }

    private fun getVariable() {
        intent.extras?.let {
            work = Parcels.unwrap(it.getParcelable(WORK_KEY))
        }
    }
}