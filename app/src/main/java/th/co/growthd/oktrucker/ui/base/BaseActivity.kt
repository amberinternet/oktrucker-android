package th.co.growthd.oktrucker.ui.base

import android.content.Context
import android.support.v7.app.AppCompatActivity
import rx.Subscription
import uk.co.chrisjenx.calligraphy.CalligraphyContextWrapper


abstract class BaseActivity : AppCompatActivity() {

    var subscription: Subscription? = null

    override fun attachBaseContext(newBase: Context?) {
        super.attachBaseContext(CalligraphyContextWrapper.wrap(newBase))
    }

    override fun onDestroy() {
        super.onDestroy()
        subscription?.unsubscribe()
    }
}