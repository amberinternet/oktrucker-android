package th.co.growthd.oktrucker.ui.base

import android.support.v4.app.Fragment
import rx.Subscription

abstract class BaseFragment : Fragment() {

    var subscription: Subscription? = null

    override fun onPause() {
        super.onPause()
        subscription?.unsubscribe()
    }
}