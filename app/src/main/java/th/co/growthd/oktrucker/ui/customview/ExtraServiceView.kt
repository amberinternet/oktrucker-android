package th.co.growthd.oktrucker.ui.customview

import android.annotation.TargetApi
import android.content.Context
import android.databinding.DataBindingUtil
import android.os.Build
import android.util.AttributeSet
import android.view.LayoutInflater
import android.widget.FrameLayout
import th.co.growthd.oktrucker.R
import th.co.growthd.oktrucker.databinding.ItemExtraServiceBinding
import th.co.growthd.oktrucker.model.AdditionalService

class ExtraServiceView : FrameLayout {

    private var binding: ItemExtraServiceBinding
    lateinit var additionalService: AdditionalService

    @JvmOverloads
    constructor(context: Context, attrs: AttributeSet? = null, defStyleAttr: Int = 0) : super(context, attrs, defStyleAttr)

    @TargetApi(Build.VERSION_CODES.LOLLIPOP)
    constructor(context: Context, attrs: AttributeSet?, defStyleAttr: Int, defStyleRes: Int) : super(context, attrs, defStyleAttr, defStyleRes)

    init {
        binding = DataBindingUtil.inflate(LayoutInflater.from(context), R.layout.item_extra_service, this, true)
    }

    fun initView() {
        binding.nameTextView.text = additionalService.name
    }
}