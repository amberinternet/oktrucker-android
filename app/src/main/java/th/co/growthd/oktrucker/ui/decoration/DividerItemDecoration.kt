package th.co.growthd.oktrucker.ui.decoration

import android.support.v7.widget.RecyclerView
import android.graphics.Canvas
import android.graphics.drawable.Drawable

class DividerItemDecoration : RecyclerView.ItemDecoration {

    private var divider: Drawable? = null

    /**
     * Custom divider will be used
     */
    constructor(drawable: Drawable) {
        divider = drawable
    }

    override fun onDraw(c: Canvas, parent: RecyclerView, state: RecyclerView.State) {
        val left = parent.paddingLeft
        val right = parent.width - parent.paddingRight

        val childCount = parent.childCount
        for (i in 0 until childCount - 2) {
            val child = parent.getChildAt(i)

            val params = child.layoutParams as RecyclerView.LayoutParams

            val top = child.bottom + params.bottomMargin
            val bottom = top + divider!!.intrinsicHeight

            divider!!.setBounds(left, top, right, bottom)
            divider!!.draw(c)
        }
    }
}