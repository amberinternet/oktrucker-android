package th.co.growthd.deerandbook.adapter

import android.graphics.Rect
import android.support.v7.widget.RecyclerView
import android.view.View

class LineDecoration(private val lineHeight: Int) : RecyclerView.ItemDecoration() {

    override fun getItemOffsets(outRect: Rect, view: View, parent: RecyclerView, state: RecyclerView.State) {

        if (parent.getChildLayoutPosition(view) != 0) {
            outRect.top = lineHeight
        }
    }
}
