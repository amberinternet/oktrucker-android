package th.co.growthd.deerandbook.adapter

import android.graphics.Rect
import android.support.v7.widget.RecyclerView
import android.view.View

class LinearDecoration(private val verticalSpace: Int, private val leftRightSpace: Int, private val isHalfMarginTop: Boolean) : RecyclerView.ItemDecoration() {

    override fun getItemOffsets(outRect: Rect, view: View, parent: RecyclerView, state: RecyclerView.State) {

        if (parent.getChildLayoutPosition(view) == 0) {
            outRect.top = if (isHalfMarginTop) verticalSpace / 2 else verticalSpace
        }

        outRect.left = leftRightSpace
        outRect.right = leftRightSpace
        outRect.bottom = verticalSpace
    }
}
