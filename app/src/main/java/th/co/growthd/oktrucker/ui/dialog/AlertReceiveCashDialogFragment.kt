package th.co.growthd.oktrucker.ui.dialog

import android.databinding.DataBindingUtil
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import th.co.growthd.deerandbook.util.*
import th.co.growthd.oktrucker.R
import th.co.growthd.oktrucker.databinding.AlertReceiveCashDialogFragmentBinding
import th.co.growthd.oktrucker.ui.base.BaseDialogFragment
import kotlin.properties.Delegates

class AlertReceiveCashDialogFragment : BaseDialogFragment() {

    lateinit var binding: AlertReceiveCashDialogFragmentBinding
    private var messageRes: Int by Delegates.notNull()
    var onDismiss: (() -> Unit)? = null
    var message: String? = null
    var price: String? = null

    companion object {
        fun newInstance(messageRes: Int, price: String): AlertReceiveCashDialogFragment {
            val fragment = AlertReceiveCashDialogFragment()
            val bundle = Bundle()
            bundle.putInt(MESSAGE_RES_KEY, messageRes)
            bundle.putString(PRICE_KEY, price)
            fragment.arguments = bundle
            return fragment
        }
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        arguments?.let {
            if (it.containsKey(MESSAGE_RES_KEY)) {
                messageRes = it.getInt(MESSAGE_RES_KEY)
                message = context!!.getString(messageRes)
            }

            if (it.containsKey(PRICE_KEY)) {
                price = it.getString(PRICE_KEY)
            }
        }
    }

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        binding = DataBindingUtil.inflate(inflater, R.layout.alert_receive_cash_dialog_fragment, container, false)
        initView()
        return binding.root
    }

    private fun initView() {
        message?.let {
            binding.messageTextView.text = it
        }
        price?.let {
            binding.priceTextView.text = it
        }
        binding.agreeTextView.setOnClickListener {
            onDismiss?.invoke()
            dismiss()
        }

        binding.closeImageView.setOnClickListener {
            dismiss()
        }
    }
}