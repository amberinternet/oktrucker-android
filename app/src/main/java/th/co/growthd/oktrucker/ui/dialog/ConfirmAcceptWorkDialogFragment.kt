package th.co.growthd.oktrucker.ui.dialog

import android.databinding.DataBindingUtil
import android.os.Bundle
import android.support.v4.content.ContextCompat
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import org.parceler.Parcels
import th.co.growthd.deerandbook.util.WORK_KEY
import th.co.growthd.oktrucker.R
import th.co.growthd.oktrucker.databinding.ConfirmAcceptWorkDialogFragmentBinding
import th.co.growthd.oktrucker.model.Work
import th.co.growthd.oktrucker.ui.base.BaseDialogFragment
import th.co.growthd.oktrucker.util.enum.PaymentMethod

class ConfirmAcceptWorkDialogFragment : BaseDialogFragment() {

    lateinit var binding: ConfirmAcceptWorkDialogFragmentBinding
    lateinit var work: Work
    lateinit var onAccept: () -> Unit?

    companion object {
        fun newInstance(work: Work): ConfirmAcceptWorkDialogFragment {
            val fragment = ConfirmAcceptWorkDialogFragment()
            val bundle = Bundle()
            bundle.putParcelable(WORK_KEY, Parcels.wrap(work))
            fragment.arguments = bundle
            return fragment
        }
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        arguments?.let {
            work = Parcels.unwrap(it.getParcelable(WORK_KEY))
        }
    }

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        binding = DataBindingUtil.inflate(inflater, R.layout.confirm_accept_work_dialog_fragment, container, false)
        initView()
        initOnClick()
        return binding.root
    }

    private fun initView() {
        binding.paymentMethodTextView.text = work.getReadableReceiveMethod()
        binding.paymentMethodIcon.setImageResource(
                when (work.paymentMethod) {
                    PaymentMethod.CASH_DESTINATION.status, PaymentMethod.CASH_SOURCE.status -> {
                        R.drawable.ic_dollar_note
                    }
                    else -> R.drawable.ic_wallet
                }
        )
        binding.priceTextView.text = work.getPrice()
        binding.feeTextView.text = work.getFee()
        binding.finalWageTextView.text = work.getFinalWage()
    }

    private fun initOnClick() {
        binding.confirmAcceptWorkTextView.setOnClickListener {
            onAccept.invoke()
            dismiss()
        }
        binding.backTextView.setOnClickListener {
            dismiss()
        }
        binding.closeImageView.setOnClickListener {
            dismiss()
        }
    }
}