package th.co.growthd.oktrucker.ui.dialog

import android.databinding.DataBindingUtil
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import org.parceler.Parcels
import th.co.growthd.deerandbook.util.WORK_KEY
import th.co.growthd.oktrucker.R
import th.co.growthd.oktrucker.databinding.ConfirmAcceptWorkDialogFragment2Binding
import th.co.growthd.oktrucker.model.Work
import th.co.growthd.oktrucker.ui.base.BaseDialogFragment

class ConfirmAcceptWorkDialogFragment2 : BaseDialogFragment() {

    lateinit var binding: ConfirmAcceptWorkDialogFragment2Binding
    lateinit var work: Work
    var onAccept: (() -> Unit)? = null

    companion object {
        fun newInstance(work: Work): ConfirmAcceptWorkDialogFragment2 {
            val fragment = ConfirmAcceptWorkDialogFragment2()
            val bundle = Bundle()
            bundle.putParcelable(WORK_KEY, Parcels.wrap(work))
            fragment.arguments = bundle
            return fragment
        }
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        arguments?.let {
            work = Parcels.unwrap(it.getParcelable(WORK_KEY))
        }
    }

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        binding = DataBindingUtil.inflate(inflater, R.layout.confirm_accept_work_dialog_fragment2, container, false)
        initView()
        initOnClick()
        return binding.root
    }

    private fun initView() {
        binding.sourceAddressTextView.text = work.source.address
        binding.destinationAddressTextView.text = work.destination.address
        binding.sourceDateTextView.text = work.source.getReceiveDateTime()
        binding.destinationDateTextView.text = work.destination.getDeliveryDateTime()
    }

    private fun initOnClick() {
        binding.confirmAcceptWorkTextView.setOnClickListener {
            onAccept?.invoke()
            dismiss()
        }
        binding.closeImageView.setOnClickListener {
            dismiss()
        }
    }
}