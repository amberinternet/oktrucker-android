package th.co.growthd.oktrucker.ui.dialog

import android.databinding.DataBindingUtil
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import th.co.growthd.oktrucker.R
import th.co.growthd.deerandbook.util.MESSAGE_RES_KEY
import th.co.growthd.deerandbook.util.TITLE_RES_KEY
import th.co.growthd.oktrucker.databinding.ConfirmDialogFragmentBinding
import th.co.growthd.oktrucker.ui.base.BaseDialogFragment
import kotlin.properties.Delegates

class ConfirmDialogFragment : BaseDialogFragment() {

    lateinit var binding: ConfirmDialogFragmentBinding
    lateinit var title: String
    lateinit var message: String
    private var titleRes: Int by Delegates.notNull()
    private var messageRes: Int by Delegates.notNull()
    var onConfirm: (() -> Unit)? = null
    var onCancel: (() -> Unit)? = null

    companion object {
        fun newInstance(titleRes: Int, messageRes: Int): ConfirmDialogFragment {
            val fragment = ConfirmDialogFragment()
            val bundle = Bundle()
            bundle.putInt(TITLE_RES_KEY, titleRes)
            bundle.putInt(MESSAGE_RES_KEY, messageRes)
            fragment.arguments = bundle
            return fragment
        }
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        arguments?.let {
            titleRes = it.getInt(TITLE_RES_KEY)
            title = context!!.getString(titleRes)
            messageRes = it.getInt(MESSAGE_RES_KEY)
            message = context!!.getString(messageRes)
        }
    }

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        binding = DataBindingUtil.inflate(inflater, R.layout.confirm_dialog_fragment, container, false)
        initView()
        initOnClick()
        return binding.root
    }

    private fun initView() {
        binding.titleTextView.text = title
        binding.messageTextView.text = message
        binding.backTextView.setOnClickListener {
            onCancel?.invoke()
            dismiss()
        }
        binding.confirmTextView.setOnClickListener {
            onConfirm?.invoke()
            dismiss()
        }
    }

    private fun initOnClick() {
        binding.closeImageView.setOnClickListener { dismiss() }
    }
}