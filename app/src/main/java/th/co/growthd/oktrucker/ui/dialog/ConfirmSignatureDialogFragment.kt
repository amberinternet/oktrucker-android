package th.co.growthd.oktrucker.ui.dialog

import android.databinding.DataBindingUtil
import android.graphics.Bitmap
import android.os.Bundle
import android.support.v4.content.ContextCompat
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Toast
import th.co.growthd.oktrucker.R
import th.co.growthd.deerandbook.util.TITLE_RES_KEY
import th.co.growthd.oktrucker.databinding.ConfirmSignatureDialogFragmentBinding
import th.co.growthd.oktrucker.ui.base.BaseDialogFragment
import kotlin.properties.Delegates

class ConfirmSignatureDialogFragment : BaseDialogFragment() {

    lateinit var binding: ConfirmSignatureDialogFragmentBinding
    lateinit var title: String
    lateinit var onConfirm: (bitmap: Bitmap) -> Unit?
    private var titleRes: Int by Delegates.notNull()

    companion object {
        fun newInstance(titleRes: Int): ConfirmSignatureDialogFragment {
            val fragment = ConfirmSignatureDialogFragment()
            val bundle = Bundle()
            bundle.putInt(TITLE_RES_KEY, titleRes)
            fragment.arguments = bundle
            return fragment
        }
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        arguments?.let {
            titleRes = it.getInt(TITLE_RES_KEY)
            title = context!!.getString(titleRes)
        }
    }

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        binding = DataBindingUtil.inflate(inflater, R.layout.confirm_signature_dialog_fragment, container, false)
        binding.titleTextView.text = title
        initOnClick()
        return binding.root
    }

    private fun initOnClick() {
        binding.closeImageView.setOnClickListener { dismiss() }

        binding.resetTextView.setOnClickListener {
            binding.signatureView.clear()
        }

        binding.confirmTextView.setOnClickListener {
            if (!binding.signatureView.isEmpty) {
                onConfirm.invoke(binding.signatureView.signatureBitmap)
                dismiss()
            } else {
                Toast.makeText(context, R.string.please_sign, Toast.LENGTH_SHORT).show()
            }
        }
    }
}