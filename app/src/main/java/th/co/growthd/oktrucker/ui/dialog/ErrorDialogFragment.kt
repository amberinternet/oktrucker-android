package th.co.growthd.oktrucker.ui.dialog

import android.databinding.DataBindingUtil
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import th.co.growthd.oktrucker.R
import th.co.growthd.deerandbook.util.MESSAGE_KEY
import th.co.growthd.oktrucker.databinding.ErrorDialogFragmentBinding
import th.co.growthd.oktrucker.ui.base.BaseDialogFragment
import th.co.growthd.oktrucker.ui.dialog.listener.ErrorDialogListener

class ErrorDialogFragment : BaseDialogFragment() {

    lateinit var binding: ErrorDialogFragmentBinding
    lateinit var message: String
    var errorDialogListener: ErrorDialogListener? = null

    companion object {
        fun newInstance(error: String): ErrorDialogFragment {
            val fragment = ErrorDialogFragment()
            val bundle = Bundle()
            bundle.putString(MESSAGE_KEY, error)
            fragment.arguments = bundle
            return fragment
        }
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        arguments?.let {
            message = it.getString(MESSAGE_KEY)
        }
    }

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        binding = DataBindingUtil.inflate(inflater, R.layout.error_dialog_fragment, container, false)
        initView()
        return binding.root
    }

    private fun initView() {
        binding.messageTextView.text = message
        binding.closeImageView.setOnClickListener {
            dismiss()
            errorDialogListener?.onDismiss()
        }
    }
}