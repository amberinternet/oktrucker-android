package th.co.growthd.oktrucker.ui.dialog

import android.databinding.DataBindingUtil
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import th.co.growthd.oktrucker.R
import th.co.growthd.oktrucker.databinding.PrivacyPolicyDialogFragmentBinding
import th.co.growthd.oktrucker.ui.base.BaseDialogFragment

class PrivacyPolicyDialogFragment : BaseDialogFragment() {

    lateinit var binding: PrivacyPolicyDialogFragmentBinding

    companion object {
        fun newInstance() = PrivacyPolicyDialogFragment()
    }

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        binding = DataBindingUtil.inflate(inflater, R.layout.privacy_policy_dialog_fragment, container, false)
        return binding.root
    }

    override fun onActivityCreated(savedInstanceState: Bundle?) {
        super.onActivityCreated(savedInstanceState)
        initView()
    }

    private fun initView() {
        binding.closeImageView.setOnClickListener { dismiss() }
    }
}