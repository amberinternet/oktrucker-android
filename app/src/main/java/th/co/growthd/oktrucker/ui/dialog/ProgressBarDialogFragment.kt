package th.co.growthd.oktrucker.ui.dialog

import android.databinding.DataBindingUtil
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import th.co.growthd.oktrucker.R
import th.co.growthd.deerandbook.util.MESSAGE_KEY
import th.co.growthd.deerandbook.util.MESSAGE_RES_KEY
import th.co.growthd.oktrucker.databinding.ProgressBarDialogFragmentBinding
import th.co.growthd.oktrucker.ui.base.BaseDialogFragment
import kotlin.properties.Delegates

class ProgressBarDialogFragment : BaseDialogFragment() {

    lateinit var binding: ProgressBarDialogFragmentBinding
    lateinit var message: String
    private var messageRes: Int by Delegates.notNull()

    companion object {
        fun newInstance(message: String): ProgressBarDialogFragment {
            val fragment = ProgressBarDialogFragment()
            val bundle = Bundle()
            bundle.putString(MESSAGE_KEY, message)
            fragment.arguments = bundle
            return fragment
        }

        fun newInstance(messageRes: Int): ProgressBarDialogFragment {
            val fragment = ProgressBarDialogFragment()
            val bundle = Bundle()
            bundle.putInt(MESSAGE_RES_KEY, messageRes)
            fragment.arguments = bundle
            return fragment
        }
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        arguments?.let {
            when {
                it.containsKey(MESSAGE_KEY) -> message = it.getString(MESSAGE_KEY)
                it.containsKey(MESSAGE_RES_KEY) -> {
                    messageRes = it.getInt(MESSAGE_RES_KEY)
                    message = context!!.getString(messageRes)
                }
            }
        }
    }

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        binding = DataBindingUtil.inflate(inflater, R.layout.progress_bar_dialog_fragment, container, false)
        isCancelable = false
        initView()
        return binding.root
    }

    private fun initView() {
        binding.messageTextView.text = message
    }

    override fun onStart() {
        super.onStart()
        dialog.window.setLayout(ViewGroup.LayoutParams.WRAP_CONTENT, ViewGroup.LayoutParams.WRAP_CONTENT)
    }

    override fun onPause() {
        super.onPause()
        dismiss()
    }
}