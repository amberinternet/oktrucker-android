package th.co.growthd.oktrucker.ui.dialog

import android.databinding.DataBindingUtil
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import th.co.growthd.oktrucker.R
import th.co.growthd.oktrucker.databinding.TermAndConditionDialogFragmentBinding
import th.co.growthd.oktrucker.ui.base.BaseDialogFragment

class TermAndConditionDialogFragment : BaseDialogFragment() {

    lateinit var binding: TermAndConditionDialogFragmentBinding

    companion object {
        fun newInstance() = TermAndConditionDialogFragment()
    }

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        binding = DataBindingUtil.inflate(inflater, R.layout.term_and_condition_dialog_fragment, container, false)
        return binding.root
    }

    override fun onActivityCreated(savedInstanceState: Bundle?) {
        super.onActivityCreated(savedInstanceState)
        initView()
        initOnClick()
    }

    private fun initOnClick() {
        binding.closeImageView.setOnClickListener { dismiss() }
    }

    private fun initView() {

    }
}