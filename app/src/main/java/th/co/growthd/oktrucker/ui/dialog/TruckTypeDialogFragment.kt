package th.co.growthd.oktrucker.ui.dialog

import android.databinding.DataBindingUtil
import android.os.Bundle
import android.support.v7.widget.LinearLayoutManager
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import org.greenrobot.eventbus.EventBus
import org.parceler.Parcels
import th.co.growthd.deerandbook.adapter.LinearDecoration
import th.co.growthd.deerandbook.util.TRUCK_TYPE_KEY
import th.co.growthd.oktrucker.R
import th.co.growthd.oktrucker.adapter.TruckTypeAdapter
import th.co.growthd.oktrucker.adapter.callback.OnRecyclerTouchListener
import th.co.growthd.oktrucker.databinding.TruckTypeDialogFragmentBinding
import th.co.growthd.oktrucker.model.TruckType
import th.co.growthd.oktrucker.model.subscriber.TruckTypeSubscriber
import th.co.growthd.oktrucker.ui.base.BaseDialogFragment
import th.co.growthd.oktrucker.util.enum.TruckTypeEnum
import th.co.growthd.oktrucker.util.manager.NetworkManager
import th.co.growthd.oktrucker.util.network.RxNetwork
import th.co.growthd.oktrucker.util.network.response.TruckTypeResponse
import th.co.growthd.oktrucker.util.network.service.AppService

class TruckTypeDialogFragment : BaseDialogFragment() {

    private lateinit var binding: TruckTypeDialogFragmentBinding
    private lateinit var truckType: TruckType
    private lateinit var truckTypeAdapter: TruckTypeAdapter
//    private lateinit var networkManager: NetworkManager
//    private lateinit var appService: AppService
    private var truckTypeList = mutableListOf<TruckType>()

    companion object {
        fun newInstance(truckType: TruckType? = null): TruckTypeDialogFragment {
            val fragment = TruckTypeDialogFragment()
            truckType?.let {
                val bundle = Bundle()
                bundle.putParcelable(TRUCK_TYPE_KEY, Parcels.wrap(it))
                fragment.arguments = bundle
            }
            return fragment
        }
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        arguments?.let {
            if (it.containsKey(TRUCK_TYPE_KEY)) {
                truckType = Parcels.unwrap(it.getParcelable(TRUCK_TYPE_KEY))
            }
        }
        initVariable()
        getTruckType()
    }

    private fun initVariable() {
//        networkManager = NetworkManager.getInstance(context!!)
//        appService = AppService.getInstance(context!!)
    }

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        binding = DataBindingUtil.inflate(inflater, R.layout.truck_type_dialog_fragment, container, false)
        initRecyclerView()
        initOnClick()
        return binding.root
    }

    private fun getTruckType() {
//        subscription = RxNetwork<TruckTypeResponse>(context!!).request(appService.getTruckType(), onSuccess = { response ->
//            binding.progressBar.visibility = View.GONE
//            truckTypeList.addAll(response.truckTypeList)
//            initRecyclerView()
//        }, onFailure = {
//            binding.progressBar.visibility = View.GONE
//            binding.errorTextView.setText(R.string.status_server_internal_error)
//        })

        for (i in 0 until TruckTypeEnum.values().size) {
            truckTypeList.add(TruckType(id = TruckTypeEnum.values()[i].id,
                    name = TruckTypeEnum.values()[i].typeName,
                    description = TruckTypeEnum.values()[i].description,
                    truckImageResId = TruckTypeEnum.values()[i].imageResId))
        }
    }

    private fun initRecyclerView() {
        truckTypeAdapter = TruckTypeAdapter(truckTypeList, truckType)
        binding.recyclerView.apply {
            layoutManager = LinearLayoutManager(context)
            adapter = truckTypeAdapter
            addItemDecoration(LinearDecoration(
                    resources.getDimension(R.dimen.margin_form).toInt(),
                    resources.getDimension(R.dimen.margin_form).toInt(),
                    false
            ))
            addOnItemTouchListener(OnRecyclerTouchListener(context!!) { position ->
                selectTruckType(position)
            })
        }

    }

    private fun initOnClick() {
        binding.closeImageView.setOnClickListener {
            dismiss()
        }
    }

    private fun selectTruckType(position: Int) {
        EventBus.getDefault().post(TruckTypeSubscriber(truckTypeList[position]))
        dismiss()
    }
}