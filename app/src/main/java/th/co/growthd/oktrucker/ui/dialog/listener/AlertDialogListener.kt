package th.co.growthd.oktrucker.ui.dialog.listener

interface AlertDialogListener {
    fun onDismiss()
}