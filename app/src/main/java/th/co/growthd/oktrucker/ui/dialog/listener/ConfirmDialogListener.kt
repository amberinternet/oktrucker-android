package th.co.growthd.oktrucker.ui.dialog.listener

interface ConfirmDialogListener {

    fun onConfirm()
    fun onCancel()
}