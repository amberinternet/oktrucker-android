package th.co.growthd.oktrucker.ui.dialog.listener

interface ErrorDialogListener {

    fun onDismiss()
}