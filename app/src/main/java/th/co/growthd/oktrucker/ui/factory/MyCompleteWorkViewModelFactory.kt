package th.co.growthd.deerandbook.ui.factory

import android.arch.lifecycle.ViewModel
import android.arch.lifecycle.ViewModelProvider
import android.support.v4.app.FragmentActivity
import th.co.growthd.oktrucker.ui.viewmodel.MyCompleteWorkViewModel

class MyCompleteWorkViewModelFactory (private val fragmentActivity: FragmentActivity) : ViewModelProvider.NewInstanceFactory() {

    override fun <T : ViewModel?> create(modelClass: Class<T>): T {
        return MyCompleteWorkViewModel(fragmentActivity) as T
    }
}