package th.co.growthd.deerandbook.ui.factory

import android.arch.lifecycle.ViewModel
import android.arch.lifecycle.ViewModelProvider
import android.support.v4.app.FragmentActivity
import th.co.growthd.oktrucker.ui.viewmodel.WorkDetailViewModel

class WorkDetailViewModelFactory (private val fragmentActivity: FragmentActivity) : ViewModelProvider.NewInstanceFactory() {

    override fun <T : ViewModel?> create(modelClass: Class<T>): T {
        return WorkDetailViewModel(fragmentActivity) as T
    }
}