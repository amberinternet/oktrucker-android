package th.co.growthd.oktrucker.ui.fragment

import android.databinding.DataBindingUtil
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import th.co.growthd.oktrucker.BuildConfig
import th.co.growthd.oktrucker.R
import th.co.growthd.oktrucker.databinding.AboutApplicationFragmentBinding
import th.co.growthd.oktrucker.ui.base.BaseFragment

class AboutApplicationFragment: BaseFragment() {

    private lateinit var binding: AboutApplicationFragmentBinding

    companion object {
        fun newInstance() = AboutApplicationFragment()
    }

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View {
        binding = DataBindingUtil.inflate(inflater, R.layout.about_application_fragment, container, false)
        return binding.root
    }

    override fun onActivityCreated(savedInstanceState: Bundle?) {
        super.onActivityCreated(savedInstanceState)
        initView()
        initOnClick()
    }

    private fun initView() {
        binding.toolBar!!.titleTextView.setText(R.string.title_about_app)
        binding.buildVersionTextView.text = BuildConfig.VERSION_NAME
        binding.buildNumberTextView.text = BuildConfig.VERSION_CODE.toString()
    }

    private fun initOnClick() {
        binding.toolBar!!.backImageView.setOnClickListener {
            activity?.onBackPressed()
        }
    }
}