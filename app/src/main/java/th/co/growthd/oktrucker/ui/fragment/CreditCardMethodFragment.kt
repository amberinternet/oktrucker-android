package th.co.growthd.oktrucker.ui.fragment

import android.app.Activity
import android.arch.lifecycle.ViewModelProviders
import android.content.Intent
import android.databinding.DataBindingUtil
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import th.co.growthd.oktrucker.databinding.CreditCardMethodFragmentBinding
import th.co.growthd.oktrucker.ui.base.BaseFragment
import co.omise.android.Client
import th.co.growthd.oktrucker.*
import th.co.growthd.oktrucker.util.manager.DialogManager
import co.omise.android.TokenRequest
import co.omise.android.TokenRequestListener
import co.omise.android.models.Token
import co.omise.android.ui.AuthorizingPaymentActivity
import th.co.growthd.deerandbook.ui.factory.CreditCardMethodViewModelFactory
import th.co.growthd.deerandbook.util.BASE_API_URL
import th.co.growthd.deerandbook.util.PRICE_KEY
import th.co.growthd.oktrucker.ui.viewmodel.CreditCardMethodViewModel
import java.util.*

class CreditCardMethodFragment : BaseFragment() {

    private lateinit var binding: CreditCardMethodFragmentBinding
    private lateinit var dialogManager: DialogManager
    private lateinit var viewModel: CreditCardMethodViewModel
    private lateinit var price: String

    companion object {
        private val AUTHORIZING_PAYMENT_REQUEST_CODE = 1
        fun newInstance(price: String): CreditCardMethodFragment {
            val fragment = CreditCardMethodFragment()
            val bundle = Bundle().apply { putString(PRICE_KEY, price) }
            fragment.arguments = bundle
            return fragment
        }
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        initVariable()
    }

    private fun initVariable() {
        dialogManager = DialogManager.getInstance(childFragmentManager)
        arguments?.let {
            price = it.getString(PRICE_KEY)
        }
    }

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View {
        binding = DataBindingUtil.inflate(inflater, R.layout.credit_card_method_fragment, container, false)
        return binding.root
    }

    override fun onActivityCreated(savedInstanceState: Bundle?) {
        super.onActivityCreated(savedInstanceState)
        viewModel = ViewModelProviders.of(this, CreditCardMethodViewModelFactory(activity!!)).get(CreditCardMethodViewModel::class.java)
        viewModel.depositOmise.addOnPropertyChanged {
            showAuthorizingPaymentForm(it.get()!!.authorizeUri)
        }
        initView()
        initOnClick()
    }

    private fun initView() {
        binding.toolBar!!.titleTextView.setText(R.string.title_payment)

        binding.monthEditText.afterTextChanged { string ->
            when (string.length) {
                2 -> binding.yearEditText.requestFocus()
            }
        }

        binding.yearEditText.afterTextChanged { string ->
            when (string.length) {
                0 -> binding.monthEditText.requestFocus()
                4 -> activity?.hideKeyboard()
            }
        }
    }

    private fun initOnClick() {
        binding.toolBar!!.backImageView.setOnClickListener {
            activity?.finish()
        }

        binding.expirationDateLayout.setOnClickListener {
            binding.monthEditText.requestFocus()
        }

        binding.confirmButton.setOnClickListener {
            if (validate()) {
                dialogManager.showLoading(R.string.checking_data)
                val client = Client("pkey_test_5ezt65lmm4vt9n9q1dy")
                val request = TokenRequest()
                request.name = binding.creditCardNameEditText.string()
                request.number = binding.creditCardNumberEditText.string().replace(" ", "")
                request.expirationMonth = binding.monthEditText.string().toInt()
                request.expirationYear = binding.yearEditText.string().toInt()
                request.securityCode = binding.cvvEditText.string()
                client.send(request, object : TokenRequestListener {
                    override fun onTokenRequestSucceed(p0: TokenRequest?, p1: Token?) {
                        dialogManager.hideLoading()
                        subscription = viewModel.requestDepositWithOmise(price.toInt(), p1?.id!!)
                    }

                    override fun onTokenRequestFailed(p0: TokenRequest?, p1: Throwable?) {
                        dialogManager.hideLoading()
                        dialogManager.showError(p1?.localizedMessage!!)
                        when (p1.localizedMessage) {
                            "expiration date cannot be in the past" -> dialogManager.showAlert(R.string.credit_card_date_expire)
                            "number is invalid", "number is invalid and brand not supported (unknown)" -> dialogManager.showAlert(R.string.invalid_credit_card_number)
                        }
                    }
                })
            }
        }
    }

    private fun showAuthorizingPaymentForm(url: String) {
        var array = arrayOf("${BASE_API_URL}redirector")
        val intent = Intent(activity, AuthorizingPaymentActivity::class.java)
        intent.putExtra(AuthorizingPaymentActivity.EXTRA_AUTHORIZED_URLSTRING, url)
        intent.putExtra(AuthorizingPaymentActivity.EXTRA_EXPECTED_RETURN_URLSTRING_PATTERNS, array)
        startActivityForResult(intent, AUTHORIZING_PAYMENT_REQUEST_CODE)
    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        if (requestCode == AUTHORIZING_PAYMENT_REQUEST_CODE && resultCode == Activity.RESULT_OK) {
            val url = data!!.getStringExtra(AuthorizingPaymentActivity.EXTRA_RETURNED_URLSTRING)
            subscription = viewModel.completePaymentOmise()
        }
    }

    private fun validate(): Boolean {
        return when {
            binding.creditCardNumberEditText.isEmpty() -> {
                dialogManager.showAlert(R.string.please_fill_out_credit_card_number)
                return false
            }
            binding.monthEditText.isEmpty() -> {
                dialogManager.showAlert(R.string.please_fill_out_expired_date)
                return false
            }
            binding.monthEditText.string().toInt() !in 1..12 -> {
                dialogManager.showAlert(R.string.invalid_date)
                return false
            }
            binding.yearEditText.isEmpty() -> {
                dialogManager.showAlert(R.string.please_fill_out_expired_date)
                return false
            }
            binding.yearEditText.string().toInt() < Calendar.getInstance().getYear() -> {
                dialogManager.showAlert(R.string.invalid_date)
                return false
            }
            binding.cvvEditText.isEmpty() -> {
                dialogManager.showAlert(R.string.please_fill_out_cvv)
                return false
            }
            binding.cvvEditText.length() < 3 || binding.cvvEditText.length() > 4 -> {
                dialogManager.showAlert(R.string.invalid_cvv)
                return false
            }
            binding.creditCardNameEditText.isEmpty() -> {
                dialogManager.showAlert(R.string.please_fill_out_credit_card_name)
                return false
            }
            else -> true
        }
    }
}