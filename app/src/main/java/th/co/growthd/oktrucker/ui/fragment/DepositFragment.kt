package th.co.growthd.oktrucker.ui.fragment

import android.content.Intent
import android.databinding.DataBindingUtil
import android.os.Bundle
import android.support.v7.widget.LinearLayoutManager
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import th.co.growthd.deerandbook.util.PRICE_KEY
import th.co.growthd.oktrucker.R
import th.co.growthd.oktrucker.adapter.CreditAdapter
import th.co.growthd.oktrucker.adapter.callback.OnRecyclerTouchListener
import th.co.growthd.oktrucker.databinding.DepositFragmentBinding
import th.co.growthd.oktrucker.model.User
import th.co.growthd.oktrucker.ui.activity.PaymentMethodActivity
import th.co.growthd.oktrucker.ui.base.BaseFragment

class DepositFragment : BaseFragment() {

    private lateinit var binding: DepositFragmentBinding
    private lateinit var user: User
    private lateinit var priceArray: Array<String>

    companion object {
        fun newInstance() = DepositFragment()
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        initVariable()
    }

    private fun initVariable() {
        user = User.instance
        priceArray = resources.getStringArray(R.array.deposit_amount_array)
    }

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View {
        binding = DataBindingUtil.inflate(inflater, R.layout.deposit_fragment, container, false)
        return binding.root
    }

    override fun onActivityCreated(savedInstanceState: Bundle?) {
        super.onActivityCreated(savedInstanceState)
        initView()
        initOnClick()
    }

    private fun initView() {
        binding.toolBar!!.titleTextView.setText(R.string.title_deposit)
        binding.creditTextView.text = user.getCredit()
        binding.recyclerView.apply {
            layoutManager = LinearLayoutManager(context, LinearLayoutManager.VERTICAL, false)
            adapter = CreditAdapter(priceArray)
            addOnItemTouchListener(OnRecyclerTouchListener(context!!) { position ->
                val intent = Intent(activity, PaymentMethodActivity::class.java)
                intent.putExtra(PRICE_KEY, priceArray[position])
                startActivity(intent)
            })
        }
    }

    private fun initOnClick() {
        binding.toolBar!!.backImageView.setOnClickListener {
            activity?.finish()
        }
    }
}