package th.co.growthd.oktrucker.ui.fragment

import android.databinding.DataBindingUtil
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.AdapterView
import org.greenrobot.eventbus.EventBus
import th.co.growthd.deerandbook.util.INITIAL_INT
import th.co.growthd.deerandbook.util.MONTH_KEY
import th.co.growthd.deerandbook.util.YEAR_KEY
import th.co.growthd.oktrucker.R
import th.co.growthd.oktrucker.adapter.TransactionRequestSpinnerAdapter
import th.co.growthd.oktrucker.databinding.FilterTransactionFragmentBinding
import th.co.growthd.oktrucker.getMonthPosition
import th.co.growthd.oktrucker.getYearPosition
import th.co.growthd.oktrucker.model.subscriber.FilterTransactionSubscriber
import th.co.growthd.oktrucker.ui.base.BaseFragment
import java.util.*

class FilterTransactionFragment : BaseFragment() {

    private lateinit var binding: FilterTransactionFragmentBinding
    private var month: Int = INITIAL_INT
    private var year: Int = INITIAL_INT

    companion object {
        fun newInstance(month: Int, year: Int): FilterTransactionFragment {
            val fragment = FilterTransactionFragment()
            val bundle = Bundle()
            bundle.putInt(MONTH_KEY, month)
            bundle.putInt(YEAR_KEY, year)
            fragment.arguments = bundle
            return fragment
        }
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        getVariable()
    }

    private fun getVariable() {
        arguments?.let {
            if (it.containsKey(MONTH_KEY)) {
                month = it.getInt(MONTH_KEY)
            }

            if (it.containsKey(YEAR_KEY)) {
                year = it.getInt(YEAR_KEY)
            }
        }
    }

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View {
        binding = DataBindingUtil.inflate(inflater, R.layout.filter_transaction_fragment, container, false)
        return binding.root
    }

    override fun onActivityCreated(savedInstanceState: Bundle?) {
        super.onActivityCreated(savedInstanceState)
        initView()
        initOnClick()
    }

    private fun initView() {
        binding.toolBar!!.titleTextView.setText(R.string.title_filter_transaction)
        binding.toolBar!!.backImageView.setOnClickListener {
            activity?.finish()
        }
        binding.monthSpinner.apply {
            adapter = TransactionRequestSpinnerAdapter().apply { stringArray = resources.getStringArray(R.array.month_array) }
            onItemSelectedListener = object : AdapterView.OnItemSelectedListener {
                override fun onNothingSelected(parent: AdapterView<*>?) {

                }

                override fun onItemSelected(parent: AdapterView<*>?, view: View?, position: Int, id: Long) {

                }
            }
            setSelection(Calendar.getInstance().getMonthPosition())
        }

        binding.yearSpinner.apply {
            adapter = TransactionRequestSpinnerAdapter().apply { stringArray = resources.getStringArray(R.array.year_array) }
            onItemSelectedListener = object : AdapterView.OnItemSelectedListener {
                override fun onNothingSelected(parent: AdapterView<*>?) {

                }

                override fun onItemSelected(parent: AdapterView<*>?, view: View?, position: Int, id: Long) {

                }
            }
            setSelection(Calendar.getInstance().getYearPosition())
        }
    }

    private fun initOnClick() {
        binding.searchTextView.setOnClickListener {
            val subscriber = FilterTransactionSubscriber().apply {
                month = binding.monthSpinner.selectedItemPosition
                year = binding.yearSpinner.selectedItemPosition
            }
            EventBus.getDefault().post(subscriber)
            activity?.finish()
        }
    }
}