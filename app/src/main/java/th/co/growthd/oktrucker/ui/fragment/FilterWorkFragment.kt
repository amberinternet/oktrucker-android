package th.co.growthd.oktrucker.ui.fragment

import android.databinding.DataBindingUtil
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.AdapterView
import org.greenrobot.eventbus.EventBus
import th.co.growthd.deerandbook.util.INITIAL_STRING
import th.co.growthd.deerandbook.util.PROVINCE_KEY
import th.co.growthd.oktrucker.R
import th.co.growthd.oktrucker.adapter.FilterWorkAutoCompleteAdapter
import th.co.growthd.oktrucker.afterTextChanged
import th.co.growthd.oktrucker.databinding.FilterWorkFragmentBinding
import th.co.growthd.oktrucker.model.subscriber.FilterWorkSubscriber
import th.co.growthd.oktrucker.string
import th.co.growthd.oktrucker.ui.base.BaseFragment

class FilterWorkFragment : BaseFragment() {

    private lateinit var binding: FilterWorkFragmentBinding
    private lateinit var provinceArray: Array<String>
    private lateinit var province: String
    private var isSelect: Boolean = false

    companion object {
        fun newInstance(province: String): FilterWorkFragment {
            val fragment = FilterWorkFragment()
            val bundle = Bundle()
            bundle.putString(PROVINCE_KEY, province)
            fragment.arguments = bundle
            return fragment
        }
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        getVariable()
        initVariable()
    }

    private fun getVariable() {
        arguments?.let {
            province = it.getString(PROVINCE_KEY, INITIAL_STRING)
        }
    }

    private fun initVariable() {
        provinceArray = resources.getStringArray(R.array.work_province_array)
    }

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View {
        binding = DataBindingUtil.inflate(inflater, R.layout.filter_work_fragment, container, false)
        return binding.root
    }

    override fun onActivityCreated(savedInstanceState: Bundle?) {
        super.onActivityCreated(savedInstanceState)
        initView()
        initOnClick()
    }

    private fun initView() {
        binding.toolBar!!.titleTextView.setText(R.string.title_filter_work)
        binding.toolBar!!.backImageView.setOnClickListener {
            activity?.finish()
        }
        binding.provinceTextView.apply {
            setAdapter(FilterWorkAutoCompleteAdapter().apply {
                provinceArray = this@FilterWorkFragment.provinceArray.toCollection(ArrayList())
                resultArray = provinceArray
            })
            onItemClickListener = AdapterView.OnItemClickListener { parent, view, position, id ->
                province = text.toString()
                setText(getReadableProvinceName(province))
                isSelect = true
            }
            setOnClickListener {
                showDropDown()
            }
            afterTextChanged { isSelect = false }

            if (province.isNotEmpty()) {
                setText(getReadableProvinceName(province))
                isSelect = true
            }
        }
    }

    private fun initOnClick() {
        binding.searchTextView.setOnClickListener {
            val subscriber = FilterWorkSubscriber().apply {
                province = if (isSelect) {
                    if (binding.provinceTextView.string() == "ทุกจังหวัด") "" else this@FilterWorkFragment.province
                } else binding.provinceTextView.string()
            }
            EventBus.getDefault().post(subscriber)
            activity?.finish()
        }
    }

    private fun getReadableProvinceName(province: String): String {
        return province.split(",")[0]
    }
}