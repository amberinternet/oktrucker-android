package th.co.growthd.oktrucker.ui.fragment

import android.arch.lifecycle.ViewModelProviders
import android.content.Intent
import android.databinding.DataBindingUtil
import android.os.Bundle
import android.support.v7.widget.LinearLayoutManager
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import org.greenrobot.eventbus.EventBus
import org.greenrobot.eventbus.Subscribe
import org.parceler.Parcels
import th.co.growthd.deerandbook.adapter.LinearDecoration
import th.co.growthd.deerandbook.ui.factory.FindWorkViewModelFactory
import th.co.growthd.deerandbook.util.*
import th.co.growthd.oktrucker.R
import th.co.growthd.oktrucker.adapter.WorkAdapter
import th.co.growthd.oktrucker.adapter.callback.OnRecyclerTouchListener
import th.co.growthd.oktrucker.addOnPropertyChanged
import th.co.growthd.oktrucker.databinding.FindWorkFragmentBinding
import th.co.growthd.oktrucker.model.subscriber.FilterWorkSubscriber
import th.co.growthd.oktrucker.model.subscriber.LoadMoreSubscriber
import th.co.growthd.oktrucker.model.subscriber.PermissionSubscriber
import th.co.growthd.oktrucker.ui.activity.FilterWorkActivity
import th.co.growthd.oktrucker.ui.activity.WorkDetailActivity
import th.co.growthd.oktrucker.ui.base.BaseFragment
import th.co.growthd.oktrucker.ui.viewmodel.FindWorkViewModel
import th.co.growthd.oktrucker.util.enum.WorkQuery
import android.app.Activity
import android.support.v4.content.ContextCompat
import kotlinx.android.synthetic.main.tab_bar_activity.*
import th.co.growthd.oktrucker.ui.activity.TabBarActivity

class FindWorkFragment : BaseFragment() {

    private lateinit var binding: FindWorkFragmentBinding
    private lateinit var workAdapter: WorkAdapter
    private lateinit var viewModel: FindWorkViewModel
    private lateinit var query: String

    companion object {
        fun newInstance() = FindWorkFragment()
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        initVariable()
    }

    private fun initVariable() {
        query = INITIAL_STRING
    }

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View {
        binding = DataBindingUtil.inflate(inflater, R.layout.find_work_fragment, container, false)
        EventBus.getDefault().register(this)
        return binding.root
    }

    override fun onActivityCreated(savedInstanceState: Bundle?) {
        super.onActivityCreated(savedInstanceState)
        viewModel = ViewModelProviders.of(this, FindWorkViewModelFactory(activity!!)).get(FindWorkViewModel::class.java)
        initVariable()
        initViewModel()
        initView()
        initOnClick()
    }

    private fun initViewModel() {
        workAdapter = WorkAdapter(viewModel.workList)
        viewModel.workPagination.addOnPropertyChanged {
            it.get()?.let {
                workAdapter.workPagination = it
                workAdapter.notifyDataSetChanged()
            }
        }
        viewModel.isRefresh.addOnPropertyChanged {
            binding.refreshLayout.isRefreshing = it.get()
        }
    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        if (requestCode == WORK_DETAIL_REQUEST) {
            if (resultCode == Activity.RESULT_OK) {
                (activity as TabBarActivity).navigation.selectedItemId = R.id.tab_bar_my_work
            } else {
                viewModel.isRefresh.set(true)
//                viewModel.getWorkList(1, WorkQuery.SORT_DESC.spinnerValue, query)
                refreshWorkList(page = 1)
            }
        }
    }

    override fun onDestroyView() {
        EventBus.getDefault().unregister(this)
        super.onDestroyView()
    }

    private fun initView() {
        binding.toolBar!!.titleTextView.text = getString(R.string.title_find_work)
        binding.recyclerView.apply {
            layoutManager = LinearLayoutManager(context, LinearLayoutManager.VERTICAL, false)
            adapter = workAdapter
            addItemDecoration(LinearDecoration(
                    resources.getDimension(R.dimen.margin_small).toInt(),
                    resources.getDimension(R.dimen.margin_tiny).toInt(),
                    true
            ))
            addOnItemTouchListener(OnRecyclerTouchListener(context!!) { position ->
                if (position < viewModel.workList.size) {
                    val intent = Intent(activity, WorkDetailActivity::class.java)
                    intent.putExtra(WORK_KEY, Parcels.wrap(viewModel.workList[position]))
                    startActivityForResult(intent, WORK_DETAIL_REQUEST)
                }
            })
        }

        binding.refreshLayout.setColorSchemeColors(ContextCompat.getColor(context!!, R.color.colorPrimary))
        binding.refreshLayout.setOnRefreshListener {
            viewModel.isRefresh.set(true)
//            viewModel.getWorkList(1, WorkQuery.SORT_DESC.spinnerValue, query)
            refreshWorkList(page = 1)
        }
    }

    private fun initOnClick() {
        binding.toolBar!!.filterImageView.setOnClickListener {
            val intent = Intent(activity, FilterWorkActivity::class.java)
            intent.putExtra(PROVINCE_KEY, query)
            startActivity(intent)
        }
    }

    @Subscribe
    fun onLoadMoreWork(loadMoreSubscriber: LoadMoreSubscriber) {
        if (loadMoreSubscriber.isLoadMoreWork) {
//            subscription = viewModel.getWorkList(loadMoreSubscriber.page, WorkQuery.SORT_DESC.spinnerValue, query)
            refreshWorkList(page = loadMoreSubscriber.page)
        }
    }

    @Subscribe
    fun onFilter(filterWorkSubscriber: FilterWorkSubscriber) {
        viewModel.isRefresh.set(true)
        query = filterWorkSubscriber.province
//        subscription = viewModel.getWorkList(1, WorkQuery.SORT_DESC.spinnerValue, query)
        refreshWorkList(page = 1)
        viewModel.workList.clear()
    }

    @Subscribe
    fun onEnablePermission(subscriber: PermissionSubscriber) {
//        viewModel.getWorkList(1, WorkQuery.SORT_DESC.spinnerValue, query)
        refreshWorkList(page = 1)
    }

    private fun refreshWorkList(page: Int) {
        val provinceNames = query.split(",")

        for (name in provinceNames) {
            subscription = viewModel.getWorkList(page, WorkQuery.SORT_DESC.value, name)
        }
    }
}