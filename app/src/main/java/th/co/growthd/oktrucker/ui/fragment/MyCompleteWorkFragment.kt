package th.co.growthd.oktrucker.ui.fragment

import android.arch.lifecycle.ViewModelProviders
import android.content.Intent
import android.databinding.DataBindingUtil
import android.os.Bundle
import android.support.v7.widget.LinearLayoutManager
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import org.greenrobot.eventbus.EventBus
import org.greenrobot.eventbus.Subscribe
import org.parceler.Parcels
import th.co.growthd.deerandbook.adapter.LinearDecoration
import th.co.growthd.deerandbook.ui.factory.MyCompleteWorkViewModelFactory
import th.co.growthd.deerandbook.util.WORK_DETAIL_REQUEST
import th.co.growthd.deerandbook.util.WORK_KEY
import th.co.growthd.oktrucker.R
import th.co.growthd.oktrucker.adapter.CompleteWorkAdapter
import th.co.growthd.oktrucker.adapter.callback.OnRecyclerTouchListener
import th.co.growthd.oktrucker.addOnPropertyChanged
import th.co.growthd.oktrucker.databinding.MyFinishWorkFragmentBinding
import th.co.growthd.oktrucker.model.subscriber.LoadMoreSubscriber
import th.co.growthd.oktrucker.ui.activity.WorkDetailActivity
import th.co.growthd.oktrucker.ui.base.BaseFragment
import th.co.growthd.oktrucker.ui.viewmodel.MyCompleteWorkViewModel

class MyCompleteWorkFragment : BaseFragment() {

    private lateinit var binding: MyFinishWorkFragmentBinding
    private lateinit var viewModel: MyCompleteWorkViewModel
    private lateinit var completeWorkAdapter: CompleteWorkAdapter

    companion object {
        fun newInstance() = MyCompleteWorkFragment()
    }

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View {
        binding = DataBindingUtil.inflate(inflater, R.layout.my_finish_work_fragment, container, false)
        EventBus.getDefault().register(this)
        return binding.root
    }

    override fun onActivityCreated(savedInstanceState: Bundle?) {
        super.onActivityCreated(savedInstanceState)
        initViewModel()
        initView()
    }

    private fun initViewModel() {
        viewModel = ViewModelProviders.of(this, MyCompleteWorkViewModelFactory(activity!!)).get(MyCompleteWorkViewModel::class.java)
        completeWorkAdapter = CompleteWorkAdapter(viewModel.workList)
        viewModel.workPagination.addOnPropertyChanged {
            it.get()?.let {
                completeWorkAdapter.workPagination = it
                completeWorkAdapter.notifyDataSetChanged()
            }
        }
        viewModel.isRefresh.addOnPropertyChanged {
            binding.refreshLayout.isRefreshing = it.get()
        }
    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        if (requestCode == WORK_DETAIL_REQUEST) {
            viewModel.isRefresh.set(true)
            subscription = viewModel.getMyCompleteWorkList(1)
        }
    }

    override fun onDestroyView() {
        EventBus.getDefault().unregister(this)
        super.onDestroyView()
    }

    private fun initView() {
        binding.recyclerView.apply {
            layoutManager = LinearLayoutManager(context, LinearLayoutManager.VERTICAL, false)
            adapter = completeWorkAdapter
            addItemDecoration(LinearDecoration(
                    resources.getDimension(R.dimen.margin_small).toInt(),
                    resources.getDimension(R.dimen.margin_tiny).toInt(),
                    true
            ))
            addOnItemTouchListener(OnRecyclerTouchListener(context!!) { position ->
                val intent = Intent(activity, WorkDetailActivity::class.java)
                intent.putExtra(WORK_KEY, Parcels.wrap(viewModel.workList[position]))
                startActivityForResult(intent, WORK_DETAIL_REQUEST)
            })
        }

        binding.refreshLayout.setColorSchemeColors(context!!.resources.getColor(R.color.colorPrimary))
        binding.refreshLayout.setOnRefreshListener {
            viewModel.isRefresh.set(true)
            subscription = viewModel.getMyCompleteWorkList(1)
        }
    }

    @Subscribe
    fun onLoadMoreMyCompleteWork(loadMoreSubscriber: LoadMoreSubscriber) {
        if (loadMoreSubscriber.isLoadMoreMyCompleteWork) {
            subscription = viewModel.getMyCompleteWorkList(loadMoreSubscriber.page)
        }
    }
}