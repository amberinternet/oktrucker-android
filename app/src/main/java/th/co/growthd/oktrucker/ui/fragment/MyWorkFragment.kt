package th.co.growthd.oktrucker.ui.fragment

import android.arch.lifecycle.ViewModelProviders
import android.databinding.DataBindingUtil
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import th.co.growthd.deerandbook.ui.factory.MyWorkViewModelFactory
import th.co.growthd.oktrucker.R
import th.co.growthd.oktrucker.databinding.MyWorkFragmentBinding
import th.co.growthd.oktrucker.ui.base.BaseFragment
import android.widget.TextView
import com.ogaclejapan.smarttablayout.utils.v4.FragmentPagerItems
import com.ogaclejapan.smarttablayout.utils.v4.FragmentPagerItemAdapter
import th.co.growthd.oktrucker.ui.viewmodel.MyWorkViewModel
import android.support.v4.view.ViewPager
import android.graphics.Typeface
import android.widget.LinearLayout

class MyWorkFragment : BaseFragment() {

    private lateinit var binding: MyWorkFragmentBinding
    private lateinit var viewModel: MyWorkViewModel
    private lateinit var tabAdapter: FragmentPagerItemAdapter
    private lateinit var tabArray: Array<String>

    companion object {
        fun newInstance() = MyWorkFragment()
    }

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View {
        binding = DataBindingUtil.inflate(inflater, R.layout.my_work_fragment, container, false)
        return binding.root
    }

    override fun onActivityCreated(savedInstanceState: Bundle?) {
        super.onActivityCreated(savedInstanceState)
        viewModel = ViewModelProviders.of(this, MyWorkViewModelFactory(activity!!)).get(MyWorkViewModel::class.java)
        initView()
    }

    private fun initView() {
        binding.toolBar!!.titleTextView.setText(R.string.title_my_work)
        tabArray = resources.getStringArray(R.array.tab_work_array)
        tabAdapter = FragmentPagerItemAdapter(childFragmentManager,
                FragmentPagerItems.with(activity)
                        .add(R.string.title_tab_in_progress_work, MyInProgressWorkFragment::class.java)
                        .add(R.string.title_tab_finish_work, MyCompleteWorkFragment::class.java)
                        .create()
        )
        binding.viewPager.apply {
            adapter = tabAdapter
        }
        binding.tabLayout.apply {
            setCustomTabView { container, position, _ ->
                val view = LayoutInflater.from(container.context).inflate(R.layout.custom_view_smart_tab, container, false)
                val name = view.findViewById<View>(R.id.tab_name_text_view) as TextView
                name.text = tabArray[position]
                view
            }
            setViewPager(binding.viewPager)

            val tabName0 = binding.tabLayout.getTabAt(0).findViewById<View>(R.id.tab_name_text_view) as TextView
            val tabName1 = binding.tabLayout.getTabAt(1).findViewById<View>(R.id.tab_name_text_view) as TextView
            tabName0.setTypeface(null, Typeface.BOLD)
            setOnPageChangeListener(object : ViewPager.OnPageChangeListener {
                override fun onPageScrolled(position: Int, positionOffset: Float, positionOffsetPixels: Int) {}
                override fun onPageSelected(position: Int) {
                    when (position) {
                        0 -> {
                            tabName0.setTypeface(null, Typeface.BOLD)
                            tabName1.setTypeface(null, Typeface.NORMAL)
                        }
                        1 -> {
                            tabName0.setTypeface(null, Typeface.NORMAL)
                            tabName1.setTypeface(null, Typeface.BOLD)
                        }
                    }
                }
                override fun onPageScrollStateChanged(state: Int) {}
            })
        }
    }
}