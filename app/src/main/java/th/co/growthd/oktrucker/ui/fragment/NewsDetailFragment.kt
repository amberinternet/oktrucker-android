package th.co.growthd.oktrucker.ui.fragment

import android.arch.lifecycle.ViewModelProviders
import android.databinding.DataBindingUtil
import android.os.Bundle
import android.support.v4.content.ContextCompat
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import org.parceler.Parcels
import th.co.growthd.deerandbook.ui.factory.NewsDetailViewModelFactory
import th.co.growthd.deerandbook.util.NEWS_KEY
import th.co.growthd.oktrucker.R
import th.co.growthd.oktrucker.addOnPropertyChanged
import th.co.growthd.oktrucker.databinding.NewsDetailFragmentBinding
import th.co.growthd.oktrucker.fromDateTime
import th.co.growthd.oktrucker.model.News
import th.co.growthd.oktrucker.toDateTimeAppFormat
import th.co.growthd.oktrucker.ui.base.BaseFragment
import th.co.growthd.oktrucker.ui.viewmodel.NewsDetailViewModel

class NewsDetailFragment : BaseFragment() {

    private lateinit var binding: NewsDetailFragmentBinding

    private lateinit var news: News
    private lateinit var viewModel: NewsDetailViewModel
    private val htmlTagOpen = "<html>\n" +
            "      <head>\n" +
            "            <meta name=\"viewport\" content=\"width=device-width, initial-scale=1\">\n" +
            "            <style type=\"text/css\">\n" +
            "                  @font-face {\n" +
            "                        font-family: \"Tahoma\";\n" +
            "                        src: url(\"file:///android_asset/fonts/Tahoma-Regular.ttf\")\n" +
            "                  }\n" +
            "                  body {\n" +
            "                        font-family: \"Tahoma\"; \n" +
            "                        margin: auto;\n" +
            "                        background: \"#f0f0f0\"; \n" +
            "                        line-height: \"0.85\"; \n" +
            "                        padding-bottom: 16px; \n" +
            "                  } \n" +
            "                  p {\n" +
            "                        font-size: 16px;\n" +
            "                  }\n" +
            "            </style>\n" +
            "      </head>\n" +
            "      <body>"

    private val htmlTagClose = "</body></html>"

    companion object {
        fun newInstance(news: News): NewsDetailFragment {
            val fragment = NewsDetailFragment()
            val bundle = Bundle()
            bundle.putParcelable(NEWS_KEY, Parcels.wrap(news))
            fragment.arguments = bundle
            return fragment
        }
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        arguments?.let {
            news = Parcels.unwrap(it.getParcelable(NEWS_KEY))
        }
    }

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View {
        binding = DataBindingUtil.inflate(inflater, R.layout.news_detail_fragment, container, false)
        return binding.root
    }

    override fun onActivityCreated(savedInstanceState: Bundle?) {
        super.onActivityCreated(savedInstanceState)
        viewModel = ViewModelProviders.of(this, NewsDetailViewModelFactory(activity!!)).get(NewsDetailViewModel::class.java)
        viewModel.isLoading.addOnPropertyChanged {
            if (!it.get()) {
                activity?.finish()
            }
        }
        viewModel.news.addOnPropertyChanged {
            initNews(it.get()!!)
        }
        initOnClick()
    }

    override fun onResume() {
        super.onResume()
        if (viewModel.news.get() == null) {
            subscription = viewModel.getNewsDetail(news.id)
        }
    }

    private fun initNews(news: News) {
        val styleWidth = "<img"
        val newStyleWidth = "<img style=\"max-width: 100%;\""
        val iframeWidth = "<iframe"
        val newIframeWidth = "<iframe style=\"max-width: 100%; height: auto;\""
        val content = news.fullContent.replace(styleWidth, newStyleWidth).replace(iframeWidth, newIframeWidth)
        binding.titleTextView.text = news.title
        binding.dateTextView.text = news.postAt.fromDateTime().toDateTimeAppFormat()
        binding.webView.settings.javaScriptEnabled = true
        binding.webView.apply {
            settings.javaScriptEnabled = true
            settings.useWideViewPort = true
//            setInitialScale(1)
            setBackgroundColor(ContextCompat.getColor(context, R.color.colorWhite))
            loadDataWithBaseURL(null, htmlTagOpen + content + htmlTagClose, "text/html; charset=utf-8", "UTF-8", null)
        }
    }

    fun initOnClick() {
        binding.toolBar!!.titleTextView.setText(R.string.title_news_detail)
        binding.toolBar!!.backImageView.setOnClickListener {
            activity!!.onBackPressed()
        }
    }
}