package th.co.growthd.oktrucker.ui.fragment

import android.arch.lifecycle.ViewModelProviders
import android.content.Intent
import android.databinding.DataBindingUtil
import android.os.Bundle
import android.support.v7.widget.LinearLayoutManager
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import org.greenrobot.eventbus.EventBus
import org.greenrobot.eventbus.Subscribe
import org.parceler.Parcels
import th.co.growthd.deerandbook.adapter.LinearDecoration
import th.co.growthd.deerandbook.ui.factory.NewsViewModelFactory
import th.co.growthd.deerandbook.util.NEWS_KEY
import th.co.growthd.oktrucker.R
import th.co.growthd.oktrucker.adapter.NewsAdapter
import th.co.growthd.oktrucker.adapter.callback.OnRecyclerTouchListener
import th.co.growthd.oktrucker.addOnPropertyChanged
import th.co.growthd.oktrucker.databinding.NewsFragmentBinding
import th.co.growthd.oktrucker.model.News
import th.co.growthd.oktrucker.model.subscriber.LoadMoreSubscriber
import th.co.growthd.oktrucker.ui.activity.NewsDetailActivity
import th.co.growthd.oktrucker.ui.base.BaseFragment
import th.co.growthd.oktrucker.ui.viewmodel.NewsViewModel

class NewsFragment : BaseFragment() {

    private lateinit var binding: NewsFragmentBinding
    private lateinit var newsAdapter: NewsAdapter
    private lateinit var viewModel: NewsViewModel

    companion object {
        fun newInstance() = NewsFragment()
    }

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View {
        binding = DataBindingUtil.inflate(inflater, R.layout.news_fragment, container, false)
        EventBus.getDefault().register(this)
        return binding.root
    }

    override fun onActivityCreated(savedInstanceState: Bundle?) {
        super.onActivityCreated(savedInstanceState)
        viewModel = ViewModelProviders.of(this, NewsViewModelFactory(activity!!)).get(NewsViewModel::class.java)
        initViewModel()
        initView()
    }

    private fun initViewModel() {
        viewModel.newsPagination.addOnPropertyChanged {
            it.get()?.let {
                newsAdapter.newsPagination = it
                newsAdapter.notifyDataSetChanged()
            }
        }
        viewModel.isRefresh.addOnPropertyChanged {
            binding.refreshLayout.isRefreshing = it.get()
        }
    }

    override fun onDestroyView() {
        EventBus.getDefault().unregister(this)
        super.onDestroyView()
    }

    private fun initView() {
        newsAdapter = NewsAdapter(viewModel.newsList)
        binding.toolBar!!.titleTextView.text = getString(R.string.title_news)
        binding.recyclerView.apply {
            layoutManager = LinearLayoutManager(context, LinearLayoutManager.VERTICAL, false)
            adapter = newsAdapter
            addItemDecoration(LinearDecoration(
                    resources.getDimension(R.dimen.margin_news).toInt(),
                    0,
                    true
            ))
            addOnItemTouchListener(OnRecyclerTouchListener(context!!) { position ->
                if (position < viewModel.newsList.size) {
                    val intent = Intent(activity, NewsDetailActivity::class.java)
                    intent.putExtra(NEWS_KEY, Parcels.wrap(viewModel.newsList[position]))
                    startActivity(intent)
                }
            })

            binding.refreshLayout.setColorSchemeColors(context!!.resources.getColor(R.color.colorPrimary))
            binding.refreshLayout.setOnRefreshListener {
                viewModel.isRefresh.set(true)
                viewModel.getNewsList()
            }
        }
    }

    @Subscribe
    fun onLoadMoreNews(loadMoreSubscriber: LoadMoreSubscriber) {
        if (loadMoreSubscriber.isLoadMoreNews) {
            subscription = viewModel.getNewsList(loadMoreSubscriber.page)
        }
    }
}