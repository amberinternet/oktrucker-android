package th.co.growthd.oktrucker.ui.fragment

import android.content.Intent
import android.databinding.DataBindingUtil
import android.os.Bundle
import android.support.v7.widget.LinearLayoutManager
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import th.co.growthd.deerandbook.util.PRICE_KEY
import th.co.growthd.oktrucker.R
import th.co.growthd.oktrucker.adapter.CreditAdapter
import th.co.growthd.oktrucker.adapter.callback.OnRecyclerTouchListener
import th.co.growthd.oktrucker.databinding.PaymentMethodFragmentBinding
import th.co.growthd.oktrucker.model.User
import th.co.growthd.oktrucker.ui.activity.CreditCardMethodActivity
import th.co.growthd.oktrucker.ui.activity.TransferMethodActivity
import th.co.growthd.oktrucker.ui.base.BaseFragment

class PaymentMethodFragment : BaseFragment() {

    private lateinit var binding: PaymentMethodFragmentBinding
    private lateinit var price: String

    companion object {
        fun newInstance(price: String): PaymentMethodFragment {
            val fragment = PaymentMethodFragment()
            val bundle = Bundle().apply { putString(PRICE_KEY, price) }
            fragment.arguments = bundle
            return fragment
        }
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        initVariable()
    }

    private fun initVariable() {
        arguments?.let {
            price = it.getString(PRICE_KEY)
        }
    }

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View {
        binding = DataBindingUtil.inflate(inflater, R.layout.payment_method_fragment, container, false)
        return binding.root
    }

    override fun onActivityCreated(savedInstanceState: Bundle?) {
        super.onActivityCreated(savedInstanceState)
        initView()
        initOnClick()
    }

    private fun initView() {
        binding.toolBar!!.titleTextView.setText(R.string.title_payment)
        binding.priceTextView.text = price
    }

    private fun initOnClick() {
        binding.toolBar!!.backImageView.setOnClickListener {
            activity?.finish()
        }

        binding.transferLayout.setOnClickListener {
            val intent = Intent(activity, TransferMethodActivity::class.java)
            intent.putExtra(PRICE_KEY, price)
            startActivity(intent)
        }

//        binding.creditCardLayout.setOnClickListener {
//            val intent = Intent(activity, CreditCardMethodActivity::class.java)
//            intent.putExtra(PRICE_KEY, price)
//            startActivity(intent)
//        }
    }
}