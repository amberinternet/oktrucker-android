package th.co.growthd.oktrucker.ui.fragment

import android.databinding.DataBindingUtil
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import th.co.growthd.oktrucker.R
import th.co.growthd.oktrucker.databinding.PrivacyPolicyFragmentBinding
import th.co.growthd.oktrucker.ui.base.BaseFragment

class PrivacyPolicyFragment: BaseFragment() {

    private lateinit var binding: PrivacyPolicyFragmentBinding

    companion object {
        fun newInstance() = PrivacyPolicyFragment()
    }

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View {
        binding = DataBindingUtil.inflate(inflater, R.layout.privacy_policy_fragment, container, false)
        return binding.root
    }

    override fun onActivityCreated(savedInstanceState: Bundle?) {
        super.onActivityCreated(savedInstanceState)
        initView()
        initOnClick()
    }

    private fun initView() {
        binding.toolBar!!.titleTextView.text = getString(R.string.privacy_policy)
    }

    private fun initOnClick() {
        binding.toolBar!!.backImageView.setOnClickListener {
            activity?.onBackPressed()
        }
    }
}