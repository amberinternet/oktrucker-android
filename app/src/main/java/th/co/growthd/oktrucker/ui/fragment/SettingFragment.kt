package th.co.growthd.oktrucker.ui.fragment

import android.annotation.SuppressLint
import android.content.Intent
import android.databinding.DataBindingUtil
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import th.co.growthd.oktrucker.databinding.SettingFragmentBinding
import th.co.growthd.oktrucker.model.User
import th.co.growthd.oktrucker.ui.activity.AboutApplicationActivity
import th.co.growthd.oktrucker.ui.activity.PrivacyPolicyActivity
import th.co.growthd.oktrucker.ui.activity.TermAndConditionActivity
import th.co.growthd.oktrucker.ui.base.BaseFragment
import th.co.growthd.oktrucker.util.manager.UserManager
import th.co.growthd.oktrucker.util.manager.DialogManager
import android.arch.lifecycle.ViewModelProviders
import th.co.growthd.deerandbook.ui.factory.SettingViewModelFactory
import th.co.growthd.oktrucker.*
import th.co.growthd.oktrucker.ui.viewmodel.SettingViewModel


class SettingFragment : BaseFragment() {

    private lateinit var binding: SettingFragmentBinding
    private lateinit var dialogManager: DialogManager
    private lateinit var userManager: UserManager
    private lateinit var viewModel: SettingViewModel
    private lateinit var user: User

    companion object {
        fun newInstance() = SettingFragment()
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        initVariable()
    }

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View {
        binding = DataBindingUtil.inflate(inflater, R.layout.setting_fragment, container, false)
        return binding.root
    }

    private fun initVariable() {
        user = User.instance
        dialogManager = DialogManager.getInstance(childFragmentManager)
        userManager = UserManager(context!!)
    }

    override fun onActivityCreated(savedInstanceState: Bundle?) {
        super.onActivityCreated(savedInstanceState)
        viewModel = ViewModelProviders.of(this, SettingViewModelFactory(activity!!)).get(SettingViewModel::class.java)
        initView()
        initOnClick()
    }

    @SuppressLint("SetTextI18n")
    private fun initView() {
        binding.toolBar!!.titleTextView.setText(R.string.title_setting)
        binding.buildVersionTextView.text = "แอพเวอร์ชั่น: ${BuildConfig.VERSION_NAME} build ${BuildConfig.VERSION_CODE}"
        binding.profileImageView.setProfileImage(user.profileImage)
        binding.driverIdTextView.text = user.driver.readableDriverId
        binding.fullNameTextView.text = user.getFullName()
        binding.truckTextView.text = "${user.driver.truck.truckType.name} / ${user.driver.truck.licensePlate}"
        binding.telephoneTextView.text = user.telephoneNumber
        binding.bankAccountTextView.text = "ธนาคาร${user.driver.bankAccount.bank} / ${user.driver.bankAccount.accountNumber}"
        binding.joinedTextView.text = user.driver.createdAt.fromDateTime().toDateAppFormat()
//        binding.notificationSwitch.isChecked = userManager.isEnableNotification
    }

    private fun initOnClick() {
//        binding.termAndConditionLayout.setOnClickListener {
//            val intent = Intent(activity, TermAndConditionActivity::class.java)
//            startActivity(intent)
//        }
//
//        binding.privacyPolicyLayout.setOnClickListener {
//            val intent = Intent(activity, PrivacyPolicyActivity::class.java)
//            startActivity(intent)
//        }
//
//        binding.aboutApplicationLayout.setOnClickListener {
//            val intent = Intent(activity, AboutApplicationActivity::class.java)
//            startActivity(intent)
//        }
//
        binding.logOutButton.setOnClickListener {
            dialogManager.showConfirm(R.string.confirm_log_out, R.string.message_confirm_log_out) {
                viewModel.logOut()
            }
        }
//
//        binding.notificationSwitch.setOnCheckedChangeListener { _, isChecked ->
//            UserManager.getInstance(context!!).setEnableNotification(isChecked)
//        }
    }
}