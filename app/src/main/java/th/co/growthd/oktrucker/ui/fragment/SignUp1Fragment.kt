package th.co.growthd.oktrucker.ui.fragment

import android.databinding.DataBindingUtil
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.AdapterView
import org.greenrobot.eventbus.EventBus
import th.co.growthd.oktrucker.*
import th.co.growthd.oktrucker.adapter.SpinnerAdapter
import th.co.growthd.oktrucker.databinding.SignUp1FragmentBinding
import th.co.growthd.oktrucker.model.DriverType
import th.co.growthd.oktrucker.model.User
import th.co.growthd.oktrucker.model.subscriber.SignUpSubscriber
import th.co.growthd.oktrucker.ui.base.BaseFragment
import th.co.growthd.oktrucker.util.manager.DialogManager
import th.co.growthd.oktrucker.util.network.service.AppService
import java.util.*

class SignUp1Fragment : BaseFragment() {

    private lateinit var binding: SignUp1FragmentBinding
    private lateinit var licenseTypeArray: Array<String>
//    private lateinit var driverTypeArray: Array<String>
    private lateinit var user: User
    private lateinit var dialogManager: DialogManager
    private var driverTypeList = ArrayList<DriverType>()

    companion object {
        fun newInstance() = SignUp1Fragment()
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        initVariable()
    }

    private fun initVariable() {
        user = User.instance
        dialogManager = DialogManager.getInstance(childFragmentManager)
    }

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View {
        binding = DataBindingUtil.inflate(inflater, R.layout.sign_up_1_fragment, container, false)
        return binding.root
    }

    override fun onActivityCreated(savedInstanceState: Bundle?) {
        super.onActivityCreated(savedInstanceState)

//        driverTypeArray = resources.getStringArray(R.array.driver_type_array)
//        for (i in 0..3) {
//            driverTypeList.add(DriverType(id = i, name = driverTypeArray[i]))
//        }

        initView()
        initOnClick()
    }

    private fun initView() {
        licenseTypeArray = resources.getStringArray(R.array.license_type_array)
        binding.licenseTypeSpinner.apply {
            adapter = SpinnerAdapter().apply { stringArray = licenseTypeArray }
            onItemSelectedListener = object : AdapterView.OnItemSelectedListener {
                override fun onNothingSelected(parent: AdapterView<*>?) {

                }

                override fun onItemSelected(parent: AdapterView<*>?, view: View?, position: Int, id: Long) {
                }
            }
        }

//        binding.driverTypeSpinner.apply {
//            adapter = SpinnerAdapter().apply { stringArray = driverTypeArray }
//            onItemSelectedListener = object : AdapterView.OnItemSelectedListener {
//                override fun onNothingSelected(parent: AdapterView<*>?) {
//
//                }
//
//                override fun onItemSelected(parent: AdapterView<*>?, view: View?, position: Int, id: Long) {
//                    if (position == 2 || position == 3) {
//                        binding.companyLayout.visibility = View.VISIBLE
//                    } else {
//                        binding.companyLayout.visibility = View.GONE
//                    }
//                }
//            }
//        }

        binding.firstNameEditText.setText(user.firstname)
        binding.lastNameEditText.setText(user.lastname)
        binding.emailEditText.setText(user.email)
        binding.citizenIdEditText.setText(user.driver?.citizenId)
        binding.currentAddressEditText.setText(user.address)
        binding.licenseIdEditText.setText(user.driver?.driverLicenseId)
        binding.licenseTypeSpinner.setSelection(user.driver?.driverLicenseTypePosition ?: 0)
//        binding.driverTypeSpinner.setSelection(getDriverTypePosition())
//        binding.companyNameEditText.setText(user.driver?.companyName)
    }

//    private fun getDriverTypePosition(): Int {
//        for ((index, driverType) in driverTypeList.withIndex()) {
//            if (driverType.id == user.driver?.driverType?.id) {
//                return index
//            }
//        }
//
//        return 0
//    }

    private fun initOnClick() {
        binding.nextTextView.setOnClickListener {
            if (validate()) {
                user.firstname = binding.firstNameEditText.string()
                user.lastname = binding.lastNameEditText.string()
                user.email = binding.emailEditText.string()
                user.driver.citizenId = binding.citizenIdEditText.string()
                user.address = binding.currentAddressEditText.string()
                user.driver.driverLicenseId = binding.licenseIdEditText.string()
                user.driver.driverLicenseTypePosition = binding.licenseTypeSpinner.selectedItemPosition
                user.driver.driverLicenseType = licenseTypeArray[binding.licenseTypeSpinner.selectedItemPosition]
                user.driver.driverTypeId = 1 /*binding.driverTypeSpinner.selectedItemPosition*/
                user.driver.companyName = "" /*binding.companyNameEditText.string()*/
                EventBus.getDefault().post(SignUpSubscriber(2))
            }
        }
    }

    fun validate(): Boolean {
        return when {
            binding.firstNameEditText.isEmpty() -> {
                dialogManager.showAlert(R.string.please_fill_out_first_name)
                false
            }
            binding.lastNameEditText.isEmpty() -> {
                dialogManager.showAlert(R.string.please_fill_out_last_name)
                false
            }
            binding.emailEditText.isEmpty() -> {
                dialogManager.showAlert(R.string.please_fill_out_email)
                false
            }
            !binding.emailEditText.isValidEmail() -> {
                dialogManager.showAlert(R.string.invalid_email)
                false
            }
            binding.citizenIdEditText.isEmpty() -> {
                dialogManager.showAlert(R.string.please_fill_out_citizen_id)
                false
            }
            binding.currentAddressEditText.isEmpty() -> {
                dialogManager.showAlert(R.string.please_fill_out_address)
                false
            }
            binding.licenseIdEditText.isEmpty() -> {
                dialogManager.showAlert(R.string.please_fill_out_driver_license)
                false
            }
            binding.licenseTypeSpinner.isNotSelected() -> {
                dialogManager.showAlert(R.string.please_select_driver_license_type)
                false
            }
//            binding.driverTypeSpinner.isNotSelected() -> {
//                dialogManager.showAlert(R.string.please_select_driver_type)
//                false
//            }
//            binding.driverTypeSpinner.selectedItemPosition > 1 && binding.companyNameEditText.isEmpty() -> {
//                dialogManager.showAlert(R.string.please_fill_out_company_name)
//                false
//            }
            else -> true
        }
    }
}