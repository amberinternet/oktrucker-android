package th.co.growthd.oktrucker.ui.fragment

import android.databinding.DataBindingUtil
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.AdapterView
import org.greenrobot.eventbus.EventBus
import org.greenrobot.eventbus.Subscribe
import th.co.growthd.oktrucker.R
import th.co.growthd.oktrucker.adapter.SpinnerAdapter
import th.co.growthd.oktrucker.databinding.SignUp2FragmentBinding
import th.co.growthd.oktrucker.isEmpty
import th.co.growthd.oktrucker.isNotSelected
import th.co.growthd.oktrucker.model.User
import th.co.growthd.oktrucker.model.subscriber.SignUpSubscriber
import th.co.growthd.oktrucker.model.subscriber.TruckTypeSubscriber
import th.co.growthd.oktrucker.string
import th.co.growthd.oktrucker.ui.base.BaseFragment
import th.co.growthd.oktrucker.util.manager.DialogManager

class SignUp2Fragment : BaseFragment() {

    private lateinit var binding: SignUp2FragmentBinding
    private lateinit var truckProvinceArray: Array<String>
    private lateinit var dialogManager: DialogManager
    private lateinit var user: User

    companion object {
        fun newInstance() = SignUp2Fragment()
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        initVariable()
    }

    private fun initVariable() {
        user = User.instance
        dialogManager = DialogManager.getInstance(childFragmentManager)
        truckProvinceArray = resources.getStringArray(R.array.province_array)
    }

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View {
        binding = DataBindingUtil.inflate(inflater, R.layout.sign_up_2_fragment, container, false)
        EventBus.getDefault().register(this)
        return binding.root
    }

    override fun onActivityCreated(savedInstanceState: Bundle?) {
        super.onActivityCreated(savedInstanceState)
        initView()
        initOnClick()
    }

    override fun onDestroyView() {
        EventBus.getDefault().unregister(this)
        super.onDestroyView()
    }

    private fun initView() {
        binding.truckProvinceSpinner.apply {
            adapter = SpinnerAdapter().apply { stringArray = truckProvinceArray }
            onItemSelectedListener = object : AdapterView.OnItemSelectedListener {
                override fun onNothingSelected(parent: AdapterView<*>?) {

                }

                override fun onItemSelected(parent: AdapterView<*>?, view: View?, position: Int, id: Long) {

                }
            }
        }

        binding.truckTypeTextView.text = user.driver.truck.truckType.name
        binding.licensePlateEditText.setText(user.driver.truck.licensePlate)
        binding.truckYearEditText.setText(user.driver.truck.truckYear)
        binding.truckBrandEditText.setText(user.driver.truck.truckBrand)
        binding.truckModelEditText.setText(user.driver.truck.truckModel)
        binding.truckProvinceSpinner.setSelection(user.driver.truck.truckProvinceId)
    }

    private fun initOnClick() {
        binding.truckTypeTextView.setOnClickListener {
            DialogManager.getInstance(childFragmentManager).showTruckType(user.driver?.truck?.truckType)
        }
        binding.nextTextView.setOnClickListener {
            if (validate()) {
                user.driver.truck.licensePlate = binding.licensePlateEditText.string()
                user.driver.truck.truckYear = binding.truckYearEditText.string()
                user.driver.truck.truckBrand = binding.truckBrandEditText.string()
                user.driver.truck.truckModel = binding.truckModelEditText.string()
                user.driver.truck.truckProvinceId = binding.truckProvinceSpinner.selectedItemPosition
                user.driver.truck.truckProvince = truckProvinceArray[binding.truckProvinceSpinner.selectedItemPosition]
                EventBus.getDefault().post(SignUpSubscriber(3))
            }
        }
    }

    fun validate(): Boolean {
        return when {
            binding.truckTypeTextView.isEmpty() -> {
                dialogManager.showAlert(R.string.please_select_truck_type)
                false
            }
            binding.licensePlateEditText.isEmpty() -> {
                dialogManager.showAlert(R.string.please_fill_out_license_plate)
                false
            }
            binding.truckYearEditText.isEmpty() -> {
                dialogManager.showAlert(R.string.please_fill_out_truck_year)
                false
            }
            binding.truckBrandEditText.isEmpty() -> {
                dialogManager.showAlert(R.string.please_fill_out_truck_brand)
                false
            }
            binding.truckModelEditText.isEmpty() -> {
                dialogManager.showAlert(R.string.please_fill_out_truck_model)
                false
            }
            binding.truckProvinceSpinner.isNotSelected() -> {
                dialogManager.showAlert(R.string.please_select_truck_province)
                false
            }
            else -> true
        }
    }

    @Subscribe
    fun onSelectTruckType(subscriber: TruckTypeSubscriber) {
        subscriber.truckType?.let {
            binding.truckTypeTextView.text = it.name
            user.driver?.truck?.truckType = it
        }
    }
}