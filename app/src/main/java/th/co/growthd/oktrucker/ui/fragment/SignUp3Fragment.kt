package th.co.growthd.oktrucker.ui.fragment

import android.arch.lifecycle.ViewModelProviders
import android.content.Intent
import android.databinding.DataBindingUtil
import android.os.Bundle
import android.support.v4.content.ContextCompat
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import com.klinker.android.link_builder.Link
import com.klinker.android.link_builder.applyLinks
import th.co.growthd.deerandbook.ui.factory.SignUp3ViewModelFactory
import th.co.growthd.oktrucker.*
import th.co.growthd.oktrucker.databinding.SignUp3FragmentBinding
import th.co.growthd.oktrucker.model.User
import th.co.growthd.oktrucker.ui.activity.PrivacyPolicyActivity
import th.co.growthd.oktrucker.ui.activity.TermAndConditionActivity
import th.co.growthd.oktrucker.ui.base.BaseFragment
import th.co.growthd.oktrucker.ui.viewmodel.SignUp3ViewModel
import th.co.growthd.oktrucker.util.manager.DialogManager

class SignUp3Fragment : BaseFragment() {

    private lateinit var binding: SignUp3FragmentBinding
    private val user: User = User.instance
    private lateinit var dialogManager: DialogManager
    private lateinit var viewModel: SignUp3ViewModel

    companion object {
        fun newInstance() = SignUp3Fragment()
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        dialogManager = DialogManager.getInstance(childFragmentManager)
    }

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View {
        binding = DataBindingUtil.inflate(inflater, R.layout.sign_up_3_fragment, container, false)
        return binding.root
    }

    override fun onActivityCreated(savedInstanceState: Bundle?) {
        super.onActivityCreated(savedInstanceState)
        viewModel = ViewModelProviders.of(this, SignUp3ViewModelFactory(activity!!)).get(SignUp3ViewModel::class.java)
        initView()
        initOnClick()
    }

    private fun initView() {
        val termLink = Link("ข้อตกลงและเงื่อนไขการใช้บริการ").apply {
            textColor = ContextCompat.getColor(context!!, R.color.colorPrimaryText)
            underlined = true
            setOnClickListener {
                val intent = Intent(activity, TermAndConditionActivity::class.java)
                startActivity(intent)
            }
        }

        val privacyLink = Link("นโยบายความเป็นส่วนตัว").apply {
            textColor = ContextCompat.getColor(context!!, R.color.colorPrimaryText)
            underlined = true
            setOnClickListener {
                val intent = Intent(activity, PrivacyPolicyActivity::class.java)
                startActivity(intent)
            }
        }

        binding.acceptTextView.applyLinks(termLink, privacyLink)
        binding.telephoneEditText.setText(user.username)
        binding.passwordEditText.setText(user.password)
        binding.confirmPasswordEditText.setText(user.confirmPassword)
        binding.acceptCheckBox.isChecked = user.isAcceptTerm
    }

    private fun initOnClick() {
        binding.signUpTextView.setOnClickListener {
            if (validate()) {
                user.telephoneNumber = binding.telephoneEditText.string()
                user.username = binding.telephoneEditText.string()
                user.password = binding.passwordEditText.string()
                user.confirmPassword = binding.confirmPasswordEditText.string()
                viewModel.signUp()
            }
        }
    }

    fun validate(): Boolean {
        return when {
            binding.telephoneEditText.isEmpty() -> {
                dialogManager.showAlert(R.string.please_fill_out_telephone)
                false
            }
            !binding.telephoneEditText.isValidTelephone() -> {
                dialogManager.showAlert(R.string.invalid_telephone)
                false
            }
            binding.passwordEditText.isEmpty() -> {
                dialogManager.showAlert(R.string.please_fill_out_password)
                false
            }
            binding.confirmPasswordEditText.isEmpty() -> {
                dialogManager.showAlert(R.string.please_fill_out_confirm_password)
                false
            }
            !binding.confirmPasswordEditText.confirmPassword(binding.passwordEditText) -> {
                dialogManager.showAlert(R.string.password_not_match)
                false
            }
            !binding.acceptCheckBox.isChecked -> {
                dialogManager.showAlert(R.string.please_agree_term_and_policy)
                false
            }
            else -> true
        }
    }
}