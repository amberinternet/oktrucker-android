package th.co.growthd.oktrucker.ui.fragment

import android.Manifest
import android.databinding.DataBindingUtil
import android.os.Bundle
import android.support.v4.content.ContextCompat
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import org.greenrobot.eventbus.EventBus
import org.greenrobot.eventbus.Subscribe
import th.co.growthd.deerandbook.util.REQUEST_CODE_LOCATION_PERMISSION
import th.co.growthd.oktrucker.R
import th.co.growthd.oktrucker.databinding.SignUpFragmentBinding
import th.co.growthd.oktrucker.model.subscriber.SignUpSubscriber
import th.co.growthd.oktrucker.ui.base.BaseFragment

class SignUpFragment : BaseFragment() {

    private lateinit var binding: SignUpFragmentBinding

    companion object {
        fun newInstance() = SignUpFragment()
    }

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View {
        binding = DataBindingUtil.inflate(inflater, R.layout.sign_up_fragment, container, false)
        EventBus.getDefault().register(this)
        return binding.root
    }

    override fun onActivityCreated(savedInstanceState: Bundle?) {
        super.onActivityCreated(savedInstanceState)
        initView()
        initOnClick()
    }

    override fun onDestroyView() {
        EventBus.getDefault().unregister(this)
        super.onDestroyView()
    }

    private fun initView() {
        setTab(1)
    }

    private fun initOnClick() {
        binding.toolBar!!.titleTextView.setText(R.string.title_sign_up)
        binding.toolBar!!.backImageView.setOnClickListener {
            activity?.onBackPressed()
        }
    }

    @Subscribe
    fun onSelectTab(subscriber: SignUpSubscriber) {
        subscriber.selectStepNumber?.let {
            setTab(it)
        }
    }

    private fun setTab(number: Int) {
        when (number) {
            1 -> {
                resetTab()
                highLightTab(1)
                replaceFragment(1)
            }

            2 -> {
                resetTab()
                highLightTab(1)
                highLightTab(2)
                replaceFragment(2)
            }

            3 -> {
                resetTab()
                highLightTab(1)
                highLightTab(2)
                highLightTab(3)
                replaceFragment(3)
            }
        }
    }

    private fun replaceFragment(number: Int) {
        when (number) {
            1 -> childFragmentManager.beginTransaction()
                    .replace(R.id.sign_up_form_container, SignUp1Fragment.newInstance(), SignUp1Fragment::class.java.simpleName)
                    .commitNowAllowingStateLoss()

            2 -> childFragmentManager.beginTransaction()
                    .replace(R.id.sign_up_form_container, SignUp2Fragment.newInstance(), SignUp2Fragment::class.java.simpleName)
                    .commitNowAllowingStateLoss()

            3 -> childFragmentManager.beginTransaction()
                    .replace(R.id.sign_up_form_container, SignUp3Fragment.newInstance(), SignUp3Fragment::class.java.simpleName)
                    .commitNowAllowingStateLoss()
        }
    }

    private fun resetTab() {
        binding.step1TextView.isSelected = false
        binding.step2TextView.isSelected = false
        binding.step3TextView.isSelected = false
        binding.icChecked1.visibility = View.GONE
        binding.icChecked2.visibility = View.GONE
    }

    private fun highLightTab(number: Int) {
        when (number) {
            1 -> {
                binding.step1TextView.isSelected = true
            }

            2 -> {
                binding.icChecked1.visibility = View.VISIBLE
                binding.step1TextView.isSelected = false
                binding.step2TextView.isSelected = true
            }

            3 -> {
                binding.icChecked2.visibility = View.VISIBLE
                binding.step2TextView.isSelected = false
                binding.step3TextView.isSelected = true
            }
        }
    }

    private fun validateStep(number: Int): Boolean {
        return when (number) {
            1 -> {
                val fragment = childFragmentManager.findFragmentByTag(SignUp1Fragment::class.java.simpleName) as? SignUp1Fragment
                fragment?.validate() ?: false
            }

            2 -> {
                val fragment = childFragmentManager.findFragmentByTag(SignUp2Fragment::class.java.simpleName) as? SignUp2Fragment
                fragment?.validate() ?: false
            }

            3 -> {
                val fragment = childFragmentManager.findFragmentByTag(SignUp3Fragment::class.java.simpleName) as? SignUp3Fragment
                fragment?.validate() ?: false
            }

            else -> true
        }

        return false
    }
}