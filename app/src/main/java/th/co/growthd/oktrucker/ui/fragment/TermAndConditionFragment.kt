package th.co.growthd.oktrucker.ui.fragment

import android.databinding.DataBindingUtil
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import th.co.growthd.oktrucker.R
import th.co.growthd.oktrucker.databinding.TermAndConditionFragmentBinding
import th.co.growthd.oktrucker.ui.base.BaseFragment

class TermAndConditionFragment: BaseFragment() {

    private lateinit var binding: TermAndConditionFragmentBinding

    companion object {
        fun newInstance() = TermAndConditionFragment()
    }

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View {
        binding = DataBindingUtil.inflate(inflater, R.layout.term_and_condition_fragment, container, false)
        return binding.root
    }

    override fun onActivityCreated(savedInstanceState: Bundle?) {
        super.onActivityCreated(savedInstanceState)
        initView()
        initOnClick()
    }

    private fun initView() {
        binding.toolBar!!.titleTextView.text = getString(R.string.term_and_condition)
    }

    private fun initOnClick() {
        binding.toolBar!!.backImageView.setOnClickListener {
            activity?.onBackPressed()
        }
    }
}