package th.co.growthd.oktrucker.ui.fragment

import android.content.Intent
import android.databinding.DataBindingUtil
import android.os.Bundle
import android.support.v4.widget.TextViewCompat
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import org.parceler.Parcels
import th.co.growthd.deerandbook.util.*
import th.co.growthd.oktrucker.R
import th.co.growthd.oktrucker.databinding.TransactionRequestDetailFragmentBinding
import th.co.growthd.oktrucker.fromDateTime
import th.co.growthd.oktrucker.model.Deposit
import th.co.growthd.oktrucker.model.Withdraw
import th.co.growthd.oktrucker.setImageUrl
import th.co.growthd.oktrucker.toDateTimeAppFormat
import th.co.growthd.oktrucker.ui.activity.FullScreenImageActivity
import th.co.growthd.oktrucker.ui.base.BaseFragment
import th.co.growthd.oktrucker.util.enum.DepositType
import th.co.growthd.oktrucker.util.enum.TransactionRequestStatus
import th.co.growthd.oktrucker.util.enum.TransactionType
import th.co.growthd.oktrucker.util.manager.UserManager

class TransactionRequestDetailFragment : BaseFragment() {

    lateinit var binding: TransactionRequestDetailFragmentBinding
    private var deposit: Deposit? = null
    private var withdraw: Withdraw? = null
    private var transactionType: Int = 0

    companion object {
        fun newInstance(transactionType: Int, deposit: Deposit?, withdraw: Withdraw?): TransactionRequestDetailFragment {
            var fragment = TransactionRequestDetailFragment()
            val bundle = Bundle()
            bundle.putInt(TRANSACTION_TYPE_KEY, transactionType)
            bundle.putParcelable(DEPOSIT_KEY, Parcels.wrap(deposit))
            bundle.putParcelable(WITHDRAW_KEY, Parcels.wrap(withdraw))
            fragment.arguments = bundle
            return fragment
        }
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        arguments?.let {
            transactionType = it.getInt(TRANSACTION_TYPE_KEY)
            if (transactionType == TransactionType.DEPOSIT.spinnerValue) {
                deposit = Parcels.unwrap(it.getParcelable(DEPOSIT_KEY))
            } else {
                withdraw = Parcels.unwrap(it.getParcelable(WITHDRAW_KEY))
            }
        }

    }

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        binding = DataBindingUtil.inflate(inflater, R.layout.transaction_request_detail_fragment, container, false)
        return binding.root
    }

    override fun onActivityCreated(savedInstanceState: Bundle?) {
        super.onActivityCreated(savedInstanceState)
        initView()
        initOnClick()
    }

    private fun initView() {
        binding.toolBar!!.titleTextView.setText(R.string.title_request_detail)
        when (transactionType) {
            TransactionType.DEPOSIT.spinnerValue -> {
                binding.statusTextView.text = deposit!!.getReadableStatus()
                TextViewCompat.setTextAppearance(binding.statusTextView, deposit!!.getStatusStyle())
                binding.statusTextView.setBackgroundResource(deposit!!.getStatusBackground())
                binding.dateTextView.text = deposit!!.createdAt.fromDateTime().toDateTimeAppFormat()
                binding.nameTextView.text = deposit!!.readableId
                binding.transactionTypeTextView.setText(R.string.deposit)
                binding.creditTextView.text = deposit!!.getCredit()
                binding.paymentMethodIcon.setImageResource(
                        when (deposit!!.type) {
                            DepositType.TRANSFER.value -> {
                                R.drawable.ic_invoice
                            }
                            else -> R.drawable.ic_card
                        }
                )
                binding.paymentMethodTextView.text = deposit!!.getReadablePaymentMethod()
                binding.transferDateLayout.visibility = View.GONE

                if (deposit!!.remark.isNullOrEmpty()) {
                    binding.remarkLayout.visibility = View.GONE
                } else binding.remarkTextView.text = deposit!!.remark

                if (deposit!!.type == DepositType.OMISE.value) {
                    binding.slipImageView.visibility = View.GONE
                } else {
                    binding.slipImageView.setImageUrl(deposit!!.getSlipImagePath(), UserManager.getInstance(context!!).token)
                }
            }
            TransactionType.WITHDRAW.spinnerValue -> {
                binding.statusTextView.text = withdraw!!.getReadableStatus()
                TextViewCompat.setTextAppearance(binding.statusTextView, withdraw!!.getStatusStyle())
                binding.statusTextView.setBackgroundResource(withdraw!!.getStatusBackground())
                binding.dateTextView.text = withdraw!!.createdAt.fromDateTime().toDateTimeAppFormat()
                binding.nameTextView.text = withdraw!!.readableId
                binding.transactionTypeTextView.setText(R.string.withdraw)
                binding.creditTextView.text = withdraw!!.getCredit()
                binding.paymentMethodLayout.visibility = View.GONE

                if (withdraw!!.payTime.isNullOrEmpty()) {
                    binding.transferDateLayout.visibility = View.GONE
                } else binding.transferDateTextView.text = withdraw!!.getReadableTransferDate()

                if (withdraw!!.remark == INITIAL_STRING) {
                    binding.remarkLayout.visibility = View.GONE
                } else binding.remarkTextView.text = withdraw!!.remark


                if (withdraw!!.status == TransactionRequestStatus.APPROVED.status) {
                    binding.slipImageView.setImageUrl(withdraw!!.getSlipImagePath(), UserManager.getInstance(context!!).token)
                } else {
                    binding.slipImageView.visibility = View.GONE
                }
            }
        }
    }

    private fun initOnClick() {
        binding.toolBar!!.backImageView.setOnClickListener {
            activity!!.onBackPressed()
        }

        binding.slipImageView.setOnClickListener {
            if (transactionType == TransactionType.DEPOSIT.spinnerValue) {
                val intent = Intent(context, FullScreenImageActivity::class.java)
                intent.putExtra(IMAGE_PATH_KEY, deposit?.getSlipImagePath())
                intent.putExtra(IMAGE_NAME_KEY, getString(R.string.transfer_slip))
                startActivity(intent)
            } else {
                val intent = Intent(context, FullScreenImageActivity::class.java)
                intent.putExtra(IMAGE_PATH_KEY, withdraw?.getSlipImagePath())
                intent.putExtra(IMAGE_NAME_KEY, getString(R.string.transfer_slip))
                startActivity(intent)
            }
        }
    }
}