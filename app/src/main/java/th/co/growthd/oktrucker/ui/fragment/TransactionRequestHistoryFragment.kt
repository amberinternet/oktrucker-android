package th.co.growthd.oktrucker.ui.fragment

import android.arch.lifecycle.ViewModelProviders
import android.content.Intent
import android.databinding.DataBindingUtil
import android.os.Bundle
import android.support.v7.widget.LinearLayoutManager
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.AdapterView
import org.greenrobot.eventbus.EventBus
import org.greenrobot.eventbus.Subscribe
import org.parceler.Parcels
import th.co.growthd.deerandbook.ui.factory.TransactionRequestHistoryViewModelFactory
import th.co.growthd.deerandbook.util.DEPOSIT_KEY
import th.co.growthd.deerandbook.util.TRANSACTION_TYPE_KEY
import th.co.growthd.deerandbook.util.WITHDRAW_KEY
import th.co.growthd.oktrucker.R
import th.co.growthd.oktrucker.adapter.DepositAdapter
import th.co.growthd.oktrucker.adapter.TransactionRequestSpinnerAdapter
import th.co.growthd.oktrucker.adapter.WithdrawAdapter
import th.co.growthd.oktrucker.adapter.callback.OnRecyclerTouchListener
import th.co.growthd.oktrucker.addOnPropertyChanged
import th.co.growthd.oktrucker.databinding.TransactionRequestHistoryFragmentBinding
import th.co.growthd.oktrucker.getMonthPosition
import th.co.growthd.oktrucker.getYearPosition
import th.co.growthd.oktrucker.model.User
import th.co.growthd.oktrucker.model.subscriber.LoadMoreSubscriber
import th.co.growthd.oktrucker.ui.activity.TransactionRequestDetailActivity
import th.co.growthd.oktrucker.ui.base.BaseFragment
import th.co.growthd.oktrucker.ui.viewmodel.TransactionRequestHistoryViewModel
import th.co.growthd.oktrucker.util.enum.TransactionType
import th.co.growthd.oktrucker.util.manager.DialogManager
import java.util.*

class TransactionRequestHistoryFragment : BaseFragment() {

    private lateinit var binding: TransactionRequestHistoryFragmentBinding
    private lateinit var depositAdapter: DepositAdapter
    private lateinit var withdrawAdapter: WithdrawAdapter
    private lateinit var viewModel: TransactionRequestHistoryViewModel
    private lateinit var dialogManager: DialogManager
    private lateinit var user: User
    private var isSelectType: Boolean = false
    private var isSelectMonth: Boolean = false
    private var isSelectYear: Boolean = false

    companion object {
        fun newInstance() = TransactionRequestHistoryFragment()
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        initVariable()
    }

    private fun initVariable() {
        user = User.instance
        dialogManager = DialogManager.getInstance(childFragmentManager)
    }

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View {
        binding = DataBindingUtil.inflate(inflater, R.layout.transaction_request_history_fragment, container, false)
        return binding.root
    }

    override fun onActivityCreated(savedInstanceState: Bundle?) {
        super.onActivityCreated(savedInstanceState)
        viewModel = ViewModelProviders.of(this, TransactionRequestHistoryViewModelFactory(activity!!)).get(TransactionRequestHistoryViewModel::class.java)
        initViewModel()
        initEventBus()
        initView()
        initOnClick()
    }

    override fun onDestroyView() {
        super.onDestroyView()
        EventBus.getDefault().unregister(this)
    }

    private fun initEventBus() {
        EventBus.getDefault().register(this)
    }

    private fun initViewModel() {
        viewModel.depositPagination.addOnPropertyChanged {
            it.get()?.let {
                depositAdapter.depositPagination = it
                depositAdapter.notifyDataSetChanged()
            }
        }
        viewModel.withdrawPagination.addOnPropertyChanged {
            it.get()?.let {
                withdrawAdapter.withdrawPagination = it
                withdrawAdapter.notifyDataSetChanged()
            }
        }
        viewModel.isRefresh.addOnPropertyChanged {
            binding.refreshLayout.isRefreshing = it.get()
        }
    }

    private fun initView() {
        binding.toolBar!!.titleTextView.setText(R.string.title_request_history)
        depositAdapter = DepositAdapter(viewModel.depositList)
        withdrawAdapter = WithdrawAdapter(viewModel.withdrawList)
        binding.requestTypeSpinner.apply {
            adapter = TransactionRequestSpinnerAdapter().apply { stringArray = resources.getStringArray(R.array.request_type_array) }
            onItemSelectedListener = object : AdapterView.OnItemSelectedListener {
                override fun onNothingSelected(parent: AdapterView<*>?) {

                }

                override fun onItemSelected(parent: AdapterView<*>?, view: View?, position: Int, id: Long) {
                    if (isSelectType) {
                        binding.recyclerView.adapter = if (binding.requestTypeSpinner.selectedItemPosition == 0) depositAdapter else withdrawAdapter
//                        viewModel.isRefresh.set(true)
//                        getRequestHistory(1)
                    } else {
                        isSelectType = true
                    }
                }
            }
            setSelection(0)
        }

        binding.monthSpinner.apply {
            adapter = TransactionRequestSpinnerAdapter().apply { stringArray = resources.getStringArray(R.array.month_array) }
            onItemSelectedListener = object : AdapterView.OnItemSelectedListener {
                override fun onNothingSelected(parent: AdapterView<*>?) {

                }

                override fun onItemSelected(parent: AdapterView<*>?, view: View?, position: Int, id: Long) {
                    if (isSelectMonth) {
                        viewModel.isRefresh.set(true)
                        getRequestHistory(1)
                    } else {
                        isSelectMonth = true
                    }
                }
            }
            setSelection(Calendar.getInstance().getMonthPosition())
        }

        binding.yearSpinner.apply {
            adapter = TransactionRequestSpinnerAdapter().apply { stringArray = resources.getStringArray(R.array.year_array) }
            onItemSelectedListener = object : AdapterView.OnItemSelectedListener {
                override fun onNothingSelected(parent: AdapterView<*>?) {

                }

                override fun onItemSelected(parent: AdapterView<*>?, view: View?, position: Int, id: Long) {
                    if (isSelectYear) {
                        viewModel.isRefresh.set(true)
                        getRequestHistory(1)
                    } else {
                        isSelectYear = true
                    }
                }
            }
            setSelection(Calendar.getInstance().getYearPosition())
        }

        binding.recyclerView.apply {
            layoutManager = LinearLayoutManager(context, LinearLayoutManager.VERTICAL, false)
            adapter = if (binding.requestTypeSpinner.selectedItemPosition == 0) depositAdapter else withdrawAdapter
            addOnItemTouchListener(OnRecyclerTouchListener(context!!) { position ->
                if (binding.requestTypeSpinner.selectedItemPosition == 0) {
                    if (position < viewModel.depositList.size) {
                        val intent = Intent(activity, TransactionRequestDetailActivity::class.java)
                        intent.putExtra(TRANSACTION_TYPE_KEY, TransactionType.DEPOSIT.spinnerValue)
                        intent.putExtra(DEPOSIT_KEY, Parcels.wrap(viewModel.depositList[position]))
                        startActivity(intent)
                    }
                } else {
                    if (position < viewModel.withdrawList.size) {
                        val intent = Intent(activity, TransactionRequestDetailActivity::class.java)
                        intent.putExtra(TRANSACTION_TYPE_KEY, TransactionType.WITHDRAW.spinnerValue)
                        intent.putExtra(WITHDRAW_KEY, Parcels.wrap(viewModel.withdrawList[position]))
                        startActivity(intent)
                    }
                }
            })
        }

        binding.refreshLayout.setColorSchemeColors(context!!.resources.getColor(R.color.colorPrimary))
        binding.refreshLayout.setOnRefreshListener {
            viewModel.isRefresh.set(true)
            getRequestHistory(1)
        }
    }

    private fun initOnClick() {
        binding.toolBar!!.backImageView.setOnClickListener {
            activity?.finish()
        }
    }

    private fun getRequestHistory(page: Int) {
        if (binding.requestTypeSpinner.selectedItemPosition == 0) {
            subscription = viewModel.getDepositList(page, getMonthYearFilter())
        } else if (binding.requestTypeSpinner.selectedItemPosition == 1) {
            subscription = viewModel.getWithdrawList(page, getMonthYearFilter())
        }
    }

    private fun getMonthYearFilter(): String {
        val yearString = (binding.yearSpinner.selectedItemPosition + 2018).toString()
        val monthString = (binding.monthSpinner.selectedItemPosition + 1).let {
            if (it < 10) "0$it" else it
        }
        return "$yearString-$monthString"
    }

    @Subscribe
    fun onLoadMoreDeposit(loadMoreSubscriber: LoadMoreSubscriber) {
        if (loadMoreSubscriber.isLoadMoreDeposit) {
            subscription = viewModel.getDepositList(loadMoreSubscriber.page, getMonthYearFilter())
        }
    }

    @Subscribe
    fun onLoadMoreWithDraw(loadMoreSubscriber: LoadMoreSubscriber) {
        if (loadMoreSubscriber.isLoadMoreWithdraw) {
            subscription = viewModel.getWithdrawList(loadMoreSubscriber.page, getMonthYearFilter())
        }
    }
}