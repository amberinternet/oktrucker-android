package th.co.growthd.oktrucker.ui.fragment

import android.Manifest
import android.arch.lifecycle.ViewModelProviders
import android.content.pm.PackageManager
import android.databinding.DataBindingUtil
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import pl.aprilapps.easyphotopicker.EasyImage
import th.co.growthd.deerandbook.ui.factory.TransferMethodViewModelFactory
import th.co.growthd.oktrucker.databinding.TransferMethodFragmentBinding
import th.co.growthd.oktrucker.ui.base.BaseFragment
import th.co.growthd.oktrucker.ui.viewmodel.TransferMethodViewModel
import th.co.growthd.oktrucker.util.manager.DialogManager
import pl.aprilapps.easyphotopicker.DefaultCallback
import android.content.Intent
import android.graphics.Bitmap
import android.support.v4.app.ActivityCompat
import android.widget.Toast
import okhttp3.MediaType
import okhttp3.MultipartBody
import okhttp3.RequestBody
import th.co.growthd.deerandbook.util.PRICE_KEY
import th.co.growthd.deerandbook.util.REQUEST_CODE_WRITE_PERMISSION
import th.co.growthd.oktrucker.*
import th.co.growthd.oktrucker.util.enum.DepositType
import java.io.File
import java.util.*
import android.graphics.BitmapFactory
import android.graphics.Matrix
import android.media.ExifInterface
import android.widget.AdapterView
import th.co.growthd.oktrucker.adapter.TransactionRequestSpinnerAdapter
import java.io.FileOutputStream

class TransferMethodFragment : BaseFragment() {

    private val TYPE_SLIP_IMAGE = 1
    private lateinit var binding: TransferMethodFragmentBinding
    private lateinit var viewModel: TransferMethodViewModel
    private lateinit var dialogManager: DialogManager
    private lateinit var price: String
    private var slipFile: File? = null

    companion object {
        fun newInstance(price: String): TransferMethodFragment {
            val fragment = TransferMethodFragment()
            val bundle = Bundle().apply { putString(PRICE_KEY, price) }
            fragment.arguments = bundle
            return fragment
        }
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        initVariable()
    }

    private fun initVariable() {
        dialogManager = DialogManager.getInstance(childFragmentManager)
        arguments?.let {
            price = it.getString(PRICE_KEY)
        }
    }

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View {
        binding = DataBindingUtil.inflate(inflater, R.layout.transfer_method_fragment, container, false)
        return binding.root
    }

    override fun onActivityCreated(savedInstanceState: Bundle?) {
        super.onActivityCreated(savedInstanceState)
        viewModel = ViewModelProviders.of(this, TransferMethodViewModelFactory(activity!!)).get(TransferMethodViewModel::class.java)
        initView()
        initOnClick()
    }

    private fun checkPermission(): Boolean {
        return if (ActivityCompat.checkSelfPermission(context!!, Manifest.permission.WRITE_EXTERNAL_STORAGE) == PackageManager.PERMISSION_GRANTED) {
            true
        } else {
            requestPermission()
            false
        }
    }

    private fun requestPermission() {
        requestPermissions(arrayOf(Manifest.permission.WRITE_EXTERNAL_STORAGE), REQUEST_CODE_WRITE_PERMISSION)
    }

    override fun onRequestPermissionsResult(requestCode: Int, permissions: Array<String>, grantResults: IntArray) {
        when (requestCode) {
            REQUEST_CODE_WRITE_PERMISSION -> {
                if (grantResults[0] == PackageManager.PERMISSION_DENIED) {
                    Toast.makeText(activity!!, R.string.enable_write_permission, Toast.LENGTH_SHORT).show()
                }
            }
            else -> super.onRequestPermissionsResult(requestCode, permissions, grantResults)
        }
    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)
        EasyImage.handleActivityResult(requestCode, resultCode, data, activity, object : DefaultCallback() {
            override fun onImagePicked(imageFile: File?, source: EasyImage.ImageSource?, type: Int) {
                applyRotationIfNeeded(imageFile!!)
                slipFile = imageFile
                binding.slipImageView.setImageBitmap(BitmapFactory.decodeFile(slipFile!!.absolutePath))
                binding.fileNameTextView.text = slipFile?.name
            }

            override fun onImagePickerError(e: Exception?, source: EasyImage.ImageSource?, type: Int) {
                Toast.makeText(context, R.string.upload_image_error, Toast.LENGTH_LONG).show()
            }
        })
    }

    private fun initView() {
        binding.toolBar!!.titleTextView.setText(R.string.title_payment)

        binding.daySpinner.apply {
            adapter = TransactionRequestSpinnerAdapter().apply { stringArray = resources.getStringArray(R.array.day_array) }
            onItemSelectedListener = object : AdapterView.OnItemSelectedListener {
                override fun onNothingSelected(parent: AdapterView<*>?) {

                }

                override fun onItemSelected(parent: AdapterView<*>?, view: View?, position: Int, id: Long) {

                }
            }
            setSelection(Calendar.getInstance().getDayPosition())
        }

        binding.monthSpinner.apply {
            adapter = TransactionRequestSpinnerAdapter().apply { stringArray = resources.getStringArray(R.array.month_array) }
            onItemSelectedListener = object : AdapterView.OnItemSelectedListener {
                override fun onNothingSelected(parent: AdapterView<*>?) {

                }

                override fun onItemSelected(parent: AdapterView<*>?, view: View?, position: Int, id: Long) {

                }
            }
            setSelection(Calendar.getInstance().getMonthPosition())
        }

        binding.yearSpinner.apply {
            adapter = TransactionRequestSpinnerAdapter().apply { stringArray = resources.getStringArray(R.array.year_array) }
            onItemSelectedListener = object : AdapterView.OnItemSelectedListener {
                override fun onNothingSelected(parent: AdapterView<*>?) {

                }

                override fun onItemSelected(parent: AdapterView<*>?, view: View?, position: Int, id: Long) {

                }
            }
            setSelection(Calendar.getInstance().getYearPosition())
        }

        binding.hourSpinner.apply {
            adapter = TransactionRequestSpinnerAdapter().apply { stringArray = resources.getStringArray(R.array.hour_array) }
            onItemSelectedListener = object : AdapterView.OnItemSelectedListener {
                override fun onNothingSelected(parent: AdapterView<*>?) {

                }

                override fun onItemSelected(parent: AdapterView<*>?, view: View?, position: Int, id: Long) {

                }
            }
            setSelection(Calendar.getInstance().getHourPosition())
        }

        binding.minuteSpinner.apply {
            adapter = TransactionRequestSpinnerAdapter().apply { stringArray = resources.getStringArray(R.array.minute_array) }
            onItemSelectedListener = object : AdapterView.OnItemSelectedListener {
                override fun onNothingSelected(parent: AdapterView<*>?) {

                }

                override fun onItemSelected(parent: AdapterView<*>?, view: View?, position: Int, id: Long) {

                }
            }
            setSelection(Calendar.getInstance().getMinutePosition())
        }
    }

    private fun initOnClick() {
        binding.toolBar!!.backImageView.setOnClickListener {
            activity?.finish()
        }

        binding.selectFileLayout.setOnClickListener {
            if (checkPermission()) {
                EasyImage.openChooserWithGallery(this, getString(R.string.please_select_silp_image), TYPE_SLIP_IMAGE)
            }
        }

        binding.confirmTextView.setOnClickListener {
            if (validate()) {
                val price = price
                val year = binding.yearSpinner.selectedItem.toString().toServerYear().toInt()
                val month = binding.monthSpinner.selectedItemPosition + 1
                val day = binding.daySpinner.selectedItemPosition + 1
                val hour = binding.hourSpinner.selectedItemPosition
                val minute = binding.minuteSpinner.selectedItemPosition
                val payTime = Calendar.getInstance().setDateTime(year, month, day, hour, minute).toDateTimeServerFormat()
                val typeBody = RequestBody.create(MultipartBody.FORM, DepositType.TRANSFER.value.toString())
                val priceBody = RequestBody.create(MultipartBody.FORM, price)
                val payTimeBody = RequestBody.create(MultipartBody.FORM, payTime)
                val slipImage = RequestBody.create(MediaType.parse("multipart/form-data"), slipFile!!.resize(context!!, 512))
                subscription = viewModel.requestTransferMethod(typeBody, priceBody, payTimeBody, slipImage)
            }
        }
    }

    private fun validate(): Boolean {
        return when (slipFile) {
            null -> {
                dialogManager.showAlert(R.string.please_upload_transfer_slip)
                return false
            }
            else -> true
        }
    }

    private fun applyRotationIfNeeded(imageFile: File) {
        val exif = ExifInterface(imageFile.absolutePath)
        val exifRotation = exif.getAttributeInt(ExifInterface.TAG_ORIENTATION, ExifInterface.ORIENTATION_UNDEFINED)
        val rotation = when(exifRotation) {
            ExifInterface.ORIENTATION_ROTATE_90 -> 90
            ExifInterface.ORIENTATION_ROTATE_180 -> 180
            ExifInterface.ORIENTATION_ROTATE_270 -> 270
            else -> 0
        }
        if (rotation == 0) return
        val bitmap = BitmapFactory.decodeFile(imageFile.absolutePath)
        val matrix = Matrix().apply { postRotate(rotation.toFloat()) }
        val rotatedBitmap = Bitmap.createBitmap(bitmap, 0, 0, bitmap.width, bitmap.height, matrix, true)
        bitmap.recycle()

        lateinit var out: FileOutputStream
        try {
            out = FileOutputStream(imageFile)
            rotatedBitmap.compress(Bitmap.CompressFormat.JPEG, 100, out)
        } catch (e: Exception) {

        } finally {
            rotatedBitmap.recycle()
            out.close()
        }
    }
}