package th.co.growthd.oktrucker.ui.fragment

import android.arch.lifecycle.ViewModelProviders
import android.content.Intent
import android.databinding.DataBindingUtil
import android.os.Bundle
import android.support.v7.widget.LinearLayoutManager
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import org.greenrobot.eventbus.EventBus
import org.greenrobot.eventbus.Subscribe
import th.co.growthd.deerandbook.ui.factory.WalletViewModelFactory
import th.co.growthd.deerandbook.util.INITIAL_INT
import th.co.growthd.deerandbook.util.MONTH_KEY
import th.co.growthd.deerandbook.util.YEAR_KEY
import th.co.growthd.oktrucker.*
import th.co.growthd.oktrucker.adapter.TransactionAdapter
import th.co.growthd.oktrucker.databinding.WalletFragmentBinding
import th.co.growthd.oktrucker.model.Transaction
import th.co.growthd.oktrucker.model.User
import th.co.growthd.oktrucker.model.subscriber.FilterTransactionSubscriber
import th.co.growthd.oktrucker.model.subscriber.LoadMoreSubscriber
import th.co.growthd.oktrucker.ui.activity.*
import th.co.growthd.oktrucker.ui.base.BaseFragment
import th.co.growthd.oktrucker.ui.viewmodel.WalletViewModel
import th.co.growthd.oktrucker.util.manager.DialogManager
import java.util.*

class WalletFragment : BaseFragment() {

    private lateinit var binding: WalletFragmentBinding
    private lateinit var transactionAdapter: TransactionAdapter
    private lateinit var viewModel: WalletViewModel
    private lateinit var user: User
    private var monthQuery: Int = INITIAL_INT
    private var yearQuery: Int = INITIAL_INT

    companion object {
        fun newInstance() = WalletFragment()
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        initVariable()
    }

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View {
        binding = DataBindingUtil.inflate(inflater, R.layout.wallet_fragment, container, false)
        EventBus.getDefault().register(this)
        return binding.root
    }

    override fun onActivityCreated(savedInstanceState: Bundle?) {
        super.onActivityCreated(savedInstanceState)
        viewModel = ViewModelProviders.of(this, WalletViewModelFactory(activity!!)).get(WalletViewModel::class.java)
        initViewModel()
        initView()
        initOnClick()
    }

    private fun initViewModel() {
        subscription = viewModel.getProfile()
        viewModel.isLoadedProfile.addOnPropertyChanged {
            binding.creditTextView.text = user.getCredit()
            viewModel.isLoadedProfile.set(false)
        }
        viewModel.transactionPagination.addOnPropertyChanged {
            it.get()?.let {
                transactionAdapter.transactionPagination = it
                transactionAdapter.notifyDataSetChanged()
            }
        }
        viewModel.isRefresh.addOnPropertyChanged {
            binding.refreshLayout.isRefreshing = it.get()
        }
    }

    private fun initVariable() {
        user = User.instance
        monthQuery = Calendar.getInstance().getMonthPosition()
        yearQuery = Calendar.getInstance().getYearPosition()
    }

    override fun onDestroyView() {
        EventBus.getDefault().unregister(this)
        super.onDestroyView()
    }

    private fun initView() {
        binding.toolBar!!.titleTextView.setText(R.string.title_wallet)
        binding.creditTextView.text = user.getCredit()
        transactionAdapter = TransactionAdapter(viewModel.transactionList)
        binding.recyclerView.apply {
            layoutManager = LinearLayoutManager(context, LinearLayoutManager.VERTICAL, false)
            adapter = transactionAdapter
        }
        binding.refreshLayout.setColorSchemeColors(context!!.resources.getColor(R.color.colorPrimary))
        binding.refreshLayout.setOnRefreshListener {
            viewModel.isRefresh.set(true)
            subscription = viewModel.getTransactionList(1, getMonthYearFilter())
            subscription = viewModel.getProfile()
        }
    }

    private fun initOnClick() {
        binding.toolBar!!.calendarImageView.setOnClickListener {
            val intent = Intent(activity, FilterTransactionActivity::class.java)
            intent.putExtra(MONTH_KEY, monthQuery)
            intent.putExtra(YEAR_KEY, yearQuery)
            startActivity(intent)
        }
        binding.withdrawLayout.setOnClickListener {
            val intent = Intent(activity, WithdrawActivity::class.java)
            startActivity(intent)
        }
        binding.depositLayout.setOnClickListener {
            val intent = Intent(activity, DepositActivity::class.java)
            startActivity(intent)
        }
        binding.transactionRequestHistoryLayout.setOnClickListener {
            val intent = Intent(activity, TransactionRequestHistoryActivity::class.java)
            startActivity(intent)
        }
    }

    private fun getMonthYearFilter(): String {
        val yearString = (yearQuery + 2018).toString()
        val monthString = (monthQuery + 1).let {
            if (it < 10) "0$it" else it
        }
        return "$yearString-$monthString"
    }

    @Subscribe
    fun onLoadMoreTransaction(loadMoreSubscriber: LoadMoreSubscriber) {
        if (loadMoreSubscriber.isLoadMoreTransaction) {
            subscription = viewModel.getTransactionList(loadMoreSubscriber.page, getMonthYearFilter())
        }
    }

    @Subscribe
    fun onFilter(filterTransactionSubscriber: FilterTransactionSubscriber) {
        viewModel.isRefresh.set(true)
        monthQuery = filterTransactionSubscriber.month
        yearQuery = filterTransactionSubscriber.year
        subscription = viewModel.getTransactionList(1, getMonthYearFilter())
        viewModel.transactionList.clear()
    }
}