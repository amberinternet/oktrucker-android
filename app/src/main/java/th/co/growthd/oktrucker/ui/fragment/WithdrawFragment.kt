package th.co.growthd.oktrucker.ui.fragment

import android.arch.lifecycle.ViewModelProviders
import android.databinding.DataBindingUtil
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import th.co.growthd.deerandbook.ui.factory.WithdrawViewModelFactory
import th.co.growthd.oktrucker.R
import th.co.growthd.oktrucker.databinding.WithdrawFragmentBinding
import th.co.growthd.oktrucker.isEmpty
import th.co.growthd.oktrucker.model.User
import th.co.growthd.oktrucker.string
import th.co.growthd.oktrucker.ui.base.BaseFragment
import th.co.growthd.oktrucker.ui.viewmodel.WithdrawViewModel
import th.co.growthd.oktrucker.util.manager.DialogManager

class WithdrawFragment : BaseFragment() {

    private lateinit var binding: WithdrawFragmentBinding
    private lateinit var viewModel: WithdrawViewModel
    private lateinit var dialogManager: DialogManager
    private lateinit var user: User

    companion object {
        fun newInstance() = WithdrawFragment()
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        initVariable()
    }

    private fun initVariable() {
        user = User.instance
        dialogManager = DialogManager.getInstance(childFragmentManager)
    }

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View {
        binding = DataBindingUtil.inflate(inflater, R.layout.withdraw_fragment, container, false)
        return binding.root
    }

    override fun onActivityCreated(savedInstanceState: Bundle?) {
        super.onActivityCreated(savedInstanceState)
        viewModel = ViewModelProviders.of(this, WithdrawViewModelFactory(activity!!)).get(WithdrawViewModel::class.java)
        initView()
        initOnClick()
    }

    private fun initView() {
        binding.toolBar!!.titleTextView.setText(R.string.title_withdraw)
        binding.creditTextView.text = user.getCredit()
    }

    private fun initOnClick() {
        binding.toolBar!!.backImageView.setOnClickListener {
            activity?.finish()
        }

        binding.confirmTextView.setOnClickListener {
            if (validate()) {
                subscription = viewModel.requestWithdraw(binding.withdrawalAmountEditText.string().toInt())
            }
        }
    }

    private fun validate(): Boolean {
        return when {
            binding.withdrawalAmountEditText.isEmpty() || binding.withdrawalAmountEditText.string().toDouble()  < 100 -> {
                dialogManager.showAlert(R.string.alert_with_draw_equal_or_more_100)
                false
            }

            binding.withdrawalAmountEditText.string().toDouble() > user.creditBalance.toDouble() -> {
                dialogManager.showAlert(R.string.alert_with_draw_more_than_my_credit)
                false
            }
            else -> true
        }
    }
}