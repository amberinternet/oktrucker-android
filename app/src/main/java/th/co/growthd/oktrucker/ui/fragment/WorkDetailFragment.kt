package th.co.growthd.oktrucker.ui.fragment

import android.Manifest
import android.arch.lifecycle.ViewModelProviders
import android.databinding.DataBindingUtil
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import org.parceler.Parcels
import th.co.growthd.deerandbook.util.*
import th.co.growthd.oktrucker.databinding.WorkDetailFragmentBinding
import th.co.growthd.oktrucker.model.Work
import th.co.growthd.oktrucker.ui.base.BaseFragment
import th.co.growthd.oktrucker.ui.customview.ExtraServiceView
import th.co.growthd.oktrucker.util.enum.WorkStatus
import th.co.growthd.oktrucker.util.manager.DialogManager
import th.co.growthd.oktrucker.util.manager.UserManager
import android.content.Intent
import android.content.pm.PackageManager
import android.net.Uri
import android.support.v4.app.ActivityCompat
import android.support.v4.content.ContextCompat
import android.support.v4.widget.TextViewCompat
import android.widget.Toast
import okhttp3.MediaType
import okhttp3.RequestBody
import th.co.growthd.deerandbook.ui.factory.WorkDetailViewModelFactory
import th.co.growthd.oktrucker.*
import th.co.growthd.oktrucker.ui.activity.FullScreenImageActivity
import th.co.growthd.oktrucker.ui.viewmodel.WorkDetailViewModel
import th.co.growthd.oktrucker.util.enum.PaymentMethod
import java.util.*

class WorkDetailFragment : BaseFragment() {

    private lateinit var binding: WorkDetailFragmentBinding
    private lateinit var work: Work
    private lateinit var dialogManager: DialogManager
    private lateinit var viewModel: WorkDetailViewModel

    companion object {
        fun newInstance(work: Work): WorkDetailFragment {
            val fragment = WorkDetailFragment()
            val bundle = Bundle().apply { putParcelable(WORK_KEY, Parcels.wrap(work)) }
            fragment.arguments = bundle
            return fragment
        }
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        initVariable()
    }

    private fun initVariable() {
        dialogManager = DialogManager.getInstance(childFragmentManager)

        arguments?.let {
            work = Parcels.unwrap(it.getParcelable(WORK_KEY))
        }
    }

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View {
        binding = DataBindingUtil.inflate(inflater, R.layout.work_detail_fragment, container, false)
        return binding.root
    }

    override fun onActivityCreated(savedInstanceState: Bundle?) {
        super.onActivityCreated(savedInstanceState)
        viewModel = ViewModelProviders.of(this, WorkDetailViewModelFactory(activity!!)).get(WorkDetailViewModel::class.java)
        viewModel.work = work
        viewModel.status.addOnPropertyChanged {
            initView()
        }
        viewModel.isRefresh.addOnPropertyChanged {
            if (viewModel.work != work) {
                work = viewModel.work
                initView()
            }
            binding.refreshLayout.isRefreshing = it.get()
        }
        initRefreshLayout()
        initView()
        initOnClick()
    }

    private fun checkPermission(): Boolean {
        return if (ActivityCompat.checkSelfPermission(context!!, Manifest.permission.CALL_PHONE) == PackageManager.PERMISSION_GRANTED) {
            true
        } else {
            requestPermission()
            false
        }
    }

    private fun requestPermission() {
        requestPermissions(arrayOf(Manifest.permission.CALL_PHONE), REQUEST_CODE_CALL_PERMISSION)
    }

    override fun onRequestPermissionsResult(requestCode: Int, permissions: Array<String>, grantResults: IntArray) {
        when (requestCode) {
            REQUEST_CODE_CALL_PERMISSION -> {
                if (grantResults[0] == PackageManager.PERMISSION_DENIED) {
                    Toast.makeText(activity!!, R.string.enable_call_permission, Toast.LENGTH_SHORT).show()
                }
            }
            else -> super.onRequestPermissionsResult(requestCode, permissions, grantResults)
        }
    }

    private fun initRefreshLayout() {
        binding.refreshLayout.setColorSchemeColors(context!!.resources.getColor(R.color.colorPrimary))
        binding.refreshLayout.setOnRefreshListener {
            viewModel.isRefresh.set(true)
            viewModel.getWorkDetail(work.id)
        }
    }

    private fun initView() {
        binding.toolBar!!.titleTextView.setText(R.string.title_work_detail)
        binding.truckTypeTextView.text = viewModel.work.truckType.name
        binding.workIdTextView.text = viewModel.work.readableWorkId
        binding.paymentMethodTextView.text = viewModel.work.getReadableReceiveMethod()
        binding.statusPaymentTextView.text = viewModel.work.getReadableWorkStatus()
        binding.sourceDateTextView.text = viewModel.work.source.getReceiveDateTime()
        binding.sourceAddressTextView.text = when (viewModel.work.status) {
            WorkStatus.FIND_TRUCK.status, WorkStatus.WAITING_CONFIRM_WORK.status, WorkStatus.COMPLETE.status, WorkStatus.CANCEL.status -> {
                viewModel.work.source.getShortAddress()
            }
            else -> {
                viewModel.work.source.getFullAddress()
            }
        }
        binding.sourceContactTextView.text = viewModel.work.source.getReadableContactName()
        binding.sourceCompanyNameTextView.text = viewModel.work.source.companyName
        binding.sourceLandmarkTextView.text = viewModel.work.source.landmark
        binding.destinationDateTextView.text = viewModel.work.destination.getDeliveryDateTime()
        binding.destinationAddressTextView.text = when (viewModel.work.status) {
            WorkStatus.FIND_TRUCK.status, WorkStatus.WAITING_CONFIRM_WORK.status, WorkStatus.COMPLETE.status, WorkStatus.CANCEL.status -> {
                viewModel.work.destination.getShortAddress()
            }
            else -> {
                viewModel.work.destination.getFullAddress()
            }
        }
        binding.destinationContactTextView.text = viewModel.work.destination.getReadableContactName()
        binding.destinationCompanyNameTextView.text = viewModel.work.destination.companyName
        binding.destinationLandmarkTextView.text = viewModel.work.destination.landmark
        binding.itemImageView.setImageUrl(viewModel.work.item.getItemImagePath(), UserManager.getInstance(context!!).token)
        binding.itemNameTextView.text = viewModel.work.item.name
        binding.itemQuantityTextView.text = viewModel.work.item.quantity
        binding.itemWeightTextView.text = viewModel.work.item.weight
        binding.customerNameTextView.text = viewModel.work.userFullName
        when (work.paymentMethod) {
            PaymentMethod.CASH_DESTINATION.status, PaymentMethod.CASH_SOURCE.status -> {
                binding.statusPaymentLayout.setBackgroundColor(ContextCompat.getColor(context!!, R.color.colorPaymentSuccess))
                binding.icStatusPayment.setImageResource(R.drawable.ic_checkbox)
                binding.statusPaymentTextView.setText(R.string.received_cash)
            }
            else -> {
                binding.statusPaymentLayout.setBackgroundColor(ContextCompat.getColor(context!!, R.color.colorPaymentPending))
                binding.icStatusPayment.setImageResource(R.drawable.ic_clock)
                binding.statusPaymentTextView.setText(R.string.waiting_confirm_credit)
            }
        }
        binding.paymentMethodIcon.setImageResource(
                when (work.paymentMethod) {
                    PaymentMethod.CASH_DESTINATION.status, PaymentMethod.CASH_SOURCE.status -> {
                        R.drawable.ic_dollar_note
                    }
                    else -> R.drawable.ic_wallet
                }
        )
        binding.priceTextView.text = viewModel.work.getPrice()
        binding.feeTextView.text = work.getFee()
        binding.finalWageTextView.text = work.getFinalWage()
        initStatusView()
        initExtraServiceAndRemarkView()
    }

    private fun initExtraServiceAndRemarkView() {
        // extra service
        if (viewModel.work.additionalServiceList.isNotEmpty()) {
            binding.extraServiceLayout.visibility = View.VISIBLE
            binding.extraServiceListLayout.removeAllViews()
            for (service in viewModel.work.additionalServiceList) {
                val view = ExtraServiceView(context!!).apply { additionalService = service }
                view.initView()
                binding.extraServiceListLayout.addView(view)
            }
        } else {
            binding.extraServiceLayout.visibility = View.GONE
        }

        // remark view
        if (viewModel.work.remark == INITIAL_STRING) {
            binding.remarkLayout.visibility = View.GONE
        } else {
            binding.remarkLayout.visibility = View.VISIBLE
            binding.remarkTextView.text = viewModel.work.remark
        }
    }

    private fun initStatusView() {
        binding.statusTextView.text = viewModel.work.getReadableWorkStatus()
        binding.actionTextView.text = work.getReadableWorkAction()
        binding.actionTextView.setBackgroundResource(R.drawable.button_work_action)
        when (viewModel.work.status) {
            WorkStatus.FIND_TRUCK.status -> {
                TextViewCompat.setTextAppearance(binding.statusTextView, R.style.TextView_Status_FindTruck)
                binding.statusTextView.setBackgroundResource(R.drawable.background_work_status_find_truck)
                binding.itemImageView.visibility = View.VISIBLE
                binding.actionLayout.visibility = View.VISIBLE
                binding.statusPaymentLayout.visibility = View.GONE
                setCustomerLayout(View.GONE)
            }

            WorkStatus.WAITING_CONFIRM_WORK.status -> {
                TextViewCompat.setTextAppearance(binding.statusTextView, R.style.TextView_Status_WaitingConfirm)
                binding.statusTextView.setBackgroundResource(R.drawable.background_work_status_waiting_confirm)
                binding.actionTextView.setBackgroundResource(R.drawable.button_work_cancel)
                binding.itemImageView.visibility = View.VISIBLE
                binding.actionLayout.visibility = View.VISIBLE
                binding.statusPaymentLayout.visibility = View.GONE
                setCustomerLayout(View.GONE)
            }

            WorkStatus.WAITING_RECEIVE_ITEM.status -> {
                TextViewCompat.setTextAppearance(binding.statusTextView, R.style.TextView_Status_WaitingReceiveItem)
                binding.statusTextView.setBackgroundResource(R.drawable.background_work_status_waiting_receive_item)
                binding.itemImageView.visibility = View.VISIBLE
                binding.actionLayout.visibility = View.VISIBLE
                binding.statusPaymentLayout.visibility = View.GONE
                setCustomerLayout(View.VISIBLE)
            }

            WorkStatus.ARRIVED_SOURCE.status -> {
                TextViewCompat.setTextAppearance(binding.statusTextView, R.style.TextView_Status_ArrivedSource)
                binding.statusTextView.setBackgroundResource(R.drawable.background_work_status_arrived_source_item)
                binding.itemImageView.visibility = View.VISIBLE
                binding.actionLayout.visibility = View.VISIBLE
                binding.statusPaymentLayout.visibility = View.GONE
                setCustomerLayout(View.VISIBLE)
            }

            WorkStatus.WAITING_SENT_ITEM.status -> {
                TextViewCompat.setTextAppearance(binding.statusTextView, R.style.TextView_Status_WaitingSendItem)
                binding.statusTextView.setBackgroundResource(R.drawable.background_work_status_waiting_send_item)
                binding.itemImageView.visibility = View.VISIBLE
                binding.actionLayout.visibility = View.VISIBLE
                binding.statusPaymentLayout.visibility = View.GONE
                setCustomerLayout(View.VISIBLE)
            }

            WorkStatus.ARRIVED_DESTINATION.status -> {
                TextViewCompat.setTextAppearance(binding.statusTextView, R.style.TextView_Status_ArrivedDestination)
                binding.statusTextView.setBackgroundResource(R.drawable.background_work_status_arrived_destination_item)
                binding.itemImageView.visibility = View.VISIBLE
                binding.actionLayout.visibility = View.VISIBLE
                binding.statusPaymentLayout.visibility = View.GONE
                setCustomerLayout(View.VISIBLE)
            }

            WorkStatus.COMPLETE.status -> {
                binding.itemImageView.visibility = View.GONE
                binding.actionLayout.visibility = View.GONE
                binding.statusPaymentLayout.visibility = View.VISIBLE
                setCustomerLayout(View.GONE)
                if (work.driverPaid == 1) {
                    TextViewCompat.setTextAppearance(binding.statusTextView, R.style.TextView_Status_Finish)
                    binding.statusTextView.setBackgroundResource(R.drawable.background_work_status_finish)
                    binding.statusPaymentLayout.setBackgroundColor(ContextCompat.getColor(context!!, R.color.colorPaymentSuccess))
                    binding.icStatusPayment.setImageResource(R.drawable.ic_checkbox)
                    binding.statusPaymentTextView.setText(R.string.received_cash)
                    binding.statusPaymentTextView.setTextColor(ContextCompat.getColor(context!!, R.color.colorWhite))
                } else {
                    when (work.paymentMethod) {
                        PaymentMethod.CASH_DESTINATION.status, PaymentMethod.CASH_SOURCE.status -> {
                            TextViewCompat.setTextAppearance(binding.statusTextView, R.style.TextView_Status_Finish)
                            binding.statusTextView.setBackgroundResource(R.drawable.background_work_status_finish)
                            binding.statusPaymentLayout.setBackgroundColor(ContextCompat.getColor(context!!, R.color.colorPaymentSuccess))
                            binding.icStatusPayment.setImageResource(R.drawable.ic_checkbox)
                            binding.statusPaymentTextView.setText(R.string.received_cash)
                            binding.statusPaymentTextView.setTextColor(ContextCompat.getColor(context!!, R.color.colorWhite))
                        }
                        else -> {
                            TextViewCompat.setTextAppearance(binding.statusTextView, R.style.TextView_Status_FinishPaymentPending)
                            binding.statusTextView.setBackgroundResource(R.drawable.background_work_status_finish_payment_pending)
                            binding.statusPaymentLayout.setBackgroundColor(ContextCompat.getColor(context!!, R.color.colorPaymentPending))
                            binding.icStatusPayment.setImageResource(R.drawable.ic_clock)
                            binding.statusPaymentTextView.setText(R.string.waiting_confirm_credit)
                            binding.statusPaymentTextView.setTextColor(ContextCompat.getColor(context!!, R.color.colorPrimaryText))
                        }
                    }
                }
            }

            WorkStatus.CANCEL.status -> {
                TextViewCompat.setTextAppearance(binding.statusTextView, R.style.TextView_Status_Cancel)
                binding.statusTextView.setBackgroundResource(R.drawable.background_work_status_cancel)
                binding.itemImageView.visibility = View.GONE
                binding.actionLayout.visibility = View.GONE
                binding.statusPaymentLayout.visibility = View.GONE
                setCustomerLayout(View.GONE)
            }
        }
    }

    private fun setCustomerLayout(statusView: Int) {
        binding.icSourceWarehouse.visibility = statusView
        binding.customerLayout.visibility = statusView
        binding.sourceCompanyNameTextView.visibility = statusView
        binding.sourceLandmarkTextView.visibility = statusView
        binding.sourceContactTextView.visibility = statusView
        binding.sourceTelephoneImageView.visibility = statusView
        binding.icDestinationWarehouse.visibility = statusView
        binding.destinationCompanyNameTextView.visibility = statusView
        binding.destinationLandmarkTextView.visibility = statusView
        binding.destinationContactTextView.visibility = statusView
        binding.destinationTelephoneImageView.visibility = statusView

        if (statusView == View.GONE) {
            binding.sourceAddressTextView.isEnabled = false
            binding.destinationAddressTextView.isEnabled = false
        }

        initCompanyAndLandmarkView()
    }

    private fun initCompanyAndLandmarkView() {
        if (work.source.companyName.isEmpty())
            binding.sourceCompanyNameTextView.visibility = View.GONE
        if (work.source.landmark.isEmpty())
            binding.sourceLandmarkTextView.visibility = View.GONE
        if (work.destination.companyName.isEmpty())
            binding.destinationCompanyNameTextView.visibility = View.GONE
        if (work.destination.landmark.isEmpty())
            binding.destinationLandmarkTextView.visibility = View.GONE
    }

    private fun initOnClick() {
        binding.toolBar!!.backImageView.setOnClickListener {
            activity!!.onBackPressed()
        }
        binding.toolBar!!.contactImageView.setOnClickListener {
            dialogManager.showContactAdmin(R.string.contact_admin, R.string.need_help_from_admin)
        }
        binding.itemImageView.setOnClickListener {
            val intent = Intent(activity, FullScreenImageActivity::class.java)
            intent.putExtra(IMAGE_PATH_KEY, work.item.getItemImagePath())
            intent.putExtra(IMAGE_NAME_KEY, work.item.name)
            startActivity(intent)
        }
        binding.sourceAddressTextView.setOnClickListener {
            val url = "https://www.google.com/maps/search/?api=1&query=${viewModel.work.source.latitude}, ${viewModel.work.source.longitude}"
            val intent = Intent(Intent.ACTION_VIEW, Uri.parse(url))
            startActivity(intent)
        }
        binding.destinationAddressTextView.setOnClickListener {
            val url = "https://www.google.com/maps/search/?api=1&query=${viewModel.work.destination.latitude}, ${viewModel.work.destination.longitude}"
            val intent = Intent(Intent.ACTION_VIEW, Uri.parse(url))
            startActivity(intent)
        }
        binding.sourceTelephoneImageView.setOnClickListener {
            if (checkPermission()) {
                val intent = Intent(Intent.ACTION_CALL, Uri.parse("tel: ${viewModel.work.source.contactTelephoneNumber}"))
                startActivity(intent)
            }
        }
        binding.sourceContactTextView.setOnClickListener {
            if (checkPermission()) {
                val intent = Intent(Intent.ACTION_CALL, Uri.parse("tel: ${viewModel.work.source.contactTelephoneNumber}"))
                startActivity(intent)
            }
        }
        binding.destinationTelephoneImageView.setOnClickListener {
            if (checkPermission()) {
                val intent = Intent(Intent.ACTION_CALL, Uri.parse("tel: ${viewModel.work.destination.contactTelephoneNumber}"))
                startActivity(intent)
            }
        }
        binding.destinationContactTextView.setOnClickListener {
            if (checkPermission()) {
                val intent = Intent(Intent.ACTION_CALL, Uri.parse("tel: ${viewModel.work.destination.contactTelephoneNumber}"))
                startActivity(intent)
            }
        }
        binding.customerNameTextView.setOnClickListener {
            if (checkPermission()) {
                val intent = Intent(Intent.ACTION_CALL, Uri.parse("tel: ${viewModel.work.userTelephoneNumber}"))
                startActivity(intent)
            }
        }
        binding.customerTelephoneImageView.setOnClickListener {
            if (checkPermission()) {
                val intent = Intent(Intent.ACTION_CALL, Uri.parse("tel: ${viewModel.work.userTelephoneNumber}"))
                startActivity(intent)
            }
        }
        binding.actionTextView.setOnClickListener {
            when (work.status) {
                WorkStatus.FIND_TRUCK.status -> {
                    dialogManager.showConfirmAcceptWork(viewModel.work) {
                        subscription = viewModel.acceptWork()
                    }
                }

                WorkStatus.WAITING_CONFIRM_WORK.status -> {
                    dialogManager.showContactAdmin()
                }

                WorkStatus.WAITING_RECEIVE_ITEM.status -> {
                    // check current date is equal receive date
                    val currentDate = Calendar.getInstance().apply {
                        set(Calendar.HOUR_OF_DAY, 0)
                        set(Calendar.MINUTE, 0)
                        set(Calendar.SECOND, 0)
                    }.time
                    val workDate = work.source.date.fromDate()

                    if (currentDate.after(workDate)) {
                        dialogManager.showConfirm(R.string.confirm_you_are_arrive_source, R.string.are_you_arrive_source) {
                            subscription = viewModel.arrivedSource()
                        }
                    } else {
                        dialogManager.showAlert(R.string.alert, R.string.alert_arrive_source_not_current_date, R.string.ok) {}
                    }
                }

                WorkStatus.ARRIVED_SOURCE.status -> {
                    if (work.paymentMethod == PaymentMethod.CASH_SOURCE.status) {
                        dialogManager.showAlertReceiveCash(R.string.alert_receive_cash_from_source, work.getPrice()) {
                            dialogManager.showConfirmSignature(R.string.source_sign) { bitmap ->
                                val signatureFile = RequestBody.create(MediaType.parse("multipart/form-data"), bitmap.toImageFile(context!!))
                                subscription = viewModel.departSource(signatureFile)
                            }
                        }
                    } else {
                        dialogManager.showConfirmSignature(R.string.source_sign) { bitmap ->
                            val signatureFile = RequestBody.create(MediaType.parse("multipart/form-data"), bitmap.toImageFile(context!!))
                            subscription = viewModel.departSource(signatureFile)
                        }
                    }
                }

                WorkStatus.WAITING_SENT_ITEM.status -> {
                    // check current date is equal sent date
                    val currentDate = Calendar.getInstance().apply {
                        set(Calendar.HOUR_OF_DAY, 0)
                        set(Calendar.MINUTE, 0)
                        set(Calendar.SECOND, 0)
                    }.time
                    val workDate = work.destination.date.fromDate()

                    if (currentDate.after(workDate)) {
                        dialogManager.showConfirm(R.string.confirm_you_are_arrive_destination, R.string.are_you_arrive_destination) {
                            subscription = viewModel.arrivedDestination()
                        }
                    } else {
                        dialogManager.showAlert(R.string.alert, R.string.alert_arrive_destination_not_current_date, R.string.ok) {}
                    }
                }

                WorkStatus.ARRIVED_DESTINATION.status -> {
                    if (work.paymentMethod == PaymentMethod.CASH_DESTINATION.status) {
                        dialogManager.showAlertReceiveCash(R.string.alert_receive_cash_from_destination, work.getPrice()) {
                            dialogManager.showConfirmSignature(R.string.destination_sign) { bitmap ->
                                val signatureFile = RequestBody.create(MediaType.parse("multipart/form-data"), bitmap.toImageFile(context!!))
                                subscription = viewModel.completeWork(signatureFile)
                            }
                        }
                    } else {
                        dialogManager.showConfirmSignature(R.string.destination_sign) { bitmap ->
                            val signatureFile = RequestBody.create(MediaType.parse("multipart/form-data"), bitmap.toImageFile(context!!))
                            subscription = viewModel.completeWork(signatureFile)
                        }
                    }
                }
            }
        }
    }
}