package th.co.growthd.okTruck.ui.viewholder

import android.support.v7.widget.RecyclerView
import th.co.growthd.oktrucker.databinding.ItemCreditBinding

class CreditViewHolder(val binding: ItemCreditBinding) : RecyclerView.ViewHolder(binding.root) {

    lateinit var credit: String

    fun initView() {
        binding.creditTextView.text = credit
    }
}