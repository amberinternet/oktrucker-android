package th.co.growthd.okTruck.ui.viewholder

import android.support.v4.content.ContextCompat
import android.support.v4.widget.TextViewCompat
import android.support.v7.widget.RecyclerView
import th.co.growthd.oktrucker.R
import th.co.growthd.oktrucker.databinding.ItemDepositBinding
import th.co.growthd.oktrucker.fromDateTime
import th.co.growthd.oktrucker.model.Deposit
import th.co.growthd.oktrucker.toDateTimeAppFormat

class DepositViewHolder(val binding: ItemDepositBinding) : RecyclerView.ViewHolder(binding.root) {

    lateinit var deposit: Deposit

    fun initView() {
        binding.nameTextView.text = deposit.readableId
        binding.creditTextView.text = deposit.getCredit()
        binding.dateTextView.text = deposit.createdAt.fromDateTime().toDateTimeAppFormat()
        binding.statusTextView.text = deposit.getReadableStatus()
        TextViewCompat.setTextAppearance(binding.statusTextView, deposit.getStatusStyle())
        binding.statusTextView.setBackgroundResource(deposit.getStatusBackground())
    }
}