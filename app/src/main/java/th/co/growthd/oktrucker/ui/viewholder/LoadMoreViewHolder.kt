package th.co.growthd.oktrucker.ui.viewholder

import android.support.v7.widget.RecyclerView
import android.view.View
import th.co.growthd.oktrucker.databinding.ItemLoadMoreBinding

class LoadMoreViewHolder(val binding: ItemLoadMoreBinding) : RecyclerView.ViewHolder(binding.root) {

    fun hideProgressBar() {
        binding.progressBar.visibility = View.GONE
    }

    fun showProgressBar() {
        binding.progressBar.visibility = View.VISIBLE
    }
}