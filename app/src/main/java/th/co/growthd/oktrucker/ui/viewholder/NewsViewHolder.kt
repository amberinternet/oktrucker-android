package th.co.growthd.okTruck.ui.viewholder

import android.support.v7.widget.RecyclerView
import android.view.View
import th.co.growthd.oktrucker.databinding.ItemNewsBinding
import th.co.growthd.oktrucker.fromDateTime
import th.co.growthd.oktrucker.model.News
import th.co.growthd.oktrucker.toDateTimeAppFormat

class NewsViewHolder(val binding: ItemNewsBinding) : RecyclerView.ViewHolder(binding.root) {

    lateinit var news: News

    fun initView() {
        binding.titleTextView.text = news.title
        binding.shortContentTextView.text = news.shortContent
        binding.dateTextView.text = news.postAt.fromDateTime().toDateTimeAppFormat()
        binding.pinImageView.visibility = if (news.isPin == 1) View.VISIBLE else View.GONE
    }
}