package th.co.growthd.deerandbook.ui.viewholder

import android.support.v7.widget.RecyclerView
import th.co.growthd.oktrucker.databinding.ItemNoDataBinding

class NoDataViewHolder(val binding: ItemNoDataBinding) : RecyclerView.ViewHolder(binding.root) {

    fun initView() {

    }
}