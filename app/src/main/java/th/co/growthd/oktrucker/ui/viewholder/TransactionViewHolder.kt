package th.co.growthd.okTruck.ui.viewholder

import android.support.v4.content.ContextCompat
import android.support.v7.widget.RecyclerView
import th.co.growthd.oktrucker.databinding.ItemTransactionBinding
import th.co.growthd.oktrucker.fromDateTime
import th.co.growthd.oktrucker.model.Transaction
import th.co.growthd.oktrucker.toDateTimeAppFormat
import th.co.growthd.oktrucker.toSpanned

class TransactionViewHolder(val binding: ItemTransactionBinding) : RecyclerView.ViewHolder(binding.root) {

    lateinit var transaction: Transaction

    fun initView() {
        binding.nameTextView.text = transaction.getReadableType().toSpanned()
        binding.creditTextView.apply {
            text = transaction.getReadableCredit()
            setTextColor(ContextCompat.getColor(context, transaction.getCreditTextColor()))
        }
        binding.dateTextView.text = transaction.createdAt.fromDateTime().toDateTimeAppFormat()
    }
}