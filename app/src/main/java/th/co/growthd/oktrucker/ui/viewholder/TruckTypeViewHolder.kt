package th.co.growthd.okTruck.ui.viewholder

import android.support.v7.widget.RecyclerView
import th.co.growthd.oktrucker.databinding.ItemTruckTypeBinding
import th.co.growthd.oktrucker.model.TruckType
import th.co.growthd.oktrucker.setImageUrl

class TruckTypeViewHolder(val binding: ItemTruckTypeBinding) : RecyclerView.ViewHolder(binding.root) {

    lateinit var truckType: TruckType

    fun initView() {
        binding.truckTypeNameTextView.text = truckType.name
        binding.truckTypeDescriptionTextView.text = truckType.description
        binding.truckTypeImageView.setImageResource(truckType.truckImageResId)
    }

    fun highlight() {
        binding.truck1Layout.isSelected = true
    }
}