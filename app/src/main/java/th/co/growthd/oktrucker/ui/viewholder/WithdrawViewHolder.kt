package th.co.growthd.okTruck.ui.viewholder

import android.support.v4.widget.TextViewCompat
import android.support.v7.widget.RecyclerView
import th.co.growthd.oktrucker.databinding.ItemWithdrawBinding
import th.co.growthd.oktrucker.fromDateTime
import th.co.growthd.oktrucker.model.Withdraw
import th.co.growthd.oktrucker.toDateTimeAppFormat

class WithdrawViewHolder(val binding: ItemWithdrawBinding) : RecyclerView.ViewHolder(binding.root) {

    lateinit var withdraw: Withdraw

    fun initView() {
        binding.nameTextView.text = withdraw.readableId
        binding.creditTextView.text = withdraw.getCredit()
        binding.dateTextView.text = withdraw.createdAt.fromDateTime().toDateTimeAppFormat()
        binding.statusTextView.text = withdraw.getReadableStatus()
        TextViewCompat.setTextAppearance(binding.statusTextView, withdraw.getStatusStyle())
        binding.statusTextView.setBackgroundResource(withdraw.getStatusBackground())
    }
}