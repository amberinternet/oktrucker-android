package th.co.growthd.okTruck.ui.viewholder

import android.support.v4.widget.TextViewCompat
import android.support.v7.widget.RecyclerView
import th.co.growthd.oktrucker.R
import th.co.growthd.oktrucker.databinding.ItemWorkBinding
import th.co.growthd.oktrucker.model.Work
import th.co.growthd.oktrucker.util.enum.PaymentMethod
import th.co.growthd.oktrucker.util.enum.WorkStatus

class WorkViewHolder(val binding: ItemWorkBinding) : RecyclerView.ViewHolder(binding.root) {

    lateinit var work: Work

    fun initView() {
        binding.truckTypeTextView.text = work.truckType.name
        binding.workIdTextView.text = work.readableWorkId
        binding.sourceShortAddressTextView.text = work.source.getShortAddress()
        binding.sourceDateTextView.text = work.source.getReceiveDateTime()
        binding.destinationShortAddressTextView.text = work.destination.getShortAddress()
        binding.destinationDateTextView.text = work.destination.getDeliveryDateTime()
        binding.itemNameTextView.text = work.item.name
        binding.priceTextView.text = work.getPrice()
        binding.paymentMethodIcon.setImageResource(
                when (work.paymentMethod) {
                    PaymentMethod.CASH_DESTINATION.status, PaymentMethod.CASH_SOURCE.status -> {
                        R.drawable.ic_dollar_note
                    }
                    else -> R.drawable.ic_wallet
                }
        )
        initStatusTextView()
    }

    private fun initStatusTextView() {
        binding.statusTextView.text = work.getReadableWorkStatus()
        when (work.status) {
            WorkStatus.FIND_TRUCK.status -> {
                TextViewCompat.setTextAppearance(binding.statusTextView, R.style.TextView_Status_FindTruck)
                binding.statusTextView.setBackgroundResource(R.drawable.background_work_status_find_truck)
            }

            WorkStatus.WAITING_CONFIRM_WORK.status -> {
                TextViewCompat.setTextAppearance(binding.statusTextView, R.style.TextView_Status_WaitingConfirm)
                binding.statusTextView.setBackgroundResource(R.drawable.background_work_status_waiting_confirm)
            }

            WorkStatus.WAITING_RECEIVE_ITEM.status -> {
                TextViewCompat.setTextAppearance(binding.statusTextView, R.style.TextView_Status_WaitingReceiveItem)
                binding.statusTextView.setBackgroundResource(R.drawable.background_work_status_waiting_receive_item)
            }

            WorkStatus.ARRIVED_SOURCE.status -> {
                TextViewCompat.setTextAppearance(binding.statusTextView, R.style.TextView_Status_ArrivedSource)
                binding.statusTextView.setBackgroundResource(R.drawable.background_work_status_arrived_source_item)
            }

            WorkStatus.WAITING_SENT_ITEM.status -> {
                TextViewCompat.setTextAppearance(binding.statusTextView, R.style.TextView_Status_WaitingSendItem)
                binding.statusTextView.setBackgroundResource(R.drawable.background_work_status_waiting_send_item)
            }

            WorkStatus.ARRIVED_DESTINATION.status -> {
                TextViewCompat.setTextAppearance(binding.statusTextView, R.style.TextView_Status_ArrivedDestination)
                binding.statusTextView.setBackgroundResource(R.drawable.background_work_status_arrived_destination_item)
            }

            WorkStatus.COMPLETE.status -> {
                if (work.driverPaid == 1) {
                    TextViewCompat.setTextAppearance(binding.statusTextView, R.style.TextView_Status_Finish)
                    binding.statusTextView.setBackgroundResource(R.drawable.background_work_status_finish)
                } else {
                    when (work.paymentMethod) {
                        PaymentMethod.CASH_DESTINATION.status, PaymentMethod.CASH_SOURCE.status -> {
                            TextViewCompat.setTextAppearance(binding.statusTextView, R.style.TextView_Status_Finish)
                            binding.statusTextView.setBackgroundResource(R.drawable.background_work_status_finish)
                        }
                        else -> {
                            TextViewCompat.setTextAppearance(binding.statusTextView, R.style.TextView_Status_FinishPaymentPending)
                            binding.statusTextView.setBackgroundResource(R.drawable.background_work_status_finish_payment_pending)
                        }
                    }
                }
            }

            WorkStatus.CANCEL.status -> {
                TextViewCompat.setTextAppearance(binding.statusTextView, R.style.TextView_Status_Cancel)
                binding.statusTextView.setBackgroundResource(R.drawable.background_work_status_cancel)
            }
        }
    }
}