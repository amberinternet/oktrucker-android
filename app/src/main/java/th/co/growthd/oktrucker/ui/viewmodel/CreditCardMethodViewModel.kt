package th.co.growthd.oktrucker.ui.viewmodel

import android.arch.lifecycle.ViewModel
import android.content.Intent
import android.databinding.ObservableField
import android.support.v4.app.FragmentActivity
import rx.Subscription
import th.co.growthd.deerandbook.util.NAVIGATION_TAB_KEY
import th.co.growthd.oktrucker.R
import th.co.growthd.oktrucker.model.DepositOmise
import th.co.growthd.oktrucker.ui.activity.TabBarActivity
import th.co.growthd.oktrucker.util.enum.DepositType
import th.co.growthd.oktrucker.util.manager.DialogManager
import th.co.growthd.oktrucker.util.network.RxNetwork
import th.co.growthd.oktrucker.util.network.response.DepositOmiseResponse
import th.co.growthd.oktrucker.util.network.response.SuccessResponse
import th.co.growthd.oktrucker.util.network.service.UserService

class CreditCardMethodViewModel(val fragmentActivity: FragmentActivity) : ViewModel() {

    private var dialogManager: DialogManager
    private var userService: UserService
    var depositOmise = ObservableField<DepositOmise>()

    init {
        dialogManager = DialogManager.getInstance(fragmentActivity.supportFragmentManager)
        userService = UserService.getInstance(fragmentActivity)
    }

    fun requestDepositWithOmise(price: Int, token: String): Subscription? {
        return RxNetwork<DepositOmiseResponse>(fragmentActivity).request(userService.depositWithOmise(DepositType.OMISE.value, price, token, 1),
                onSuccess = { response ->
                    depositOmise.set(response.depositOmise)
                },
                onFailure = { error ->
                    dialogManager.showError(error)
                },
                onLoading = {
                    dialogManager.showLoading(R.string.sending_request)
                },
                onLoaded = {
                    dialogManager.hideLoading()
                }
        )
    }

    fun completePaymentOmise(): Subscription? {
        return RxNetwork<SuccessResponse>(fragmentActivity).request(userService.completePaymentOmise(depositOmise.get()!!.deposit.id),
                onSuccess = {
                    dialogManager.showSuccess(R.string.payment_complete) {
                        val intent = Intent(fragmentActivity, TabBarActivity::class.java)
                        intent.flags = Intent.FLAG_ACTIVITY_CLEAR_TOP
                        intent.putExtra(NAVIGATION_TAB_KEY, R.id.tab_bar_wallet)
                        fragmentActivity.startActivity(intent)
                    }
                },
                onFailure = { error ->
                    dialogManager.showError(error)
                },
                onLoading = {
                    dialogManager.showLoading(R.string.checking_payment)
                },
                onLoaded = {
                    dialogManager.hideLoading()
                })
    }
}