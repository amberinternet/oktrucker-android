package th.co.growthd.oktrucker.ui.viewmodel

import android.arch.lifecycle.ViewModel
import android.databinding.ObservableBoolean
import android.databinding.ObservableField
import android.support.v4.app.FragmentActivity
import android.util.Log
import rx.Subscription
import th.co.growthd.oktrucker.R
import th.co.growthd.oktrucker.model.Work
import th.co.growthd.oktrucker.models.WorkPagination
import th.co.growthd.oktrucker.util.manager.DialogManager
import th.co.growthd.oktrucker.util.network.RxNetwork
import th.co.growthd.oktrucker.util.network.response.WorkListResponse
import th.co.growthd.oktrucker.util.network.service.UserService

class FindWorkViewModel(val fragmentActivity: FragmentActivity) : ViewModel() {

    private var userService: UserService
    var dialogManager: DialogManager
    var workList = mutableListOf<Work>()
    var workPagination = ObservableField<WorkPagination>()
    var isRefresh = ObservableBoolean(false)

    init {
        userService = UserService.getInstance(fragmentActivity)
        dialogManager = DialogManager.getInstance(fragmentActivity.supportFragmentManager)
    }

    fun getWorkList(page: Int = 1, sort: String, query: String? = ""): Subscription? {
        return RxNetwork<WorkListResponse>(fragmentActivity).request(userService.getWork(page, query!!, sort), onSuccess = { response ->
            if (isRefresh.get()) {
                workList.clear()
                isRefresh.set(false)
            }
            workList.addAll(response.workPagination.workList)
            workPagination.set(response.workPagination)

            Log.d("ssssss", query)
            Log.d("ssssss", workList.toString())

        }, onFailure = { error ->
            dialogManager.showError(error)
            if (isRefresh.get()) {
                isRefresh.set(false)
            }
        })
    }
}