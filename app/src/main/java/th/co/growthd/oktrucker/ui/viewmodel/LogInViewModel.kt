package th.co.growthd.oktrucker.ui.viewmodel

import android.arch.lifecycle.ViewModel
import android.databinding.ObservableBoolean
import android.support.v4.app.FragmentActivity
import android.util.Log
import com.crashlytics.android.Crashlytics
import com.google.firebase.iid.FirebaseInstanceId
import rx.Observable
import rx.Subscription
import th.co.growthd.oktrucker.R
import th.co.growthd.oktrucker.model.User
import th.co.growthd.oktrucker.util.enum.UserType
import th.co.growthd.oktrucker.util.manager.DialogManager
import th.co.growthd.oktrucker.util.manager.UserManager
import th.co.growthd.oktrucker.util.network.RxNetwork
import th.co.growthd.oktrucker.util.network.response.UserResponse
import th.co.growthd.oktrucker.util.network.service.AuthService
import th.co.growthd.oktrucker.util.network.service.UserService

class LogInViewModel(val fragmentActivity: FragmentActivity) : ViewModel() {

    private var userManager: UserManager
    private var dialogManager: DialogManager
    private var userService: UserService
    private var authService: AuthService
    private var user: User
    var logInSuccess = ObservableBoolean()

    init {
        user = User.instance
        dialogManager = DialogManager.getInstance(fragmentActivity.supportFragmentManager)
        userService = UserService.getInstance(fragmentActivity)
        authService = AuthService.getInstance(fragmentActivity)
        userManager = UserManager.getInstance(fragmentActivity)
    }

    fun getFirebaseToken(callback: (token: String) -> Unit) {
        FirebaseInstanceId.getInstance().instanceId.addOnCompleteListener { task ->
            if (!task.isSuccessful) {
                callback.invoke("")
                Crashlytics.log(task.exception?.localizedMessage)
            } else {
                callback.invoke(task.result?.token!!)
            }
        }
    }

    fun logInWithTelephone(telephone: String, password: String, firebaseToken: String): Subscription? {
        val observable: Observable<UserResponse> = authService.logInWithTelephone(
                telephone, password, firebaseToken, UserType.DRIVER.type).flatMap {
            userManager.storeAccessToken(it.accessToken)
            return@flatMap userService.getProfile()
        }

        return RxNetwork<UserResponse>(fragmentActivity).request(observable,
                onSuccess = { response ->
                    user.init(response.user)
                    if (user.isVerify == 1) {
                        logInSuccess.set(true)
                    } else {
                        dialogManager.showAlert(R.string.alert_user_not_verify)
                    }
                },
                onFailure = { error ->
                    dialogManager.showError(error)
                },
                onLoading = {
                    dialogManager.showLoading(R.string.loggin_in)
                },
                onLoaded = {
                    dialogManager.hideLoading()
                }
        )
    }
}