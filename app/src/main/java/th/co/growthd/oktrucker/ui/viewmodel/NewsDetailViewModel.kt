package th.co.growthd.oktrucker.ui.viewmodel

import android.arch.lifecycle.ViewModel
import android.databinding.ObservableBoolean
import android.databinding.ObservableField
import android.support.v4.app.FragmentActivity
import rx.Subscription
import th.co.growthd.oktrucker.model.News
import th.co.growthd.oktrucker.util.manager.DialogManager
import th.co.growthd.oktrucker.util.network.RxNetwork
import th.co.growthd.oktrucker.util.network.response.NewsResponse
import th.co.growthd.oktrucker.util.network.service.UserService

class NewsDetailViewModel(val fragmentActivity: FragmentActivity) : ViewModel() {

    private var userService: UserService
    private var dialogManager: DialogManager
    var news = ObservableField<News>()
    var isLoading = ObservableBoolean()

    init {
        dialogManager = DialogManager.getInstance(fragmentActivity.supportFragmentManager)
        userService = UserService.getInstance(fragmentActivity)
    }

    fun getNewsDetail(newsId: Int): Subscription? {
        return RxNetwork<NewsResponse>(fragmentActivity).request(userService.getNewsDetail(newsId),
                onSuccess = { response ->
                    news.set(response.news)
                },
                onFailure = {
                    isLoading.set(false)
                },
                onLoading = {
                    dialogManager.showLoading()
                },
                onLoaded = {
                    dialogManager.hideLoading()
                }
        )
    }
}