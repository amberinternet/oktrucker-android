package th.co.growthd.oktrucker.ui.viewmodel

import android.arch.lifecycle.ViewModel
import android.databinding.ObservableBoolean
import android.databinding.ObservableField
import android.support.v4.app.FragmentActivity
import android.util.Log
import rx.Subscription
import th.co.growthd.oktrucker.R
import th.co.growthd.oktrucker.model.News
import th.co.growthd.oktrucker.models.NewsPagination
import th.co.growthd.oktrucker.util.manager.DialogManager
import th.co.growthd.oktrucker.util.network.RxNetwork
import th.co.growthd.oktrucker.util.network.response.NewsListResponse
import th.co.growthd.oktrucker.util.network.service.UserService

class NewsViewModel(val fragmentActivity: FragmentActivity) : ViewModel() {

    private var userService: UserService
    var dialogManager: DialogManager
    var newsList = mutableListOf<News>()
    var newsPagination = ObservableField<NewsPagination>()
    var isRefresh = ObservableBoolean(false)

    init {
        userService = UserService.getInstance(fragmentActivity)
        dialogManager = DialogManager.getInstance(fragmentActivity.supportFragmentManager)
    }

    fun getNewsList(page: Int = 1) : Subscription? {
        return RxNetwork<NewsListResponse>(fragmentActivity).request(userService.getNews(page), onSuccess = { response ->
            if (isRefresh.get()) {
                newsList.clear()
                isRefresh.set(false)
            }
            newsList.addAll(response.newsPagination.newsList)
            newsPagination.set(response.newsPagination)
        }, onFailure = { error ->
            dialogManager.showError(error)
            if (isRefresh.get()) {
                isRefresh.set(false)
            }
        })
    }
}