package th.co.growthd.oktrucker.ui.viewmodel

import android.arch.lifecycle.ViewModel
import android.content.Intent
import android.support.v4.app.FragmentActivity
import com.crashlytics.android.Crashlytics
import com.google.firebase.iid.FirebaseInstanceId
import th.co.growthd.oktrucker.model.User
import th.co.growthd.oktrucker.ui.activity.LogInActivity
import th.co.growthd.oktrucker.util.manager.UserManager
import th.co.growthd.oktrucker.util.network.RxNetwork
import th.co.growthd.oktrucker.util.network.response.SuccessResponse
import th.co.growthd.oktrucker.util.network.service.UserService

class SettingViewModel(val fragmentActivity: FragmentActivity) : ViewModel() {

    private var userService: UserService
    private var userManager: UserManager
    private var user: User

    init {
        user = User.instance
        userService = UserService.getInstance(fragmentActivity)
        userManager = UserManager.getInstance(fragmentActivity)
    }

    private fun getFirebaseToken(callback: (token: String) -> Unit) {
        FirebaseInstanceId.getInstance().instanceId.addOnCompleteListener { task ->
            if (!task.isSuccessful) {
                callback.invoke("")
                Crashlytics.log(task.exception?.localizedMessage)
            } else {
                callback.invoke(task.result?.token!!)
            }
        }
    }

    fun logOut() {
        getFirebaseToken { token ->
            user.clear()
            userManager.destroySharePreferences()
            val intent = Intent(fragmentActivity, LogInActivity::class.java)
            fragmentActivity.startActivity(intent)
            fragmentActivity.finish()
            RxNetwork<SuccessResponse>(fragmentActivity).request(userService.destroyToken(token))
        }
    }
}