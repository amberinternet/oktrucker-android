package th.co.growthd.oktrucker.ui.viewmodel

import android.arch.lifecycle.ViewModel
import android.content.Intent
import android.support.v4.app.FragmentActivity
import android.util.Log
import rx.Subscription
import th.co.growthd.oktrucker.R
import th.co.growthd.oktrucker.model.User
import th.co.growthd.oktrucker.ui.activity.LogInActivity
import th.co.growthd.oktrucker.util.manager.DialogManager
import th.co.growthd.oktrucker.util.network.RxNetwork
import th.co.growthd.oktrucker.util.network.response.SuccessResponse
import th.co.growthd.oktrucker.util.network.service.AuthService

class SignUp3ViewModel(val fragmentActivity: FragmentActivity) : ViewModel() {

    private var dialogManager: DialogManager
    private var authService: AuthService
    private var user: User

    init {
        user = User.instance
        dialogManager = DialogManager.getInstance(fragmentActivity.supportFragmentManager)
        authService = AuthService.getInstance(fragmentActivity)
    }

    fun signUp(): Subscription? {
        return RxNetwork<SuccessResponse>(fragmentActivity).request(authService.signUp(
                user.firstname,
                user.lastname,
                user.username,
                user.email,
                user.password,
                user.confirmPassword,
                user.address,
                user.driver.companyName,
                user.telephoneNumber,
                user.driver.driverTypeId,
                user.driver.driverLicenseId,
                user.driver.driverLicenseType,
                user.driver.citizenId,
                user.driver.truck.truckType.id,
                user.driver.truck.licensePlate,
                user.driver.truck.truckProvince,
                user.driver.truck.truckYear,
                user.driver.truck.truckModel,
                user.driver.truck.truckBrand,
        1
        ), onSuccess = {
            dialogManager.showSuccess(R.string.sign_up_successful, R.string.alert_waiting_verify) {
                val intent = Intent(fragmentActivity, LogInActivity::class.java)
                intent.flags = Intent.FLAG_ACTIVITY_CLEAR_TOP
                fragmentActivity.startActivity(intent)
                user.clear()
            }
        }, onFailure = { error ->
            dialogManager.showError(error)
        }, onLoading = {
            dialogManager.showLoading(R.string.signing_up)
        }, onLoaded = {
            dialogManager.hideLoading()
        })
    }
}