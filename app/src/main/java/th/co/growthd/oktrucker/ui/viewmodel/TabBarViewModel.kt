package th.co.growthd.oktrucker.ui.viewmodel

import android.arch.lifecycle.ViewModel
import android.support.v4.app.FragmentActivity
import android.util.Log
import th.co.growthd.oktrucker.util.network.RxNetwork
import th.co.growthd.oktrucker.util.network.response.SuccessResponse
import th.co.growthd.oktrucker.util.network.service.UserService

class TabBarViewModel(val fragmentActivity: FragmentActivity) : ViewModel() {

    private var userService: UserService

    init {
        userService = UserService.getInstance(fragmentActivity)
    }

    fun updateLocation(latitude: Double, longitude: Double) {
        Log.d("ssssss update", "$latitude , $longitude")
        RxNetwork<SuccessResponse>(fragmentActivity).request(userService.updateLocation(latitude, longitude))
    }
}