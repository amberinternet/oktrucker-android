package th.co.growthd.oktrucker.ui.viewmodel

import android.arch.lifecycle.ViewModel
import android.databinding.ObservableBoolean
import android.databinding.ObservableField
import android.support.v4.app.FragmentActivity
import android.util.Log
import rx.Subscription
import th.co.growthd.oktrucker.model.Deposit
import th.co.growthd.oktrucker.model.Withdraw
import th.co.growthd.oktrucker.models.DepositPagination
import th.co.growthd.oktrucker.models.WithdrawPagination
import th.co.growthd.oktrucker.util.manager.DialogManager
import th.co.growthd.oktrucker.util.network.RxNetwork
import th.co.growthd.oktrucker.util.network.response.DepositListResponse
import th.co.growthd.oktrucker.util.network.response.WithdrawListResponse
import th.co.growthd.oktrucker.util.network.service.UserService

class TransactionRequestHistoryViewModel(val fragmentActivity: FragmentActivity) : ViewModel() {

    private var userService: UserService
    var dialogManager: DialogManager
    var depositList = mutableListOf<Deposit>()
    var depositPagination = ObservableField<DepositPagination>()
    var withdrawList = mutableListOf<Withdraw>()
    var withdrawPagination = ObservableField<WithdrawPagination>()
    var isRefresh = ObservableBoolean(false)

    init {
        userService = UserService.getInstance(fragmentActivity)
        dialogManager = DialogManager.getInstance(fragmentActivity.supportFragmentManager)
    }

    fun getDepositList(page: Int = 1, date: String): Subscription? {
        return RxNetwork<DepositListResponse>(fragmentActivity).request(userService.getDeposit(page, date), onSuccess = { response ->
            if (isRefresh.get()) {
                depositList.clear()
                isRefresh.set(false)
            }
            depositList.addAll(response.depositPagination.depositList)
            depositPagination.set(response.depositPagination)
        }, onFailure = {error ->
            dialogManager.showError(error)
            if (isRefresh.get()) {
                isRefresh.set(false)
            }
        })
    }

    fun getWithdrawList(page: Int = 1, date: String): Subscription? {
        Log.d("sssss", isRefresh.get().toString())
        return RxNetwork<WithdrawListResponse>(fragmentActivity).request(userService.getWithdraw(page, date), onSuccess = { response ->
            if (isRefresh.get()) {
                withdrawList.clear()
                isRefresh.set(false)
            }
            withdrawList.addAll(response.withdrawPagination.withdrawList)
            withdrawPagination.set(response.withdrawPagination)
        }, onFailure = {error ->
            dialogManager.showError(error)
            if (isRefresh.get()) {
                isRefresh.set(false)
            }
        })
    }
}