package th.co.growthd.oktrucker.ui.viewmodel

import android.arch.lifecycle.ViewModel
import android.support.v4.app.FragmentActivity
import okhttp3.RequestBody
import rx.Subscription
import th.co.growthd.oktrucker.R
import th.co.growthd.oktrucker.util.manager.DialogManager
import th.co.growthd.oktrucker.util.network.RxNetwork
import th.co.growthd.oktrucker.util.network.response.SuccessResponse
import th.co.growthd.oktrucker.util.network.service.UserService
import android.support.v4.content.ContextCompat.startActivity
import android.content.Intent
import android.util.Log
import th.co.growthd.deerandbook.util.NAVIGATION_TAB_KEY
import th.co.growthd.oktrucker.ui.activity.TabBarActivity


class TransferMethodViewModel(val fragmentActivity: FragmentActivity) : ViewModel() {

    private var userService: UserService
    private var dialogManager: DialogManager

    init {
        dialogManager = DialogManager.getInstance(fragmentActivity.supportFragmentManager)
        userService = UserService.getInstance(fragmentActivity)
    }

    fun requestTransferMethod(type: RequestBody, price: RequestBody, payTime: RequestBody, slipImage: RequestBody): Subscription? {
        return RxNetwork<SuccessResponse>(fragmentActivity).request(userService.depositWithTransferSlip(type, price, payTime, slipImage),
                onSuccess = {
                    dialogManager.showSuccess(R.string.send_request_success) {
                        val intent = Intent(fragmentActivity, TabBarActivity::class.java)
                        intent.flags = Intent.FLAG_ACTIVITY_CLEAR_TOP
                        intent.putExtra(NAVIGATION_TAB_KEY, R.id.tab_bar_wallet)
                        fragmentActivity.startActivity(intent)
                    }
                },
                onFailure = { error ->
                    dialogManager.showError(error)
                },
                onLoading = {
                    dialogManager.showLoading(R.string.sending_request)
                },
                onLoaded = {
                    dialogManager.hideLoading()
                }
        )
    }
}