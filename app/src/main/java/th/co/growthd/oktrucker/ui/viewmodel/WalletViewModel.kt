package th.co.growthd.oktrucker.ui.viewmodel

import android.arch.lifecycle.ViewModel
import android.databinding.ObservableBoolean
import android.databinding.ObservableField
import android.support.v4.app.FragmentActivity
import android.util.Log
import rx.Subscription
import th.co.growthd.oktrucker.R
import th.co.growthd.oktrucker.model.Transaction
import th.co.growthd.oktrucker.model.User
import th.co.growthd.oktrucker.models.TransactionPagination
import th.co.growthd.oktrucker.util.manager.DialogManager
import th.co.growthd.oktrucker.util.network.RxNetwork
import th.co.growthd.oktrucker.util.network.response.TransactionsListResponse
import th.co.growthd.oktrucker.util.network.response.UserResponse
import th.co.growthd.oktrucker.util.network.service.UserService

class WalletViewModel(val fragmentActivity: FragmentActivity) : ViewModel() {

    private var userService: UserService
    private var user: User
    var dialogManager: DialogManager
    var transactionList = mutableListOf<Transaction>()
    var transactionPagination = ObservableField<TransactionPagination>()
    var isRefresh = ObservableBoolean(false)
    var isLoadedProfile = ObservableBoolean(false)

    init {
        user = User.instance
        userService = UserService.getInstance(fragmentActivity)
        dialogManager = DialogManager.getInstance(fragmentActivity.supportFragmentManager)
    }

    fun getTransactionList(page: Int = 1, date: String): Subscription? {
        return RxNetwork<TransactionsListResponse>(fragmentActivity).request(userService.getTransaction(page, date), onSuccess = { response ->
            if (isRefresh.get()) {
                transactionList.clear()
                isRefresh.set(false)
            }
            transactionList.addAll(response.transactionPagination.transactionList)
            transactionPagination.set(response.transactionPagination)
        }, onFailure = {error ->
            dialogManager.showError(error)
            if (isRefresh.get()) {
                isRefresh.set(false)
            }
        })
    }

    fun getProfile(): Subscription? {
        return RxNetwork<UserResponse>(fragmentActivity).request(userService.getProfile(), onSuccess = { response ->
            user.init(response.user)
            isLoadedProfile.set(true)
        }, onFailure = { error ->

        })
    }
}