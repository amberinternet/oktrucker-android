package th.co.growthd.oktrucker.ui.viewmodel

import android.arch.lifecycle.ViewModel
import android.support.v4.app.FragmentActivity
import rx.Subscription
import th.co.growthd.oktrucker.R
import th.co.growthd.oktrucker.util.manager.DialogManager
import th.co.growthd.oktrucker.util.network.RxNetwork
import th.co.growthd.oktrucker.util.network.response.SuccessResponse
import th.co.growthd.oktrucker.util.network.service.UserService

class WithdrawViewModel(val fragmentActivity: FragmentActivity) : ViewModel() {

    private var dialogManager: DialogManager
    private var userService: UserService

    init {
        dialogManager = DialogManager.getInstance(fragmentActivity.supportFragmentManager)
        userService = UserService.getInstance(fragmentActivity)
    }

    fun requestWithdraw(price: Int): Subscription? {
        return RxNetwork<SuccessResponse>(fragmentActivity).request(userService.withdraw(price),
                onSuccess = {
                    dialogManager.showSuccess(R.string.send_request_success) {
                        fragmentActivity.finish()
                    }
                },
                onFailure = { error ->
                    dialogManager.showError(error)
                },
                onLoading = {
                    dialogManager.showLoading(R.string.sending_request)
                },
                onLoaded = {
                    dialogManager.hideLoading()
                }
        )
    }
}