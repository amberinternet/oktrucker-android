package th.co.growthd.oktrucker.ui.viewmodel

import android.app.Activity
import android.arch.lifecycle.ViewModel
import android.content.Intent
import android.databinding.ObservableBoolean
import android.databinding.ObservableInt
import android.support.v4.app.FragmentActivity
import android.util.Log
import okhttp3.RequestBody
import rx.Subscription
import th.co.growthd.oktrucker.R
import th.co.growthd.oktrucker.model.Work
import th.co.growthd.oktrucker.util.enum.WorkStatus
import th.co.growthd.oktrucker.util.manager.DialogManager
import th.co.growthd.oktrucker.util.network.RxNetwork
import th.co.growthd.oktrucker.util.network.response.SuccessResponse
import th.co.growthd.oktrucker.util.network.response.WorkResponse
import th.co.growthd.oktrucker.util.network.service.UserService

class WorkDetailViewModel(val fragmentActivity: FragmentActivity) : ViewModel() {

    private var userService: UserService
    private var dialogManager: DialogManager
    lateinit var work: Work
    var status = ObservableInt()
    var isRefresh = ObservableBoolean(false)

    init {
        dialogManager = DialogManager.getInstance(fragmentActivity.supportFragmentManager)
        userService = UserService.getInstance(fragmentActivity)
    }

    fun getWorkDetail(workId: Int): Subscription? {
        return RxNetwork<WorkResponse>(fragmentActivity).request(userService.getWorkDetail(workId),
                onSuccess = { response ->
                    print(response.work.toString())
                    if (isRefresh.get()) {
                        work = response.work
                        isRefresh.set(false)
                    }
                },
                onFailure = { error ->
                    dialogManager.showError(error)
                    if (isRefresh.get()) {
                        isRefresh.set(false)
                    }
                })
    }

    fun acceptWork(): Subscription? {
        return RxNetwork<SuccessResponse>(fragmentActivity).request(userService.acceptWork(work.id),
                onSuccess = {
                    work.status = WorkStatus.WAITING_CONFIRM_WORK.status
                    dialogManager.showSuccess(R.string.accept_work_success) {
                        status.set(WorkStatus.WAITING_CONFIRM_WORK.status)
                        val returnIntent = Intent()
                        fragmentActivity.setResult(Activity.RESULT_OK, returnIntent)
                        fragmentActivity.finish()                                                           //TODO: change to success Dialog
                    }
                },
                onFailure = { error ->
                    dialogManager.showError(error)
                },
                onLoading = {
                    dialogManager.showLoading(R.string.sending_data)
                },
                onLoaded = {
                    dialogManager.hideLoading()
                })
    }

    fun arrivedSource(): Subscription? {
        return RxNetwork<SuccessResponse>(fragmentActivity).request(userService.arrivedSource(work.id),
                onSuccess = {
                    work.status = WorkStatus.ARRIVED_SOURCE.status
                    dialogManager.showSuccess(R.string.arrived_source) {
                        status.set(WorkStatus.ARRIVED_SOURCE.status)
                        fragmentActivity.finish()
                    }
                },
                onFailure = { error ->
                    dialogManager.showError(error)
                },
                onLoading = {
                    dialogManager.showLoading(R.string.sending_data)
                },
                onLoaded = {
                    dialogManager.hideLoading()
                })
    }

    fun departSource(autograph: RequestBody): Subscription? {
        return RxNetwork<SuccessResponse>(fragmentActivity).request(userService.departSource(work.id, autograph),
                onSuccess = {
                    work.status = WorkStatus.WAITING_SENT_ITEM.status
                    dialogManager.showSuccess(R.string.receive_item_success) {
                        status.set(WorkStatus.WAITING_SENT_ITEM.status)
                        fragmentActivity.finish()
                    }
                },
                onFailure = { error ->
                    dialogManager.showError(error)
                },
                onLoading = {
                    dialogManager.showLoading(R.string.sending_data)
                },
                onLoaded = {
                    dialogManager.hideLoading()
                })
    }

    fun arrivedDestination(): Subscription? {
        return RxNetwork<SuccessResponse>(fragmentActivity).request(userService.arrivedDestination(work.id),
                onSuccess = {
                    work.status = WorkStatus.ARRIVED_DESTINATION.status
                    dialogManager.showSuccess(R.string.arrived_destination) {
                        status.set(WorkStatus.ARRIVED_DESTINATION.status)
                        fragmentActivity.finish()
                    }
                },
                onFailure = { error ->
                    dialogManager.showError(error)
                },
                onLoading = {
                    dialogManager.showLoading(R.string.sending_data)
                },
                onLoaded = {
                    dialogManager.hideLoading()
                })
    }

    fun completeWork(autograph: RequestBody): Subscription? {
        return RxNetwork<SuccessResponse>(fragmentActivity).request(userService.completeWork(work.id, autograph),
                onSuccess = {
                    work.status = WorkStatus.COMPLETE.status
                    dialogManager.showSuccess(R.string.completed_work) {
                        status.set(WorkStatus.COMPLETE.status)
                    }
                },
                onFailure = { error ->
                    dialogManager.showError(error)
                },
                onLoading = {
                    dialogManager.showLoading(R.string.sending_data)
                },
                onLoaded = {
                    dialogManager.hideLoading()
                })
    }
}