package th.co.growthd.oktrucker.util;

import android.annotation.SuppressLint;
import android.content.Context;
import android.content.res.AssetManager;
import android.graphics.Typeface;
import android.support.design.internal.BottomNavigationItemView;
import android.support.design.internal.BottomNavigationMenuView;
import android.support.design.widget.BottomNavigationView;
import android.text.SpannableStringBuilder;
import android.util.DisplayMetrics;
import android.util.Log;
import android.util.TypedValue;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;

import java.lang.reflect.Field;

/**
 * Created by bulakorn on 10/4/2017 AD.
 */

public class BottomNavigationViewHelper {

    @SuppressLint("RestrictedApi")
    public static void disableShiftMode(BottomNavigationView view) {
        BottomNavigationMenuView menuView = (BottomNavigationMenuView) view.getChildAt(0);
        try {
            Field shiftingMode = menuView.getClass().getDeclaredField("mShiftingMode");
            shiftingMode.setAccessible(true);
            shiftingMode.setBoolean(menuView, false);
            shiftingMode.setAccessible(false);
            for (int i = 0; i < menuView.getChildCount(); i++) {
                BottomNavigationItemView item = (BottomNavigationItemView) menuView.getChildAt(i);
                //noinspection RestrictedApi
                item.setShifting(false);
                // set once again checked readableStatus, so view will be updated
                //noinspection RestrictedApi
                item.setChecked(item.getItemData().isChecked());
            }
        } catch (NoSuchFieldException e) {
            Log.e("BNVHelper", "Unable to get shift mode field", e);
        } catch (IllegalAccessException e) {
            Log.e("BNVHelper", "Unable to change readableStatus of shift mode", e);
        }
    }

    public static void setIconSize(BottomNavigationView view, int width, int height, float topPaddingDp, Context context) {
        BottomNavigationMenuView menuView = (BottomNavigationMenuView) view.getChildAt(0);
        for (int i = 0; i < menuView.getChildCount(); i++) {
            final View iconView = menuView.getChildAt(i).findViewById(android.support.design.R.id.icon);
            final ViewGroup.LayoutParams layoutParams = iconView.getLayoutParams();
            final DisplayMetrics displayMetrics = context.getResources().getDisplayMetrics();

            if (i == 0) {   // center menu
                // set your width here
                layoutParams.width = (int) TypedValue.applyDimension(TypedValue.COMPLEX_UNIT_DIP, width, displayMetrics);
                // set your height here
                layoutParams.height = (int) TypedValue.applyDimension(TypedValue.COMPLEX_UNIT_DIP, (height) + topPaddingDp, displayMetrics);
                iconView.setLayoutParams(layoutParams);

                // set top padding here
                int topPaddingPixel = (int)((topPaddingDp - 2) * displayMetrics.density);
                iconView.setPadding(0,topPaddingPixel,0,0);
            }  if (i == 1) {   // center menu
                // set your width here
                layoutParams.width = (int) TypedValue.applyDimension(TypedValue.COMPLEX_UNIT_DIP, width, displayMetrics);
                // set your height here
                layoutParams.height = (int) TypedValue.applyDimension(TypedValue.COMPLEX_UNIT_DIP, (height) + topPaddingDp, displayMetrics);
                iconView.setLayoutParams(layoutParams);

                // set top padding here
                int topPaddingPixel = (int)((topPaddingDp) * displayMetrics.density);
                iconView.setPadding(4,topPaddingPixel,0,0);
            } else if (i == 2) {   // center menu
                // set your width here
                layoutParams.width = (int) TypedValue.applyDimension(TypedValue.COMPLEX_UNIT_DIP, width + 3, displayMetrics);
                // set your height here
                layoutParams.height = (int) TypedValue.applyDimension(TypedValue.COMPLEX_UNIT_DIP, (height + 3) + topPaddingDp, displayMetrics);
                iconView.setLayoutParams(layoutParams);

                // set top padding here
                int topPaddingPixel = (int)((topPaddingDp - 2) * displayMetrics.density);
                iconView.setPadding(0,topPaddingPixel,0,0);
            } else if (i == 3) {   // center menu
                // set your width here
                layoutParams.width = (int) TypedValue.applyDimension(TypedValue.COMPLEX_UNIT_DIP, width + 1, displayMetrics);
                // set your height here
                layoutParams.height = (int) TypedValue.applyDimension(TypedValue.COMPLEX_UNIT_DIP, (height + 1) + topPaddingDp, displayMetrics);
                iconView.setLayoutParams(layoutParams);

                // set top padding here
                int topPaddingPixel = (int)((topPaddingDp - 1) * displayMetrics.density);
                iconView.setPadding(6,topPaddingPixel,0,0);
            } else {
                // set your width here
                layoutParams.width = (int) TypedValue.applyDimension(TypedValue.COMPLEX_UNIT_DIP, width, displayMetrics);
                // set your height here
                layoutParams.height = (int) TypedValue.applyDimension(TypedValue.COMPLEX_UNIT_DIP, (height)+ topPaddingDp, displayMetrics);
                iconView.setLayoutParams(layoutParams);

                // set top padding here
                int topPaddingPixel = (int)((topPaddingDp) * displayMetrics.density);
                iconView.setPadding(0,topPaddingPixel,0,0);
            }
        }
    }

    public static void setLabelPadding(BottomNavigationView view, float bottomPaddingDp, Context context) {
        BottomNavigationMenuView menuView = (BottomNavigationMenuView) view.getChildAt(0);
        for (int i = 0; i < menuView.getChildCount(); i++) {
            final View largeLabel = menuView.getChildAt(i).findViewById(android.support.design.R.id.largeLabel);
            final View smallLabel = menuView.getChildAt(i).findViewById(android.support.design.R.id.smallLabel);
            final DisplayMetrics displayMetrics = context.getResources().getDisplayMetrics();

            // set top padding here
            int bottomPaddingPixel = (int)(bottomPaddingDp * displayMetrics.density);
            largeLabel.setPadding(0,0,0,bottomPaddingPixel);
            smallLabel.setPadding(0,0,0,bottomPaddingPixel);
        }
    }
}
