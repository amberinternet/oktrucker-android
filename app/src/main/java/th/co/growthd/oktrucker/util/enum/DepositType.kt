package th.co.growthd.oktrucker.util.enum

enum class DepositType(val value: Int, val method: String) {
    OMISE(1, "บัตรเครดิต/เดบิต"),
    TRANSFER(2, "เงินโอนแนบสลิป")
}