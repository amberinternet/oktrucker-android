package th.co.growthd.oktrucker.util.enum

enum class PaymentMethod(val status: Int, val pay: String, val receive: String) {
    CASH_SOURCE(1, "ชำระเงินสดต้นทาง", "งานเงินสด (ชำระต้นทาง)"),
    CASH_DESTINATION(2, "ชำระเงินสดปลายทาง", "งานเงินสด (ชำระปลายทาง)"),
    WALLET(3, "ชำระด้วย Wallet", "งานเครดิต"),
    OMISE(4, "ชำระด้วยบัตร", "งานเครดิต")
}