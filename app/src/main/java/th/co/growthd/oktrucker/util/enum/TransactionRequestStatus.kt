package th.co.growthd.oktrucker.util.enum

enum class TransactionRequestStatus(val status: Int, val depositMessage: String, val withdrawMessage: String) {
    WAITING(0, "รอเติม", "รอโอน"),
    APPROVED(1, "เติมสำเร็จ", "โอนสำเร็จ"),
    REJECT(2, "ไม่อนุมัติ", "ไม่อนุมัติ")
}