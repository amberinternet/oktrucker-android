package th.co.growthd.oktrucker.util.enum

import th.co.growthd.oktrucker.R

enum class TruckTypeEnum(val id: Int, val typeName: String, val description: String, val imageResId: Int) {
    TRUCK_9(9, "เทรเล่อร์", "ไม่เกิน 25,000 kg", R.drawable.truck_9),
    TRUCK_8(8, "10 ล้อเฮี๊ยบ", "ไม่เกิน 20,000 kg", R.drawable.truck_8),
    TRUCK_7(7, "10 ล้อทึบ", "ไม่เกิน 20,000 kg", R.drawable.truck_7),
    TRUCK_6(6, "10 ล้อคอก", "ไม่เกิน 20,000 kg", R.drawable.truck_6),
    TRUCK_5(5, "6 ล้อเฮี๊ยบ", "ไม่เกิน 10,000 kg", R.drawable.truck_5),
    TRUCK_4(4, "6 ล้อทึบ", "ไม่เกิน 10,000 kg", R.drawable.truck_4),
    TRUCK_3(3, "6 ล้อคอก", "ไม่เกิน 10,000 kg", R.drawable.truck_3);
}