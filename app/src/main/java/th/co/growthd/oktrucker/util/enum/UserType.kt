package th.co.growthd.oktrucker.util.enum

enum class UserType(val type: String) {
    DRIVER("driver")
}