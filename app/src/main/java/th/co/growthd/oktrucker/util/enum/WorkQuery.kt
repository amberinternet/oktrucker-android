package th.co.growthd.oktrucker.util.enum

enum class WorkQuery(val value: String) {
    STATUS_IN_PROGRESS("in-progress"),
    STATUS_COMPLETED("completed"),
    SORT_ASC("asc"),
    SORT_DESC("desc")
}