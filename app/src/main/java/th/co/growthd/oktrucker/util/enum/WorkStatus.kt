package th.co.growthd.oktrucker.util.enum

enum class WorkStatus(val status: Int, val readableStatus: String, val readableAction: String) {
    PAYMENT_FAIL(0, "จ่ายเงินด้วยบัตรไม่สำเร็จ", ""),
    FIND_TRUCK(1, "กำลังหารถ", "รับงาน"),
    WAITING_CONFIRM_WORK(2, "รอคอนเฟริม", "ยกเลิก"),
    WAITING_RECEIVE_ITEM(3, "รอขึ้นของ", "ถึงต้นทาง"),
    ARRIVED_SOURCE(4, "ถึงต้นทาง", "ขึ้นของเสร็จ"),
    WAITING_SENT_ITEM(5, "กำลังจัดส่ง", "ถึงปลายทาง"),
    ARRIVED_DESTINATION(6, "ถึงปลายทาง", "ลงของเสร็จ"),
    COMPLETE(7, "เสร็จงาน", ""),
    CANCEL(8, "งานยกเลิก", "");
}