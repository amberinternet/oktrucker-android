package th.co.growthd.oktrucker.util.firebase

import android.app.NotificationChannel
import android.app.NotificationManager
import android.app.PendingIntent
import android.content.Context
import android.content.Intent
import android.media.RingtoneManager
import android.os.Build
import android.support.v4.app.NotificationCompat
import android.util.Log
import com.firebase.jobdispatcher.FirebaseJobDispatcher
import com.firebase.jobdispatcher.GooglePlayDriver
import com.google.firebase.messaging.FirebaseMessagingService
import com.google.firebase.messaging.RemoteMessage
import th.co.growthd.oktrucker.R
import th.co.growthd.oktrucker.ui.activity.SplashScreenActivity
import th.co.growthd.oktrucker.util.manager.UserManager
import android.app.TaskStackBuilder
import th.co.growthd.deerandbook.util.NAVIGATION_TAB_KEY
import th.co.growthd.oktrucker.ui.activity.TabBarActivity


class MyFirebaseMessagingService : FirebaseMessagingService() {

    override fun onMessageReceived(remoteMessage: RemoteMessage?) {
        val userManager = UserManager.getInstance(applicationContext)

        if (userManager.hasToken() && userManager.isEnableNotification) {
            remoteMessage?.data?.isNotEmpty()?.let {
                var body = remoteMessage.data["body"]
                sendNotification(body!!)
                scheduleJob()
            }
        }

        // Check if depositMessage contains a notification payload.
        remoteMessage?.notification?.let {}
    }

    override fun onNewToken(token: String?) {
        sendRegistrationToServer(token)
    }

    private fun scheduleJob() {
        val dispatcher = FirebaseJobDispatcher(GooglePlayDriver(this))
        val myJob = dispatcher.newJobBuilder()
                .setService(MyJobService::class.java)
                .setTag("my-job-tag")
                .build()
        dispatcher.schedule(myJob)
    }

    private fun handleNow() {
        Log.d(TAG, "Short lived task is done.")
    }

    private fun sendRegistrationToServer(token: String?) {
        // TODO: Implement this method to send token to your app server.
    }

    private fun sendNotification(messageBody: String) {
        val intent = Intent(this, SplashScreenActivity::class.java)
        intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP)
        val pendingIntent = PendingIntent.getActivity(this, 0 /* Request code */, intent,
                PendingIntent.FLAG_ONE_SHOT)

        val channelId = getString(R.string.default_notification_channel_id)
        val defaultSoundUri = RingtoneManager.getDefaultUri(RingtoneManager.TYPE_NOTIFICATION)
        val notificationBuilder = NotificationCompat.Builder(this, channelId)
                .setSmallIcon(R.mipmap.ic_launcher)
                .setContentTitle(getString(R.string.app_name))
                .setContentText(messageBody)
                .setContentIntent(getPendingIntent())
                .setAutoCancel(true)
                .setSound(defaultSoundUri)
                .setVibrate(longArrayOf(500,500))
                .setPriority(NotificationCompat.PRIORITY_HIGH)
                .setVisibility(NotificationCompat.VISIBILITY_PUBLIC)
                .setContentIntent(pendingIntent)

        val notificationManager = getSystemService(Context.NOTIFICATION_SERVICE) as NotificationManager

        // Since android Oreo notification channel is needed.
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
            val channel = NotificationChannel(channelId,
                    "OKTrucker Channel",
                    NotificationManager.IMPORTANCE_DEFAULT)
            notificationManager.createNotificationChannel(channel)
        }

        notificationManager.notify(0, notificationBuilder.build())
    }

    private fun getPendingIntent(): PendingIntent {
        val intent = Intent(applicationContext, TabBarActivity::class.java)
        intent.putExtra(NAVIGATION_TAB_KEY, R.id.tab_bar_my_work)
        val stackBuilder = TaskStackBuilder.create(applicationContext)
        stackBuilder.addParentStack(TabBarActivity::class.java)
        stackBuilder.addNextIntent(intent)
        return stackBuilder.getPendingIntent(0, PendingIntent.FLAG_UPDATE_CURRENT)
    }

    companion object {

        private const val TAG = "MyFirebaseMsgService"
    }
}