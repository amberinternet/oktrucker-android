package th.co.growthd.oktrucker.util.glide

import com.bumptech.glide.load.model.LazyHeaders
import com.bumptech.glide.load.model.GlideUrl
import th.co.growthd.deerandbook.util.HEADER_KEY
import th.co.growthd.deerandbook.util.TOKEN_TYPE

internal object GlideHeader {

    fun getUrlWithHeaders(url: String, token: String): GlideUrl {
        return GlideUrl(url, LazyHeaders.Builder()
                .addHeader(HEADER_KEY, TOKEN_TYPE + token)
                .build())
    }
}