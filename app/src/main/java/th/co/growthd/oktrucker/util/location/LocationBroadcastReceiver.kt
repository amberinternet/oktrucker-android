package th.co.growthd.oktrucker.util.location

import android.content.BroadcastReceiver
import android.content.Context
import android.content.Intent
import android.util.Log
import com.google.android.gms.location.LocationResult
import org.greenrobot.eventbus.EventBus
import th.co.growthd.oktrucker.model.subscriber.LocationSubscriber

class LocationBroadcastReceiver: BroadcastReceiver() {

    override fun onReceive(context: Context?, intent: Intent?) {
        Log.d("ssssss", "receive")
        val result = LocationResult.extractResult(intent)
        if (result != null) {
            val locations = result.locations
            for (location in locations) {
                Log.d("ssssss", location.toString())
                EventBus.getDefault().post(LocationSubscriber(location.latitude, location.longitude))
            }
        }
    }
}