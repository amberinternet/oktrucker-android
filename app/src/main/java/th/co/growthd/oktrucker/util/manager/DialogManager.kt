package th.co.growthd.oktrucker.util.manager

import android.graphics.Bitmap
import android.support.v4.app.FragmentManager
import th.co.growthd.oktrucker.R
import th.co.growthd.oktrucker.model.TruckType
import th.co.growthd.oktrucker.model.Work
import th.co.growthd.oktrucker.ui.dialog.*
import th.co.growthd.oktrucker.ui.dialog.listener.ErrorDialogListener

class DialogManager private constructor(private val fragmentManager: FragmentManager) {

    companion object {

        @Volatile
        private var INSTANCE: DialogManager? = null

        fun getInstance(fragmentManager: FragmentManager): DialogManager = INSTANCE
                ?: synchronized(this) {
                    INSTANCE ?: DialogManager(fragmentManager)
                }
    }

    fun showContactAdmin(titleRes: Int = R.string.please_contact_admin, messageRes: Int = R.string.need_help_from_admin) {
        ContactAdminDialogFragment.newInstance(titleRes, messageRes).show(fragmentManager, ContactAdminDialogFragment::class.java.simpleName)
    }

    fun showAlert(messageRes: Int) {
        AlertDialogFragment.newInstance(messageRes).show(fragmentManager, AlertDialogFragment::class.java.simpleName)
    }

    fun showAlert(message: String) {
        AlertDialogFragment.newInstance(message).show(fragmentManager, AlertDialogFragment::class.java.simpleName)
    }

    fun showAlert(messageRes: Int, onDismiss: (() -> Unit)? = null) {
        AlertDialogFragment.newInstance(messageRes).apply {
            this.onDismiss = onDismiss
        }.show(fragmentManager, AlertDialogFragment::class.java.simpleName)
    }

    fun showAlert(titleRes: Int, messageRes: Int, actionRes: Int, onDismiss: (() -> Unit)? = null) {
        AlertDialogFragment.newInstance(titleRes, messageRes, actionRes).apply {
            this.onDismiss = onDismiss
        }.show(fragmentManager, AlertDialogFragment::class.java.simpleName)
    }

    fun showAlert(title: String, message: String, action: String, onDismiss: (() -> Unit)? = null) {
        AlertDialogFragment.newInstance(title, message, action).apply {
            this.onDismiss = onDismiss
        }.show(fragmentManager, AlertDialogFragment::class.java.simpleName)
    }

    fun showAlertReceiveCash(messageRes: Int, price: String, onDismiss: (() -> Unit)? = null) {
        AlertReceiveCashDialogFragment.newInstance(messageRes, price).apply {
            this.onDismiss = onDismiss
        }.show(fragmentManager, AlertReceiveCashDialogFragment::class.java.simpleName)
    }

    fun showConfirm(titleRes: Int, messageRes: Int, onConfirm: (() -> Unit)? = null) {
        ConfirmDialogFragment.newInstance(titleRes, messageRes).apply {
            this.onConfirm = onConfirm
        }.show(fragmentManager, ConfirmDialogFragment::class.java.simpleName)
    }

    fun showConfirm(titleRes: Int, messageRes: Int, onConfirm: (() -> Unit)? = null, onCancel: (() -> Unit)? = null) {
        ConfirmDialogFragment.newInstance(titleRes, messageRes).apply {
            this.onConfirm = onConfirm
            this.onCancel = onCancel
        }.show(fragmentManager, ConfirmDialogFragment::class.java.simpleName)
    }

    fun showConfirmAcceptWork(work: Work, onAccept: () -> Unit) {
        ConfirmAcceptWorkDialogFragment.newInstance(work).apply {
            this.onAccept = onAccept
        }.show(fragmentManager, ConfirmAcceptWorkDialogFragment::class.java.simpleName)
    }

    fun showConfirmSignature(titleRes: Int, onConfirm: (bigmap: Bitmap) -> Unit) {
        ConfirmSignatureDialogFragment.newInstance(titleRes).apply {
            this.onConfirm = onConfirm
        }.show(fragmentManager, ConfirmSignatureDialogFragment::class.java.simpleName)
    }

    fun showError(error: String) {
        ErrorDialogFragment.newInstance(error).show(fragmentManager, ErrorDialogFragment::class.java.simpleName)
    }

    fun showError(error: String, listener: ErrorDialogListener) {
        ErrorDialogFragment.newInstance(error).apply {
            errorDialogListener = listener
        }.show(fragmentManager, ErrorDialogFragment::class.java.simpleName)
    }

    fun showSuccess(messageRes: Int, onDismiss: (() -> Unit)? = null) {
        SuccessDialogFragment.newInstance(messageRes).apply {
            this.onDismiss = onDismiss
        }.show(fragmentManager, SuccessDialogFragment::class.java.simpleName)
    }

    fun showSuccess(titleRes: Int, messageRes: Int, onDismiss: (() -> Unit)? = null) {
        SuccessDialogFragment.newInstance(titleRes, messageRes).apply {
            this.onDismiss = onDismiss
        }.show(fragmentManager, SuccessDialogFragment::class.java.simpleName)
    }

    fun showPrivacy() {
        PrivacyPolicyDialogFragment.newInstance().show(fragmentManager, PrivacyPolicyDialogFragment::class.java.simpleName)
    }

    fun showTerm() {
        TermAndConditionDialogFragment.newInstance().show(fragmentManager, TermAndConditionDialogFragment::class.java.simpleName)
    }

    fun showTruckType(truckType: TruckType? = null) {
        TruckTypeDialogFragment.newInstance(truckType).show(fragmentManager, TruckTypeDialogFragment::class.java.simpleName)
    }

    fun showLoading(loading: String = "กำลังโหลดข้อมูล...") {
        ProgressBarDialogFragment.newInstance(loading).show(fragmentManager, ProgressBarDialogFragment::class.java.simpleName)
    }

    fun showLoading(loadingRes: Int) {
        ProgressBarDialogFragment.newInstance(loadingRes).show(fragmentManager, ProgressBarDialogFragment::class.java.simpleName)
    }

    fun hideLoading() {
        val fragment = fragmentManager.findFragmentByTag(ProgressBarDialogFragment::class.java.simpleName) as? ProgressBarDialogFragment
        fragment?.let { it.dismiss() }
    }
}