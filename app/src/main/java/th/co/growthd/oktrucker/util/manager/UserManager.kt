package th.co.growthd.oktrucker.util.manager

import android.content.Context
import android.content.SharedPreferences
import android.preference.PreferenceManager
import th.co.growthd.deerandbook.util.IS_ENABLE_NOTIFICATION_KEY
import th.co.growthd.deerandbook.util.INITIAL_STRING
import th.co.growthd.deerandbook.util.TOKEN_KEY
import th.co.growthd.oktrucker.model.AccessToken
import th.co.growthd.oktrucker.putBoolean
import th.co.growthd.oktrucker.putString

class UserManager(val context: Context) {

    private val sharedPreferences: SharedPreferences = PreferenceManager.getDefaultSharedPreferences(context)
    val token: String get() = sharedPreferences.getString(TOKEN_KEY, INITIAL_STRING)
    val isEnableNotification: Boolean get() = sharedPreferences.getBoolean(IS_ENABLE_NOTIFICATION_KEY, true)

    companion object {

        @Volatile
        private var INSTANCE: UserManager? = null

        fun getInstance(context: Context): UserManager = INSTANCE
                ?: synchronized(this) {
                    INSTANCE ?: UserManager(context)
                }
    }

    fun storeAccessToken(accessToken: AccessToken) {
        sharedPreferences.putString(TOKEN_KEY, accessToken.token)
    }

    fun hasToken(): Boolean {
        return sharedPreferences.contains(TOKEN_KEY)
    }

    fun setEnableNotification(isEnable: Boolean) {
        sharedPreferences.putBoolean(IS_ENABLE_NOTIFICATION_KEY, isEnable)
    }

    fun destroySharePreferences() {
        sharedPreferences.edit().clear().apply()
    }
}