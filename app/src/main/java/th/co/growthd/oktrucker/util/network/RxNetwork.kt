package th.co.growthd.oktrucker.util.network

import android.content.Context
import com.google.gson.Gson
import rx.Subscriber
import rx.schedulers.Schedulers
import android.util.Log
import retrofit2.adapter.rxjava.HttpException
import rx.Observable
import rx.Subscription
import rx.android.schedulers.AndroidSchedulers
import th.co.growthd.oktrucker.R
import th.co.growthd.oktrucker.util.manager.NetworkManager
import th.co.growthd.oktrucker.util.network.response.ErrorResponse
import java.io.IOException
import java.net.HttpURLConnection
import java.util.concurrent.TimeUnit

class RxNetwork<T>(val context: Context) {

    private var response: T? = null

    /* Handle status network */
    private val messageNoInternetConnection = context.getString(R.string.status_no_internet_connection)
    private val messageServerInternalError = context.getString(R.string.status_server_internal_error)
    private val isAvailableNetwork: Boolean get() = NetworkManager.getInstance(context).isConnectingInternet()

    fun request(observable: Observable<T>,
                onSuccess: (response: T) -> Unit,
                onFailure: (error: String) -> Unit,
                onLoading: () -> Unit,
                onLoaded: () -> Unit): Subscription? {
        if (isAvailableNetwork) {
            onLoading.invoke()
            return observable.subscribeOn(Schedulers.io())
                    .observeOn(AndroidSchedulers.mainThread())
                    .unsubscribeOn(Schedulers.io())
                    .take(120, TimeUnit.SECONDS)
                    .subscribe(object : Subscriber<T>() {
                        override fun onCompleted() {
                            response?.let {
                                onLoaded.invoke()
                                onSuccess.invoke(it)
                            }
                        }

                        override fun onError(throwable: Throwable) {
                            Log.d("error", throwable.localizedMessage)
                            onLoaded.invoke()
                            onFailure(getError(throwable))
                        }

                        override fun onNext(response: T) {
                            this@RxNetwork.response = response
                        }
                    })
        } else {
            onFailure.invoke(messageNoInternetConnection)
        }

        return null
    }

    fun request(observable: Observable<T>,
                onSuccess: (response: T) -> Unit,
                onFailure: (error: String) -> Unit
    ): Subscription? {
        if (isAvailableNetwork) {
            return observable.subscribeOn(Schedulers.io())
                    .observeOn(AndroidSchedulers.mainThread())
                    .unsubscribeOn(Schedulers.io())
                    .take(120, TimeUnit.SECONDS)
                    .subscribe(object : Subscriber<T>() {
                        override fun onCompleted() {
                            response?.let {
                                onSuccess.invoke(it)
                            }
                        }

                        override fun onError(throwable: Throwable) {
                            Log.d("error", throwable.localizedMessage)
                            onFailure(getError(throwable))
                        }

                        override fun onNext(response: T) {
                            this@RxNetwork.response = response
                        }
                    })
        } else {
            onFailure.invoke(messageNoInternetConnection)
        }

        return null
    }

    fun request(observable: Observable<T>): Subscription? {
        if (isAvailableNetwork) {
            return observable.subscribeOn(Schedulers.io())
                    .observeOn(AndroidSchedulers.mainThread())
                    .unsubscribeOn(Schedulers.io())
                    .take(120, TimeUnit.SECONDS)
                    .subscribe(object : Subscriber<T>() {
                        override fun onCompleted() {

                        }

                        override fun onError(throwable: Throwable) {

                        }

                        override fun onNext(response: T) {

                        }
                    })
        }

        return null
    }

    private fun getError(throwable: Throwable): String {
        var error = ""
        if (throwable is HttpException) {
            val response = throwable.response()
            when (response.code()) {
                HttpURLConnection.HTTP_BAD_GATEWAY, HttpURLConnection.HTTP_INTERNAL_ERROR, HttpURLConnection.HTTP_GATEWAY_TIMEOUT -> error = messageServerInternalError
                HttpURLConnection.HTTP_FORBIDDEN, HttpURLConnection.HTTP_BAD_REQUEST, HttpURLConnection.HTTP_BAD_METHOD, HttpURLConnection.HTTP_CLIENT_TIMEOUT, HttpURLConnection.HTTP_GONE, HttpURLConnection.HTTP_UNAUTHORIZED, HttpURLConnection.HTTP_NOT_FOUND -> try {
                    val responseServer = response.errorBody()?.string()
                    Log.d("ssssss", responseServer)
                    val errorResponse = Gson().fromJson<ErrorResponse>(responseServer, ErrorResponse::class.java)
                    error = errorResponse.errorDescription?.getError() ?: errorResponse.error
                } catch (e: IOException) {
                    e.printStackTrace()
                    error = "Error IOException"
                }
            }
        } else {
            Log.d("ssssss", throwable.message)
            error = "ขออภัย พบข้อผิดพลาดในการติดต่อระบบ"
        }

        return error
    }
}