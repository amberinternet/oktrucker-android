package th.co.growthd.oktrucker.util.network.response

import com.google.gson.annotations.SerializedName
import th.co.growthd.oktrucker.model.AccessToken

data class AccessTokenResponse (@SerializedName("data") var accessToken: AccessToken)