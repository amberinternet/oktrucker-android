package th.co.growthd.oktrucker.util.network.response

import com.google.gson.annotations.SerializedName
import th.co.growthd.oktrucker.models.DepositPagination

data class DepositListResponse(
        @SerializedName("data") var depositPagination: DepositPagination
)