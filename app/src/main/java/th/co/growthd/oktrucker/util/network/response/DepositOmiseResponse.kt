package th.co.growthd.oktrucker.util.network.response

import com.google.gson.annotations.SerializedName
import th.co.growthd.oktrucker.model.DepositOmise

data class DepositOmiseResponse(
        @SerializedName("data") var depositOmise: DepositOmise
)
