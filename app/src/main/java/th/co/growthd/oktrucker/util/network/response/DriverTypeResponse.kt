package th.co.growthd.oktrucker.util.network.response

import com.google.gson.annotations.SerializedName
import th.co.growthd.oktrucker.model.DriverType

data class DriverTypeResponse (@SerializedName("data") var driverType: List<DriverType>)