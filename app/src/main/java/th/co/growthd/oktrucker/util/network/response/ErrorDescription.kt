package th.co.growthd.oktrucker.util.network.response

import com.google.gson.annotations.SerializedName
import th.co.growthd.oktrucker.getError

class ErrorDescription {

    @SerializedName("first_name")
    var firstNameList: List<String> = mutableListOf()

    @SerializedName("last_name")
    var lastNameList: List<String> = mutableListOf()

    @SerializedName("username")
    var usernameList: List<String> = mutableListOf()

    @SerializedName("email")
    var emailList: List<String> = mutableListOf()

    @SerializedName("password")
    var passwordList: List<String> = mutableListOf()

    @SerializedName("current_address")
    var addressList: List<String> = mutableListOf()

    @SerializedName("driver_type_id")
    var driverTypeIdList: List<String> = mutableListOf()

    @SerializedName("driver_license_id")
    var driverLicenseIdList: List<String> = mutableListOf()

    @SerializedName("driver_license_type")
    var driverLicenseTypeList: List<String> = mutableListOf()

    @SerializedName("citizen_id")
    var citizenIdList: List<String> = mutableListOf()

    @SerializedName("car_type_id")
    var carTypeIdList: List<String> = mutableListOf()

    @SerializedName("license_plate")
    var licensePlateList: List<String> = mutableListOf()

    @SerializedName("car_province")
    var carProvinceList: List<String> = mutableListOf()

    @SerializedName("car_year")
    var carYearList: List<String> = mutableListOf()

    @SerializedName("car_model")
    var carModelList: List<String> = mutableListOf()

    @SerializedName("car_brand")
    var carBrandList: List<String> = mutableListOf()

    @SerializedName("pay_time")
    var payTimeList: List<String> = mutableListOf()

    @SerializedName("price")
    var priceList: List<String> = mutableListOf()

    @SerializedName("pay_slip_image")
    var slipImageList: List<String> = mutableListOf()

    @SerializedName("autograph")
    var autographList: List<String> = mutableListOf()

    private var error: String? = ""

    fun getError(): String {
        error += firstNameList?.getError()
        error += lastNameList?.getError()
        error += usernameList?.getError()
        error += emailList?.getError()
        error += passwordList?.getError()
        error += payTimeList?.getError()
        error += addressList.getError()
        error += driverTypeIdList.getError()
        error += driverLicenseIdList.getError()
        error += driverLicenseTypeList.getError()
        error += citizenIdList.getError()
        error += carTypeIdList.getError()
        error += licensePlateList.getError()
        error += carProvinceList.getError()
        error += carYearList.getError()
        error += carModelList.getError()
        error += carBrandList.getError()
        error += priceList.getError()
        error += slipImageList.getError()
        error += autographList.getError()
        return if (error != null) error!!.trim { it <= ' ' } else ""
    }
}