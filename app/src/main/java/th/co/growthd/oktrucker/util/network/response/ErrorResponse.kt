package th.co.growthd.oktrucker.util.network.response

import com.google.gson.annotations.SerializedName
import th.co.growthd.deerandbook.util.INITIAL_STRING

class ErrorResponse {

    @SerializedName("error")
    var error: String = ""
        get() {
            return when (field) {
                "validation_failed", "could_not_create_token" -> "พบข้อผิดพลาด"
                "invalid_credentials" -> "เบอร์โทรศัพท์หรือรหัสผ่านไม่ถูกต้อง"
                "token_invalid", "token_not_provided", "token_expired" -> "กรุณาเข้าสู่ระบบใหม่"
                "user_telephone_number_is_not_verified", "user_is_not_verified" -> "บัญชีของคุณยังไม่ได้รับการยืนยัน\nกรุณารอแอดมินติดต่อกลับเพื่อยืนยันตัวตน"
                "user_status_is_deactive" -> "บัญชีของคุณถูกพักใช้งาน"
                "user_credit_is_not_enough" -> "จำนวนเครดิตไม่พอรับงาน"
                "topup_not_found" -> "ชำระเงินไม่สำเร็จ กรุณาติดต่อแอดมิน"
                "credit_is_not_enough" -> "เครดิตของคุณไม่เพียงพอ\nกรุณาเติมเงิน"
                "news_not_found" -> "ไม่พบข่าวสาร"
                "work_not_found" -> "ไม่พบงานที่คุณกำลังหา"
                "work_cannot_cancel" -> "ไม่สามารถยกเลิกงานได้"
                "work_cannot_accept" -> "ไม่สามารถรับงานได้"
                "work_limit_exceeded" -> "ไม่สามารถรับงานได้\nเนื่องจากคุณรับงานครบ 3 งานแล้ว"
                "invalid_car_type" -> "ไม่สามารถรับงานได้"
                "work_cannot_reach_source" -> "ไม่สามารถถึงต้นทางได้"
                "too_early_to_reach_source" -> "ยังไม่ถึงวันทำงาน ไม่สามารถถึงต้นทางได้"
                "work_cannot_depart_from_source" -> "ไม่สามารถรับสินค้าได้"
                "work_cannot_reach_destination" -> "ไม่สามารถถึงปลายทางได้"
                "too_early_to_reach_destination" -> "ยังไม่ถึงวันทำงาน ไม่สามารถถึงปลายทางได้"
                "work_cannot_complete" -> "ไม่สามารถส่งของได้"
                "invalid_user_type" -> "บัญชีคุณไม่สามารถใช้งานแอปพลิเคชันคนขับรถได้"
                else -> field.replace("_", " ")
            }
        }

    @SerializedName("description")
    var errorDescription: ErrorDescription? = null

    fun getErrorDescription(): String {
        return errorDescription?.getError() ?: INITIAL_STRING
    }

    override fun toString(): String {
        return "ErrorResponse(errorDescription=$errorDescription)"
    }
}