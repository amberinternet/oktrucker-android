package th.co.growthd.oktrucker.util.network.response

import com.google.gson.annotations.SerializedName
import th.co.growthd.oktrucker.models.NewsPagination

data class NewsListResponse(
        @SerializedName("data") var newsPagination: NewsPagination
)