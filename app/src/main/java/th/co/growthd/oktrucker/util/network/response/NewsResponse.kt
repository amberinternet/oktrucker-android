package th.co.growthd.oktrucker.util.network.response

import com.google.gson.annotations.SerializedName
import th.co.growthd.oktrucker.model.News

data class NewsResponse(
        @SerializedName("data") var news: News
)