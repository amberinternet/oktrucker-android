package th.co.growthd.oktrucker.util.network.response

import com.google.gson.annotations.SerializedName
import th.co.growthd.oktrucker.models.TransactionPagination

data class TransactionsListResponse(
        @SerializedName("data") var transactionPagination: TransactionPagination
)