package th.co.growthd.oktrucker.util.network.response

import com.google.gson.annotations.SerializedName
import th.co.growthd.oktrucker.model.TruckType

data class TruckTypeResponse (@SerializedName("data") var truckTypeList: List<TruckType>)