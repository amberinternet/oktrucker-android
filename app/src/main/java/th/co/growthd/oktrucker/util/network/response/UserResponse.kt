package th.co.growthd.oktrucker.util.network.response

import com.google.gson.annotations.SerializedName
import th.co.growthd.oktrucker.model.User

data class UserResponse(@SerializedName("data") var user: User)