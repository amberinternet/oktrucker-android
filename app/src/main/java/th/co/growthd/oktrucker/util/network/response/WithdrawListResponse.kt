package th.co.growthd.oktrucker.util.network.response

import com.google.gson.annotations.SerializedName
import th.co.growthd.oktrucker.models.WithdrawPagination

data class WithdrawListResponse(
        @SerializedName("data") var withdrawPagination: WithdrawPagination
)