package th.co.growthd.oktrucker.util.network.response

import com.google.gson.annotations.SerializedName
import th.co.growthd.oktrucker.models.WorkPagination

data class WorkListResponse(
        @SerializedName("data") var workPagination: WorkPagination
)