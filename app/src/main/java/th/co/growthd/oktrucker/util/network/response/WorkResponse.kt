package th.co.growthd.oktrucker.util.network.response

import com.google.gson.annotations.SerializedName
import th.co.growthd.oktrucker.model.Work

data class WorkResponse(
        @SerializedName("data") var work: Work
)