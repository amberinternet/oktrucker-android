package th.co.growthd.oktrucker.util.network.service

import android.content.Context
import retrofit2.http.GET
import rx.Observable
import th.co.growthd.oktrucker.util.manager.NetworkManager
import th.co.growthd.oktrucker.util.network.response.DriverTypeResponse
import th.co.growthd.oktrucker.util.network.response.TruckTypeResponse

interface AppService {

    companion object {
        fun getInstance(context: Context): AppService {
            return NetworkManager.getInstance(context).nonHeaderRetrofit.create(AppService::class.java)
        }
    }

    @GET("driver/type")
    fun getDriverType(): Observable<DriverTypeResponse>

    @GET("car/type")
    fun getTruckType(): Observable<TruckTypeResponse>
}