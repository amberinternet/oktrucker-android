package th.co.growthd.oktrucker.util.network.service

import android.content.Context
import retrofit2.http.Field
import retrofit2.http.FormUrlEncoded
import retrofit2.http.POST
import rx.Observable
import th.co.growthd.oktrucker.util.manager.NetworkManager
import th.co.growthd.oktrucker.util.network.response.AccessTokenResponse
import th.co.growthd.oktrucker.util.network.response.SuccessResponse

interface AuthService {

    companion object {
        fun getInstance(context: Context): AuthService {
            return NetworkManager.getInstance(context).nonHeaderRetrofit.create(AuthService::class.java)
        }
    }

    @POST("token")
    @FormUrlEncoded
    fun logInWithTelephone(@Field("username") telephone: String, @Field("password") password: String, @Field("token") firebaseToken: String, @Field("type") type: String): Observable<AccessTokenResponse>

    @POST("signup/driver")
    @FormUrlEncoded
    fun signUp(
            @Field("first_name") firstName: String,
            @Field("last_name") lastName: String,
            @Field("username") username: String,
            @Field("email") email: String,
            @Field("password") password: String,
            @Field("password_confirmation") confirmPassword: String,
            @Field("current_address") address: String,
            @Field("company_name") companyName: String,
            @Field("telephone_number") telephoneNumber: String,
            @Field("driver_type_id") driverTypeId: Int,
            @Field("driver_license_id") driverLicenseId: String,
            @Field("driver_license_type") driverLicenseType: String,
            @Field("citizen_id") citizenId: String,
            @Field("car_type_id") truckTypeId: Int,
            @Field("license_plate") licensePlate: String,
            @Field("car_province") truckProvince: String,
            @Field("car_year") truckYear: String,
            @Field("car_model") truckModel: String,
            @Field("car_brand") truckBrand: String,
            @Field("logistee_id") logisteeId: Int
    ): Observable<SuccessResponse>

}