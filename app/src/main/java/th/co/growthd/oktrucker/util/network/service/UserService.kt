package th.co.growthd.oktrucker.util.network.service

import android.content.Context
import okhttp3.RequestBody
import retrofit2.http.*
import rx.Observable
import th.co.growthd.oktrucker.util.manager.NetworkManager
import th.co.growthd.oktrucker.util.network.response.*

interface UserService {

    companion object {
        fun getInstance(context: Context): UserService {
            return NetworkManager.getInstance(context).headerRetrofit.create(UserService::class.java)
        }
    }

    @POST("token/refresh")
    fun refreshToken(): Observable<AccessTokenResponse>

    @POST("token/destroy")
    @FormUrlEncoded
    fun destroyToken(@Field("token") firebaseToken: String): Observable<SuccessResponse>

    @GET("me")
    fun getProfile(): Observable<UserResponse>

    @GET("news")
    fun getNews(@Query("page") page: Int): Observable<NewsListResponse>

    @GET("news/{news_id}")
    fun getNewsDetail(@Path("news_id") newsId: Int): Observable<NewsResponse>

    @GET("me/credit-log")
    fun getTransaction(@Query("page") page: Int, @Query("month") date: String): Observable<TransactionsListResponse>

    @GET("me/topup")
    fun getDeposit(@Query("page") page: Int, @Query("q") date: String): Observable<DepositListResponse>

    @GET("me/withdraw")
    fun getWithdraw(@Query("page") page: Int, @Query("q") date: String): Observable<WithdrawListResponse>

    @GET("work")
    fun getMyWork(@Query("page") page: Int, @Query("status") status: String, @Query("q") search: String, @Query("order_by") sort: String): Observable<WorkListResponse>

    @GET("work/pending")
    fun getWork(@Query("page") page: Int, @Query("q") search: String, @Query("order_by") sort: String): Observable<WorkListResponse>

    @GET("work/{work_id}")
    fun getWorkDetail(@Path("work_id") workId: Int): Observable<WorkResponse>

    @POST("work/{work_id}/accept")
    fun acceptWork(@Path("work_id") workId: Int): Observable<SuccessResponse>

    @POST("work/{work_id}/reach-source")
    fun arrivedSource(@Path("work_id") workId: Int): Observable<SuccessResponse>

    @Multipart
    @POST("work/{work_id}/depart-from-source")
    fun departSource(@Path("work_id") workId: Int,
                     @Part("autograph\"; filename=\"autograph.jpg\"") autograph: RequestBody): Observable<SuccessResponse>

    @POST("work/{work_id}/reach-destination")
    fun arrivedDestination(@Path("work_id") workId: Int): Observable<SuccessResponse>

    @Multipart
    @POST("work/{work_id}/complete")
    fun completeWork(
            @Path("work_id") workId: Int,
            @Part("autograph\"; filename=\"autograph.jpg\"") autograph: RequestBody): Observable<SuccessResponse>

    @POST("me/withdraw")
    @FormUrlEncoded
    fun withdraw(@Field("price") price: Int): Observable<SuccessResponse>

    @Multipart
    @POST("me/topup")
    fun depositWithTransferSlip(
            @Part("topup_type") depositType: RequestBody,
            @Part("price") price: RequestBody,
            @Part("pay_time") payTime: RequestBody,
            @Part("pay_slip_image\"; filename=\"pay_slip.jpg\" ") slipImage: RequestBody): Observable<SuccessResponse>

    @POST("me/topup")
    @FormUrlEncoded
    fun depositWithOmise(
            @Field("topup_type") depositType: Int,
            @Field("price") price: Int,
            @Field("omise_token") omiseToken: String,
            @Field("from_app") fromApp: Int): Observable<DepositOmiseResponse>

    @POST("topup/{deposit_id}/success")
    fun completePaymentOmise(@Path("deposit_id") depositId: Int): Observable<SuccessResponse>

    @POST("location")
    @FormUrlEncoded
    fun updateLocation(@Field("latitude") latitude: Double, @Field("longitude") longitude: Double): Observable<SuccessResponse>
}